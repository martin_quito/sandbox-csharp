﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace PRONETMVC5_Razor.Model
{
	public class Product
	{
		// instead of doing this normal properties
		private int productID;
		public int ProductID
		{
			get { return productID; }
			set { productID = value; }
		}

		// we can do this, using automaticall implemented property
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string Category { get; set; }
		// - remmeber I cannot do a mix-match, is either one or the other
	}
}