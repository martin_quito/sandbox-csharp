﻿/*

What is Razor?
	- is the view engine of ASP.NET that create dynamic pages
    - view engine processes content and looks for instructions, typically to 
    insert dynamic content into the output sent to a browser

Why?
	- reduction in multiple views 
	
How?
Syntax
	- @model statement declares the type of the model object that I will pass to
	the view from the action method. I can refer the model using @Model
		- Visual studio will provide intellisente when using @model
		- @model is a razor expression

	- implicit razor expressions
		<p>@DateTime.Now</p>

	- explicit razor expression
		<p>Last week this time: @(DateTime.Now - TimeSpan.FromDays(7))</p>

	- C# expressions that evaluate to a string are HTML encoded
		@("<span>Hello World</span>") => &lt;span&gt;Hello World&lt;/span&gt;

	- @ {} is a code block
		@
        {
			var quote = "The future ..."	
		}

    - implicit transitions
        The default language in a code block is C#, bit Razor page can transition back to HTML
        @
        {
            var inCSharp = true;
            <p>Now in HTML was in C# @inCSharp</p>
        }

	- explicit delimited  transition
		@for (var i = 0; i < people.length; i++) {
			var person = people[i];
			<text>Name: @person.Name</text>
		}
		- to define a subsection of code block that should render HTML, surround 
        the charactes for rendering with the Razor <text> tag
            - you don't need this if you have another tag that wraps around it
            - this tag is useful to control the whitespace, only the content
            between the <text> tag is rendered AND no whitespace before
            or after the <text> tag appears in the HTML output.

	- explicit line transition with @:
		- to render the rest of an entire line as HTML inside a code block, use the "@:"
		@for( ...) {
			var person = peoeple[i];
			@:Name: @person.Name
		}


        For example
        @for (var i = 0; i < nums.Length; i++) {
            var person = nums[i];
            Name: @person
        }
            - this will NOT WORK 
           
        @for (var i = 0; i < nums.Length; i++) {
            var person = nums[i];
            @person
        }
            - this will work
            - the "@person" line is treated as HTML , but when you have HTML and
            C#, then it needsd help. 
            - if I want to have html and C# (e.g. @person) in the same line, 
            I need to wrap it with "@:" which means the whole line is treated 
            as HTML and I can then inject Razor

	- Control structures
			@if(...) {}
			@for(...){}
			@foreach(...){}
			@while ...
			@do while ...

	- compound @using
		- in Razor, used to create HTML helpers that contain additional cotnent


	- @try, catch, finally
		@try{}catch{}finally{}

	- Comments
		@{
            \/*   *\/
            //
        }
		<!-- HTML element -->
	
		Razor comments are removed by the server before the webpage is rendered. Razor's comments
		are AtAsterik <=> AtAsterik

	- directives
		@using	- adds the C# 'using' directive
		@model	- specifies the type of the model passed to a view
		@inherits	- provides full control of the class the view inhertis
		@injects	- enables the Razor page to inject a service from the service container
		@section - is used in conjection with the layout to enable views
		to render content in different parts of the HTML page.


Layouts
	- specialized form of view that works as a template
	- files in the "View" folder whose names begin with "_" are nto returned to the user
	- to use the layout 
		- inside a view for rednering to the user, I pass a relative path to the "Layout" property
		- but what if I don't want to declare the layout multiple times, then use the "_ViewStart.cshtml" file
		and place it in the views folder. What happens next is that the MVC framework will locate
		the file and use its contents automatically. The values defined in the view file take precedence,
		which makes it easy to override the view start file.
	- shared layouts
		- the "NameAndPrice" action method is using the same layout that the "index" action method.
    

RAZOR
    - @ { layout = null } this razor expression tells razor that I chose NOT TO USE a layout
*/
