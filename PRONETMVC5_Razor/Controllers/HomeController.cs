﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRONETMVC5_Razor.Model;

namespace PRONETMVC5_Razor.Controllers
{
    public class HomeController : Controller
    {
		Product myProduct = new Product
		{
			ProductID = 1,
			Name = "Kayak",
			Description = "A boat",
			Category = "Watersports",
			Price = 275M
		};

        // GET: Home
        public ActionResult Index()
        {
            return View(myProduct);
        }

		// example of a sharedlayout
		public ActionResult NameAndPrice()
		{
			return View(myProduct);
		}

    }
}