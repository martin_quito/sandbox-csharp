﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.Infrastructure {
    public class RangeExceptionAttribute : FilterAttribute, IExceptionFilter {

        public void OnException(ExceptionContext filterContext) {
            if (!filterContext.ExceptionHandled 
				&& filterContext.Exception is ArgumentOutOfRangeException) {
                // generic
				filterContext.Result
						= new RedirectResult("~/Content/RangeErrorPage.html");
				filterContext.ExceptionHandled = true;
            }
        }
    }


	public class RangeExceptionWithViewLowLevelAttribute : FilterAttribute, IExceptionFilter {

		public void OnException(ExceptionContext filterContext) {
			if (!filterContext.ExceptionHandled
				&& filterContext.Exception is ArgumentOutOfRangeException) {

				int val = (int)(((ArgumentOutOfRangeException)
					filterContext.Exception).ActualValue);
				filterContext.Result = new ViewResult {
					ViewName = "RangeError",
					ViewData = new ViewDataDictionary<int>(val)
				};

				filterContext.ExceptionHandled = true;
				// this is a bit messy because i'm working directly
				// with the viewresult instead of using the "view" method
				// 
			}
		}
	}
}