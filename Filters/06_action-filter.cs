﻿/*
Why
What
	- used for any purpose
	

How
	Interface: IActionFilter
	DEfault: ActionFilterAttribute

	public interface IActionFilter {
        void OnActionExecuting(ActionExecuingContext c)
        void OnActionExecuted(ActionExecutedContext c)
    }
		- MVC calls	"OnActionExecuting" before the action method is invoked
		and "OnActionExecuted" is called after the method has been invoked
            - the OnActionExectued is executed after the method has completed
            but before the result is processed
	
	Implementing "OnActionExecution" Method
		- the 'OnActionExecuting' method is call before the action method is
		invoked, you can use this opporunity to inspect the request and
        and elect to cancel the request, modify the request, or start some activity
        
        - the parameter is 'ActionExecutingContext' which suclasses the
        'ControllerContext' class and defines the two additional propewrties 

            ActionDescriptor 
                - ActionDescriptor (type)
                - provides details of the action method
            Result
                - ActionResult
                - the result for action method; a filter can cancel the request
                by setting this property to a non-null value   
        - when implementing the 'IActionFilter', you don't need to implement both
        interface methods. Be careful not to accidentaly use visual studio's 
        interface implemntation and throw 'NotImplementedException', this will cause
        and un necessary call and raise exception filters. 

        - see 'CustomActionAttribute.cs' and 'HomeDController.cs'
            - this will return the HTTP Error 404.0 - Not found

    Implementing the "OnActionExecuted" Method
        - example, see ProfileACtionAttribute.cs- HomeEController.cs

        
        "ActionExectuedContext" 
            props
                ActionDescriptor
                    - ActionDescriptor (type)
                    - provides details of the action method
                Canceled
                    - bool (type)
                    - returns true if the action has been canceled by 
                    another filter
                Exception
                    - Exception (type)
                    - returns an exception thrown by another filter OR by
                    the action method
                ExceptionHandled
                    - bool (type)
                    - returns true if the exception has been handled
                Result           
                    - ActionResult (type) 
                    - the result for the action method; a filter can cancel
                    the request by setting this property to a non-value null
            It extends ControllerContext

    Built in action adn Result Filter class
        - the 'ActionFilterAttribute' implements the FilterAttribute, 
        'IACtionFilter', 'IResultFilter'. The benefit is that I don't need to 
        override and implement the methods that I don't intend to use

        public abstract class ActionFilterAttribute: FilterAttribute, IActionFilter,
            IResultFilter {
            
            public virtual void OnACtionExecuting...
            public virtual void OnActionExecuted...
            public virtual void OnResultExecuting...
            public virtual void OnResultExecuted...
        }

      
        
*/