﻿/*
Filtering Without Attributes
    - the controller class implments the 
        - IAuthenticationFilter
        - IAuthorizsationFilter
        - IActionFilter
        - IResultFilter
        - IExceptionFilter
    - when is this useful?
        - when you are creating a base class from which multiple controllers
        in your project are dervied. The whole point of filter is to put
        code that is required ACROSS the application in one reusable location.
            - separation between controller logic and attribute logic? There is another
            method to apply filters to all your controllers  

Using Global Filters
    - are applied to all action methods in all of the controllers 
    - how
        One method
            - create a new file "FilterConfig.cs" and put it in the "App_Start" dir
            See "FilterConfig.cs" as an example. 
            - one convention to note is that the namespace must be "Filters"
            - the "HandleError" filter is always defined as a global filter 
                - I don't have to set the HandleErrorAttribute globally, but
                if you do, it defines the default MVC exception handling policy.
                This will render the /Views/Shared/Error.cshtml" view when
                and unhandled exception arises. This exception handling policy
                is disbled by default for development. 

            - and last step is to ensure that "FilterConfig.RegisterGlobalFilters" get called
            in the Global.asax file
          Other methods
            - you can have the "RegisterGlobalFilters" inside the Globlax.axas and call it
            from there


Ordering Filter Execution
    The sequence is
        AuthenticationFilters
        AuthorizationFilters
        ActionFilters
        ResultFilters

    Exception fiulters are executed at any stage if there is an unhandled
    exception. However, withing each type category, you can take control
    of the order in which individuals filters are used. 
    
    How       
        - use the "Order" property
        - no example right now (see page 523 from first book)

Overriding Filters
    - use the "IOverrideFilter"
    - see page 525 from first book 


*/