﻿/*
Why
What
    - are general purpose filters which operate on the results produced by
    action methods
    - what its time execution?
        - when applied to an action method,
            - the 'OnResultExecuting' is invoked AFTER the action
            method has returned but BEFORE the action result is executed
            (how about the action filter OnActionExecuted). The 'OnResultExecuted'
            method is invoked after the action result is executed. 
                 - default order with action filters
                    action.executing
                    action.executed
                    result.executing
                    result.executed
How
    Result filters implement: "IResultFilter"
        public interface IResultFilter {
            void OnResultExecuting(ResultExecutingContext filterContext)
            void OnREsultExecuted(REsultExecutedContext filterContext)
        }
    
    Example 
        - ProfileActionResultFilterAttribute.cs, HomeEController.cs
    
*/