﻿/*
Why
	
What
	- runs after authentication filters, before action filters, and
	before the action method is invoked
	- ensures Action methods can be invoked by approved users
	- you can implement "IAuthorizationFilter" but is not encourage, 
	instead, subclass of the "AuthorizeAttribute". Writing security code
	is dangerous. 
		
	- built-in authorization filter, is the  "AuthorizeAttribute" 

How
	Interface: IAutnhorizationFilter
	DefaultImplementation : AuthorizeAttribute
	
	Example 1 - custom Authorization Filter
		Define static configuration in the "web.config" for it
				<authentication mode="Forms">
				  <forms loginUrl="~/Account/Login" timeout="2880">
					<credentials passwordFormat="Clear">
					  <user name="user" password="secret"/>
					  <user name="admin" password="secret"/>
					</credentials>
				  </forms>
				</authentication>
			- unauthenticated should go redirected to "/Account/Login"

				
		Create "AccountController.cs" (see file) to define the "Login" method
			that checks if the user is valid or not, and it redirects

		Create "HomeController.cs" (see file)

		Create "Views/Account/Login.cshtml" (see file)

		How to subclass the "AuthorizeAttrbiute" check "CustomAuthAttribute.cs"
			- this prevents local users
			- this overwrites the "AuthorizeCore" to take benefit from the features
			from AuthorizeAttribute
				- this is how MVC checkx to see if the filter
				will authorize access for a request. 
				- note you can get info of the request from the  HttpContextBase
					- using the "AuthorizeCore" method is passed the "HttpContextBase"
					object which provide access to info about the request, but NOT
					the CONTROLLER OR ACTION METHOD that the aturhoziation attribute
					has been applied. The main reason that developers implement
					the 'IAuthorizationFilter' interface directly is to get access
					to the 'AuthorizationContext' passed to the 'OnAuthorization'
					method, through which a much wider range of information can be contained
						- author suggest not to use this approach because not just
						writing your own secutity code, but building logic in your
						'authorization' attribute which is tightly coupled to the
						structued of your controller undermines the separation
						of concern and causes testing and maintenance problems
						- its funny but mvc 3.0.0.1, you can subclass the
						'AuthorizeAttribute' and override the 'OnAuthorization' method
				- taking advatnaged of the "AuthorizeAttribute" I only need
				to focus on the authorization logic since all the other data
				I need is already there
				
		All this makes it so if the request is local, mvc prompts for credentials,
		but again, when a request is sent again, mvc will check that is local,
		It is a challange that will never get passed. 


	Example 2 - using built-in authorization filter
		- you can use built authotization policies
			- Users
				- 
			- Roles

*/