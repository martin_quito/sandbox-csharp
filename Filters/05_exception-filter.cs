﻿/*  
Why
What
    - are only if an unhandled exceptions has been thrown when invokin
    an action method. The exception can come from
		- another kind of filter
		- the action method itself
		- when the action result is executed

    - the two main uses of ExceptionFilter is for loggin and display
    the exception to the user

	- 
	
How
	1. Creating an Exception Filter
        implement IExceptionFilter
            onException (ExceptionContext filterContext)
				- is called when an unhandled exception arises

				- the ExceptionContext is derived from ControllerCOntext

					Useful properties of ControllerContext (type)
					- Controller
						- returns controller object for this request
					- HttpContext
						- details of the request
					- IsChildAction
						- true if this is a child action 
					- RequestContext
						- access to the HttpContext and Routing data
					- RouteData
						- routing data for this request

					ExceptionContext (type)
						- ActionDescriptor
							- provides details of the action method
						- Result
							- the result for the action method, a filter cancel
							the request by setting this property to a non-null value
						- Exception
							- the unhandled exception
						- ExceptionHandled
							- its a good practice to check this to have other
							exception filtes ignore the handled exception
							If none of the exception filters for an action
							method set the 'ExceptionHandled' property to	
							true, the MVC uses the default ASP.NET exeception
							handling procedure, known as 'yellow screen death'

		Example of this is 'RangeExceptionAttribute.cs'
			- this handles 'ArgumentOutOfRangeException' instances by redirecting
			the user's browser to a file called 'RangeErrorPage.html'
			in the Content Folter
				- it derives from the FilterAttribute
					- in order for MVC to see this as a filter, ithe class neds
					to implement the 'IMvcFilter' interface
					- this attribute also provides handleding the default order
					in which filters are processed 
				- it implements IExceptionFilter
		
		Applying the created Exception filter
			- RnageErrorPage.html
			- HomeBController.html
				
	2. Using a view to respond an exception
		- you can return view instead of a static file
		- the exception filter is in the 'rangeExceptionAttribute.cs'
			- RangeExceptionWithViewLowLevelAttribute type
			- its a bit messy because I'm creating the ViewResult from scracth
			and not use the View object returned from the action method. A better
			approach is to use the built-in exception

		- the reason why use views is to keep a consistante of layouts when
		showing errors. The problem is when an exception occurs INSIDE the
		view which nothing will be cached. So when using an exception filter
		that relies on a view, be careful to test that view throughly

	3. Using a built-in Exception filter
		Interface: IExceptionFilter
		DefaultImplementation: HandleErrorAttribute
	
		- its better to use the built in. With it, you can specify the
		exception and the names of a view and layout using the following props

                ExceptionType 
                    - the type of exceptions it will handle.  
                    includes those that inherit from a specific type
                    - defualt is System.Exception
                View
                    - the name of the view template that this filter renders
                    - defaults to "Error" 
                        /Views/<currentControllerName>/Error.cshtml
                        /Views/Shared/Error.cshtml
                Master
                    - name of the layout
                    - if not, it uses the default layout


            - to enable it, you need to enable it in the config
                <customErrors mode="RemoteOnly" defaultRedirect="~/Error">
                    <error statusCode="500" redirect="~/Error/Internal" />
                </customErrors>

				- You can have the url point to a controller too! But you can't
				point directly to a view
				- 'RemoteOnly' means that connections made from the local machine
				will always receive the standard yellow page of death errors
			
		- when to use th 'HandleErrorAttribute'
			- ??? 

		- when to use 'web.config' custom errors
			- ???

		- how many ways to handle errors?
			- application_error
			- web.config customErrors
			- handleErrorAttribute
			- controller onException

		- in HomeBController.cs, RangeTest2
			- when rendering the view, the HandleErrorAtrbute filter passes
			a 'HandleErrorInfo' view model object which is wrapped around
			the exception that provides additional info that you can use in your
			view (see RangeError2.cshtml)
			
			HandleErrorInfo
				ActionName
				ControllerName
				Exception

*/