﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Filters.Infrastructure;

namespace Filters.Controllers
{
    public class HomeBController : Controller
    {
		[RangeException]
		public string RangeTest(int id) {
			if (id > 100) {
				return String.Format("The id value is: {0}", id);
			}
			else {
				throw new ArgumentOutOfRangeException("id", id, "");
			}
		}


		[RangeExceptionWithViewLowLevel]
		public string RangeTestB(int id) {
			if (id > 100) {
				return String.Format("The id value is: {0}", id);
			}
			else {
				throw new ArgumentOutOfRangeException("id", id, "");
			}
		}

		[HandleError(ExceptionType = typeof(ArgumentOutOfRangeException),
			View = "RangeError2")]
		public string RangeTest2(int id) {
			if (id > 100) {
				return String.Format("The id value is: {0}", id);
			}
			else {
				throw new ArgumentOutOfRangeException("id", id, "");
			}
		}

		public ViewResult Error() {
			return View();
		}

		//[CustomAction]
		//public string FilterTest() {
		//	return "This is the Filter Test";
		//}

		//[ProfileAction]
		//public string FilterTest2() {
		//	return "This is the Filter Test";
		//}

		//public string FilterTest3() {
		//	return "This is FitlerTest3";
		//}
    }
}