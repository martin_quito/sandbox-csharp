﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Filters.Infrastructure;

namespace Filters.Controllers
{
    public class HomeEController : Controller
    {
        [ProfileAction]
        [ProfileResult]
        public string FilterTest() {
            return "This is the FilterTest action";
        }
    }
}