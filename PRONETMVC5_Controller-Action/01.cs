﻿/*
Why?
	- 

What?
	- every request that comes to your application is handled by a controller
	- are .NET classes that contain the logci required to handle a request
	- responsbilities
		-  processing incoming request
		- perfoming operations on the domain model
		- selecting views to render to the user

How?
	- a controller must implement the IController interface from the System.Web.Mvc
	- IController has one method "Execute"
	- when a request comes, MVC will try to get the correct controller, and it uses either
	value of the "controller" property generated by the routing data OR the custom routing
	classes to select the controller
*/