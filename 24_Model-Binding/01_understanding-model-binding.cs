﻿/*
What is model binding?
	- is the process of creating .NET objects using the data sent by the browser
	in an HTTP request

So, how does it work?
	Lets use this to explain what is happening
		
		public ActionResult Index(int id) {...}

	- The process that leads to 'model binding' begins when the request is
	received and processed by the routing engine. Then the default action
	invoker used the routing information to figure out that the 'index'
	action method is required to service the request, but it can't call the
	index method until it had some usefult values for the method argument.
	The default action invoker, 'ControllerActionInvoker', relies on
	'model binders' to generate the data objects that are required to invoke
	the action. Model binders are defined by the 'IModelBinder' interface. 
	There can be multiple 'model binders' in an mvc app, each binder
	can be responsible for binding one or more model types. So, when the
	action invoker needs to call an action method, it looks at the parameters
	that the method defines and finds the responsbile model binder for
	the type of each one. 

	In example 1, the action invoker examins the 'index' method
	and finds that it has one 'int' parameter. It would then locate
	the binder responsible for 'int' values and call its 'BindModel' method
	The model binder is responsible for PROVIDING an 'int' value that can
	be used to call the 'Index' method. This usually transforming some elements
	of reh request data (mvc frmaewokr doesn't put any limits on how the
	data is obtained')
	


	
*/


// ==================
// Example 1 - simple model binding
// ==================
/*


Files
	- HomeController.cs | "simple binding" section
	- Index.cshtml
*/