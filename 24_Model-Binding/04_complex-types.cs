﻿/*
What?
    - when the action method parameter is complex type (any type which cannot be converted 
    using the 'TypeCoverter' class), then the 'DefaultModelBinder' class use reflection to 
    obtain the set of public properties and then binds to each of them in turn. 

How does it work when the default action invoker encoutners a complex type?
    - using example 1, when I post the form back to the 'CreatePerson' method
    the default model binder discovers that the action method requires a 'Person'
    object and process each of the properties in turn. For each simple type
    property, the binder tries to locate a request value, just as it did in the previous
    example, for example, when it encouters the 'PersonId' property, the binder
    will look for a 'PersonId' data value which it finds in the form data
    in the request. If the property REQUIRE another complex type, then the process
    is repeated for the new type. The set of 'public' properties are obtained and
    the binder tries to find values for all of them. The difference is that
    the property names are nested. The 'HomeAddress' property of the 'Person'
    class is of the 'Address' type. So, this means that for example when
    looking for Line1 property
        
        public class Address {
            public string Line1 {get;set;}
            ...
        }

    , the model binder looks for a value for 'HomeAddress.Line1' as in the
    name of the property in the model object contained with the name of the
    property in the property type. This 

    
*/

// ============
// Example 1-  of binding a complex type
// ============
/*
What
    - binding a complex type and nested compelx types
Files:
    - HomeController.cs | "Complex Types" section
    - CreatePerson.cshtml

Learned
    <div>
        @Html.LabelFor(m => m.HomeAddress.Country)
        @Html.EditorFor(m => m.HomeAddress.Country)
    </div>

        into =>
    
    <input class "text-box single-line" id="HomeAddress_Country" 
    name="HomeAddress.Country" type="text" value "" />
    
    This feature makes it so I don't have to take any special action
    to ensure that the model binder can create the 'Address' object
    for the 'HomeAddress' property. 

*/