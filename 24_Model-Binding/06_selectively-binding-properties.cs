﻿/*
What?
    The problem: Image there is a property like 'Country' property
    in the 'AddressSummary' class is specially sensitive and that I dont
    want the user to be able to specify values for it.

    Solution: I can prevent the user from seeing the property or
    even prevent the property from being included in the HTML
    sent to the browser, using the attributes showed in anothe occasion.
    or simply by not adding editors for that property to the view. 
    However a nefarious user could simply edit the form data sent
    to the server when submitting the form data and pick the value
    for the 'Country' property that suits them. WHAT I NEED TO DO
    is to tell the model binder not to bind a value for the
    'Country' property from the request which I can do by by using the
    'Bind' attribute AGAIn on the action method parameter. 

    A Quick example
        
    public ActionResult DisplaySummary(
        [Bind(Prefix="HomeAddress", Exclude="Country")AddressSummary summary
    ) {...}

    The 'Exclude' property allows you to exclude properties from
    the model binding process. Note, you can also use the 'Include' 
    property to specify those properties that should be bound in
    the model; all the properties will be ignored. 
 
    Be aware of the scope of this 'Bind' attribute, so far it will
    affect instances of that class for a specific action emethod. If you
    want to affect other action methods, you can apply the 'Bind'
    attribute in a 'class' scope

    namespace MvcModels.Models {
        [Bind(Include="City")]
        public class AddressSummary {
            public string City {get;set;}
            public string Country {get;set;}
        }
    }

What happens if you applyi 'Bind' to the model class AND to the
action method parameter?
    - see page 692 (from book1)
*/