﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace _24_Model_Binding.Infrastructure {
    /*
        - to regiester the value provider with the app, I need to create
        a factory class that will create instances of the provider when they
        are requried by the mvc. The factory class must be derived from the
        abstract 'valueProviderFactory' class. 

        - the 'GetValueProvider' method is called when the model wants
        to obtain values for the binding process. This implementation simply
        creates and returns an sintance of the 'CountryValuePRovider' class,
        but you can use the data provided through the 'ControllerContext'
        parameter to respond to different kinds of REQUESTS by creating 
        different value providers. 
    */
    public class CustomValueProviderFactory : ValueProviderFactory {

        public override IValueProvider GetValueProvider(ControllerContext controllerContext) {
            return new CountryValueProvider();
        }

    }
}