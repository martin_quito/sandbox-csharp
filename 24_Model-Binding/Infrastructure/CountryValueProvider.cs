﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace _24_Model_Binding.Infrastructure {
    public class CountryValueProvider : IValueProvider {
        /*
            - this is called by the model binder to determine
            if the VALUE provider can resolve the data for a given
            prefix. 
        */
        public bool ContainsPrefix(string prefix) {
            return prefix.ToLower().IndexOf("country") > -1;
        }

        /*
            - returns a value for a given data key or null if
            the provider doesn't have any suitable data
        */
        public ValueProviderResult GetValue(string key) {
            if (ContainsPrefix(key)) {
                /*
                    - data I want to associate with the requested key
                    - version of the data value that is safe to display
                    as part of an html page
                    - culture information 
                */
                return new ValueProviderResult("USA", "USA",
                    CultureInfo.InvariantCulture);
            }
            else {
                return null;
            }
        }

        /*
            - this value provider only responds to requests for values
            for the 'Country' property and it always returns the value
            USA. For other request, it returns 'null'
        */

    }
}