﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _24_Model_Binding.Models;
/*
- I can override the default binder's behavior by creating a custom
model binder for a specific type
- custom model binders implement the 'IModelBinder' interface
- mvc will call BindModel when it wants an instance of the model type
that the binder supports 
- the controllercontext can be used to get details of the request
- the modelbindingcontext is used to provide details of the 'model object'
that is sought and as well to access the rest of the model binding
facilities in the MVC application. 
    Model       - returns the model object passed to the UpdateModel
                    method if binding has been invoked manually
    ModelName   - returns the name of the model that is begin bound
                    
    ModelType   - returns the type of the model that is beign created
            
    valueProvider   - returns an IValueProvider implementation that can be
                        be used to get data values from the request
                    
    ...
- when the bindModel is called, I check if the model has been set. if not, 
create an new instance. To get the values, I get the values
from the ValueProvider. 
*/
namespace _24_Model_Binding.Infrastructure {
    public class AddressSummaryBinder : IModelBinder {

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

            AddressSummary model = (AddressSummary)bindingContext.Model
                                    ?? new AddressSummary();
            model.City = GetValue(bindingContext, "City");
            model.Country = GetValue(bindingContext, "Country");
            return model;
        }

        private string GetValue(ModelBindingContext context, string name) {
            name = (context.ModelName == "" ? "" : context.ModelName 
                + ".") + name;

            ValueProviderResult result = context.ValueProvider.GetValue(name);
            if (result == null || result.AttemptedValue == "") {
                return "<Not specified>";
            }
            else {
                return (string)result.AttemptedValue;
            }
        }
    }
}