﻿/*
How does the default model binder binds simple types?
	- when dealing with simple parameter types, the 'defaultmodelBinder'
	tries to convert the string value (which has been obtained from the
	request data) into the parameter type using the 'System.ComponentModel
	.TypeDescriptor' class. If the value cannot be converted, then the
	'defaultModelBinder' wont be able to bind to the model. 

How culture sensitive parsing is done?
	- see more in page 685 on first-book



*/

// ===========
// Example - passing not a simple type
// ===========
/*
Files
	- HomeController.cs | 'simple binding' section
	- Index.cshtml
	- People.cs
Result
	- it failes when you do /Home/Index/apple

Learned
	- because int cannot accept a null, I can provide a fallback by making
	the 'int' nullable. That would require to add logic to handle nulls
	inside the action method. A better approach is to use a default value
		e.g.
			(int id = 1)

	But be aware, in this example, to do additional checks, because
	for example, if I pass "/Home/Index/-1" or "/Home/Index/500", it will
	throw an error. 
*/