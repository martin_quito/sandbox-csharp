﻿/*
What is IModelBinder?
	public interface IModelBinder {
		object BindModel(ControllerContext controllerContext,
				ModelBindingContext bindingContext) {

		}	
	}

What is the default model binder?
	- this is the binder that is used by action invoker when it cannot find a	
	a custom binder to bind the type. 

Where does the Default binder look for data to bind to c# obejcts?
	By DEFAULT, the default model binder searches four locations  for
	data matching the name of the parameter being bound
		- Request.Form
		- RouteData.Values
		- Request.QueryString
		- Request.Files

	For example
		public ActionResult Index(int id) {...}
		
		The default-action-invoker uses the default-model-binder and looks 
		for 'id' on the following locations
			Request.Form["Id"]
			RouteData.Values["id"]
			Request.QueryString["id"]
			Request.Files["id"]

		The search is stopped as soon as a value is found. 

		*** "When relying on the default model binder, it is important that
		the parameters for your action method match the data property you
		are looking for." It is not case sensitive though, for example,
		Id or ID, it will work the same. 
	
	
*/
