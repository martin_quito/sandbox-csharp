﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using _24_Model_Binding.Infrastructure;
using _24_Model_Binding.Models;

namespace _24_Model_Binding
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //ValueProviderFactories.Factories.Insert(0, new CustomValueProviderFactory());
                /*
                    - the model binder looks at the value provider
                    in sequence, which means I have to use 'insert'
                    method to put the custom factory at the first position
                    in the collection IF I WANT to take precedence over the
                    built-in providers. 
                */

            // - register the binder
            // - you can also register via an attribute
            /*
                [ModelBinder(typeof(AddressSummaryBinder))]
                public class AddressSummary {}
            */
            ModelBinders.Binders.Add(typeof(AddressSummary), new AddressSummaryBinder());
        }
    }
}
