﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _24_Model_Binding.Models;

namespace _24_Model_Binding.Controllers
{
    public class HomeController : Controller
    {
        private Person[] personData = {
            new Person {PersonId = 1, FirstName = "Adam", LastName = "FreeMan", Role = Role.Admin},
            new Person {PersonId = 2, FirstName = "Jackqui", LastName = "Griffyh", Role = Role.User},
            new Person {PersonId = 3, FirstName = "John", LastName = "Smith", Role = Role.User},
            new Person {PersonId = 4, FirstName = "Anne", LastName = "Jones", Role = Role.Guest},
        };

        // =============
        // simple binding
        // =============
        public ActionResult Index(int ID)
        {
            Person dataItem = personData.Where(p => p.PersonId == ID).First();
            return View(dataItem);
        }

        // =============
        // complex types
        // =============
        public ActionResult CreatePerson() {
            return View(new Person());
        }


        [HttpPost]
        public ActionResult CreatePerson(Person model) {
            return View("Index", model);
        }

        // =============
        // specifying custom prefix
        // =============
        public ActionResult CustomPrefixIndex() {
            return View("CustomPrefixIndex", new Person());
        }

        [HttpPost]
        public ActionResult CustomPrefixIndex(
            // instead of the prefix "AddressSummary" the binder will look
            // for a prefix HomeAddress
            [Bind(Prefix="HomeAddress")]AddressSummary summary
        ) {
            return View("CustomPrefixIndexPost", summary);
        }

        // =============
        // arrays
        // =============
        public ViewResult Arrays(string[] names) {
            names = names ?? new string[0];
            return View(names);
        }

        // =============
        // collections 
        // =============
        public ActionResult Collections(IList<string> names) {
            names = names ?? new string[0];
            return View("Collections", names);
        }

        // =============
        // colelctions of custom types
        // =============
        public ActionResult CollectionsCustomType(
            IList<AddressSummary> addresses
        ) {
            addresses = addresses ?? new List<AddressSummary>();
            return View("CollectionsCustomType", addresses);
        }

        // =============
        // Manually Invoking Model Binding
        // =============
        public ActionResult ManualInvoke() {
            IList<AddressSummary> addresses = new List<AddressSummary>();
            UpdateModel(addresses);
            return View("CollectionsCustomType", addresses);
        }

        // =============
        // Manually Invoking Model Binding
        // =============
        //public ActionResult Manual
        //    InvokeFormRestricted() {
        //    IList<AddressSummary> addresses = new List<AddressSummary>();
        //    UpdateModel(addresses, new FormValueProvider(ControllerContext));
        //    return View("CollectionsCustomType", addresses);
        //}

        // =============
        // Manually Invoking Model Binding
        // =============
        public ActionResult ManualInvokeFormRestrictedParam(
            FormCollection formData
        ) {
            IList<AddressSummary> addresses = new List<AddressSummary>();
            UpdateModel(addresses, formData);
            return View("CollectionsCustomType", addresses);
        }

        // =============
        // custom value provider and custom model binder (example)
        // =============
        public ActionResult Address() {
            IList<AddressSummary> addresses = new List<AddressSummary>();
            UpdateModel(addresses);
            return View("CollectionsCustomType", addresses);
        }

    }
}