﻿/*
What
    - there are occasions when the HTML you generate relates to one type of
    object, but you want to bind it to another
*/

// =============
// Example
// =============
/*
Files
    - Person.cs
    - CustomPrefixIndex.cshtml
    - CustomPrefixIndexPost.cshtml
    - HomeController | "specifying custom prefix" section

Learned
    - if you have the CustomPRefixIndex with just 'AddressSummary summary', 
    when the form is posted, the 'name' attritute in the form have the
    'HomeAddress' prefix, which is not what the model binder is LOOKING
    for when it tries to bind the 'AddressSummary' type. You can fix it
    by applying the 'Bind' Attribute to the action method parameter
    which tells the binder which prefix to look for. 
*/