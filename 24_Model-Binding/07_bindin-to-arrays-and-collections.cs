﻿/*
What
    - defaul model binder includes some nice support for binding request
    data to arrays and collections


Other
    - you can only use constant or literal values
    for parameters
*/

// ===========
// Example 1 - array
// ===========
/*
Files
    - HomeController | "specifying custom prefix" section
    - Arrays.cshtml

Learned
    - it generates
    <form action ...>
        <div>...<input name="names" type="text"/></div>
        <div>...<input name="names" type="text"/></div>
        <div>...<input name="names" type="text"/></div>
    </form>

    The 'Names' action takes a string aray parameter called 'names'
    The model binder will look for any data item that is called 'names'
    and create an array that contains those values
*/


// ===========
// Example 2 - collection
// ===========
/*
What
    - same as arrays
Files
    - Home Controller | "Collections"
    - Collections.cshtml
*/


// ===========
// Example 3 - collections of custom model types
// ===========
/*
What
    - I can also bind individual data properties to an array 
    custom types such as 'AddressSummary' model class

Files
    - Home COntroller | "Colections of custom types"
    - CollectionsCustomType.cshtml
*/
