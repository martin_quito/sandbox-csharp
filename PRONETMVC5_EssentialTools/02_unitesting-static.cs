﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace SomeLibraryYouCantModify {
	public static class SomeStaticClass {
		public static bool SomeStaticMethod(string input) {
			return !string.IsNullOrEmpty(input);
		}
	}
}

namespace SomeLibraryYouCanModify {
	// this class is tightly coupled to the class, I want to separated!
	public class SomeUntstableClass {
		public int SomeMethod(string input) {
			var value = SomeLibraryYouCantModify.SomeStaticClass.SomeStaticMethod(input);
			return value ? 1 : 0;
		}
	}

	// interace
	public interface IStaticWrapper {
		bool SomeStaticMethod(string input);
	}

	// 
	public class StaticWrapper : IStaticWrapper {
		public bool SomeStaticMethod(string input) {
			return SomeLibraryYouCantModify.SomeStaticClass.SomeStaticMethod(input);
		}
	}

	// this is the wrapper to the tighly coupled class I had earlier
	public class WrapperMethod {
		IStaticWrapper _wrapper;
		public WrapperMethod(IStaticWrapper wrapper) {
			_wrapper = wrapper;
		}
		public int SomeMethod(string input) {
			var value = _wrapper.SomeStaticMethod(input);
			return value ? 1 : 0;
		}
	}

	// Testing
	

}

