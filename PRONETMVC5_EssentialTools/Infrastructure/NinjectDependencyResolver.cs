﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using PRONETMVC5_EssentialTools.Models;
using System.Web.Mvc;

namespace PRONETMVC5_EssentialTools.Infrastructure
{
	/*
        IDependencyResolver
            - represents a dependency injection container and which the MVC 
            framework uses to get the object it needs
            - this is not part of the ninject
		    - will be used by MVC when it needs an instance of a class
		    to service an incoming request. The job of the dependency resolver is
            to create that instance which is done by using the GetServices and GetService

        To use this resolver, we need to register this class
        to the "NinjectWeCommon.cs" which is provided by ninject. We do this
        under the "RegisterServices"
        
	*/
	public class NinjectDependencyResolver : IDependencyResolver  {
		private IKernel kernel;

		public NinjectDependencyResolver(IKernel kernelParam)
		{
			kernel = kernelParam;
			AddBindings();
		}

        // implemented
		public object GetService(Type serviceType)
		{
			return kernel.TryGet(serviceType);
		}

        // implemented
		public IEnumerable<object> GetServices(Type serviceType)
		{
			return kernel.GetAll(serviceType);
		}

		private void AddBindings()
		{
			kernel.Bind<IValueCalculator>().To<LinqValueCalculator2>();
		}
	}
}