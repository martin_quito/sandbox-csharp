﻿/*

*/

// ===============
// 1
// ===============
namespace Dependencies {
    public class MyEmailSender : IEmailSender {
        public void SendEmail() {
            // sending
        }
    }
    public interface IEmailSender {
        void SendEmail();
    }

    // - this is a problem. The PasswordResetHelper object
    // is tightly coupled with the interface and the MyEmailSender.
    public class PasswordResetHelper {
        public void ResetPassword() {

            IEmailSender mySender = new MyEmailSender();
            mySender.SendEmail();
        }
    }

    // - using the Dependecy Injection (DI) or Inversion of control (IoC)
    // to solve this problem
    public class PasswordResetHelperTwo {
        private IEmailSender emailSender;

        // *** THIS CONSTRUCTOR is now said to 'declare a dependency' on the IEmailSender interface *** 
        // - this object no longer has knowledge of the MyEmailSender
        public PasswordResetHelperTwo(IEmailSender emailSenderParam) {
            this.emailSender = emailSenderParam;
        }

        public void ResetPassword() {
            emailSender.SendEmail();
        }
    }
    /*
        - What is Dependency injection? Its when I pass an depedency to an object to get it to live. If I inject the dependency
        on the constructor then I can say it is a constructor-injection or I can also inject it through a public
        property, this is known as the setter-injection. And also note that the injection happens at runtime which lets me
        decide what dependency inject based on another variable.

    */
}