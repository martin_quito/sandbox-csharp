﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRONETMVC5_EssentialTools.Models
{
	public class Product
	{
		public int ProductID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string Category { get; set; }
	}

	// ==================
	// problem
	// ==================
	public class LinqValueCalculator
	{
		public decimal ValueProducts(IEnumerable<Product> products)
		{
			return products.Sum(p => p.Price);
		}
	}

	public class ShoppingCart
	{
		private LinqValueCalculator calc;
		public ShoppingCart(LinqValueCalculator calcParam)
		{
			calc = calcParam;
		}
		public IEnumerable<Product> Products { get; set; }
		public decimal CalculateProductTotal()
		{
			return calc.ValueProducts(Products);
		}
	}

	// ==================
	// solution (applying interface)
	// ==================
	public interface IValueCalculator
	{
		decimal ValueProducts(IEnumerable<Product> products);
	}

	public class LinqValueCalculator2 : IValueCalculator
	{
		public decimal ValueProducts(IEnumerable<Product> products)
		{
			return products.Sum(p => p.Price);
		}
	}

	public class ShoppingCart2
	{
		private IValueCalculator calc;
		public ShoppingCart2(IValueCalculator calcParam)
		{
			calc = calcParam;
		}
		public IEnumerable<Product> Products { get; set; }
		public decimal CalculateProductTotal()
		{
			return calc.ValueProducts(Products);
		}
	}
}