﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRONETMVC5_EssentialTools.Models;
using Ninject;
// ===================
// Dependency injection
// ===================

namespace PRONETMVC5_EssentialTools.Controllers
{
    public class HomeController : Controller
    {
		private IValueCalculator calc;
		private Product[] products = {
				new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
				new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
				new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
				new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.5M}
		};

        /*
		What are we doing?
			- "To recap, the idea is to decouple the compoenents in an MVC application,
			with a combination of interfaces and DI container that creates instances of objects
			by creating implementations of the interfaces they depend on and injecting them
			into the constructor"

		But there is a problem
			- The ShoppingCart class is tightly coupled to the LinqValueCalculator class
			and the HomeController class is tightly coupled to both ShoppingCart and
			LinqValueCalculator.
			- THE DI addresses one major problem: tightly coupled classes
		*/
        public ActionResult Index()
        {
			// there is a problem here ...
			LinqValueCalculator calc = new LinqValueCalculator();
			ShoppingCart cart = new ShoppingCart(calc) { Products = products };
			decimal totalValue = cart.CalculateProductTotal();
			return View(totalValue);

        }

		// solution, applying an interface (see Models), but there is still a problem
		public ActionResult Index2()
		{
			IValueCalculator calc = new LinqValueCalculator2();
			ShoppingCart2 cart = new ShoppingCart2(calc) { Products = products };
			decimal totalValue = cart.CalculateProductTotal();
			return View(totalValue);

			/*
				- the problem is that HomeController is still tightly coupled 
				to the ShoppingCart2 and LinqValueCalculator2
				- the solution is to use a DI contaier such as Ninject. The goal with Ninject
				in this context is to reach the point WHERE I SPECIFY that I want to 
				instantiate an implementation of the IValueCalculator interface, but
				the details of of which implem entation is required are not part of the code
				in the 'Home' Controller.
			*/
		}

		// using ninject
		public ActionResult Index3()
		{
			// "create an instance of the Ninject kernel which is the object
			// that is responsible for resolving dependencies and creating
			// new objects. When I need an object, I will use the kernel
			// instead of the new Keyword. Here is teh statement that creates
			// the kernel from the listing" (pro asp.net mvc 5 5th)
			IKernel ninjectKernel = new StandardKernel(); // this is just one implementation of the ninject kernel

			// - configure our created ninject kernel so that it understands which
			// implementation objects I want to use for each interface Iam working with
			// - it uses type parameters to build relations
			ninjectKernel.Bind<IValueCalculator>().To<LinqValueCalculator2>();
			
			// using ninject to create an object and i use '.Get'
			IValueCalculator calc = ninjectKernel.Get<IValueCalculator>();
			ShoppingCart2 cart = new ShoppingCart2(calc) { Products = products };
			decimal totalValue = cart.CalculateProductTotal();
			return View(totalValue);

			// but we still have the problem, HomeController is tithly couplied to the LinQValueCalcualtor
			/*
				- We are going to create a "Custom depedency Resolver" THe MVC will use this
				when it needs to create instances of the classes to service requests. This feature
				ensures that MVC will use ninject when creating instances of classes including those
				that are controllers
			*/
		}

		// using the dependency resolver
		public HomeController(IValueCalculator calcParam)
		{
			calc = calcParam;
		}

		public ActionResult Index4()
		{
			ShoppingCart2 cart = new ShoppingCart2(calc) { Products = products };
			decimal totalValue = cart.CalculateProductTotal();
			return View("Index", totalValue);
			/*
			What happend here is the following:
			1) The MVC framework receive the request and figure out that the process is intented
			for Home controller.
			
			2) The MVC framework asked my custom dependency resolver class to create a new isntance
			of the "HomeController" class, specifying the class using the 'Type' parameter of the
			"GetService" method.

			3) My depedency resolver asked Ninject to create a new HomeController class, passing on
			the 'Type' object to the 'TryGet' method

			4) Ninject inspected the "HOmeController" constructor and found that it has declared a dependency 
			on the "IValueCalcuator" interface, for which it has a binding.

			5) Ninject creates an instance of the "LinqValueCalculator" class and uses it to create a new
			instance of the "HomeController" class

            6) Ninject passes the HomeController instance to the custom dependecy resolver, which
            returns it to the MVC framework. The MVC Framework uses the controller
            instance to service the request

			
            What happens when I debug it
            - it never went through the GetService or GetServices
            - when the app starts, it runs the "NinjectWebCommmon"'s 
            registerServices method which creates an instance of the custom resolver
            which calls the "AddBindings" to start the bindings. And that's it. In Governet,
            there is no custom dependency resolver, it immiately does the bindings
            when it calls the "RegisterServices"
			*/
		}
    }
}