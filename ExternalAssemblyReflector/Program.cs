﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ExternalAssemblyReflector {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("*** Exernal Assembly Viewer ***");
            string asmName = "";
            Assembly asm = null;

            do {
                Console.WriteLine("\nEnter an assembly to evaluate");
                Console.Write("or enter Q to quit:");

                // get name of assembly
                asmName = Console.ReadLine();

                // Does user want to quit?
                if (asmName.ToUpper() == "Q") {
                    break;
                }
                // try to load assembly
                try {
                    asm = Assembly.Load(asmName);

                    /*
                    - you can use the friendly name of the assembly
                    - to use an external path but you need to use  "LoadFrom"
                    - The Assembly.Load() has multiple overloads
                        - See more in Chapter 15, Reflecting on Shared Assemblies
                    - remember you need to copy the assembly you are loading in the
                    app's direectory (usually in bin\Debug) which then the CLR
                    will probe in the client foler. 

                    */

                    DisplayValuesInAsm(asm);
                }
                catch {
                    Console.WriteLine("Sorry");
                }
            } while (true);
        }

        static void DisplayValuesInAsm(Assembly asm) {
            Console.WriteLine("\n*** Types in assembly ***");
            Console.WriteLine("-> {0}", asm.FullName);
            Type[] types = asm.GetTypes();
            types.ToList().ForEach(x => {
                Console.WriteLine("Type: {0}", x);
            });
        }


    }
}
