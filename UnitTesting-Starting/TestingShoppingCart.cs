﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTesting_DataToTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

/*
https://msdn.microsoft.com/en-us/library/ms182484.aspx
*/

namespace UnitTesting_Starting
{
	[TestClass]
	public class TestingShoppingCart
	{
		private Product[] products = {
				new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
				new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
				new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
				new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.5M}
		};

		[TestMethod]
		public void Sum_Products_Correctly()
		{
			// arrange
			var discounter = new MinimumDiscountHelper();
			var target = new LinqValueCalculator(discounter);
			var goalTotal = products.Sum(e => e.Price);

			// act
			var result = target.ValueProducts(products);

			// assert
			Assert.AreEqual(goalTotal, result);
			/*
			Problen
				- LinqValueCalculator is dependent on the implementation of the IDiscounter
				 which is the implementation of the MinimunDiscountHelper
				- unit test is complex and brittle
					- the brittlenes comes form the fact that my test will fail 
                    if the DISCOUNT logic in the implementation changes, even
                    though the 'LinqValueCalculator' class may well be working
                    properly. 
					- also, if the test fails, I will not know where the problem happenned,
					either in the MinimunDiscountHelper or LinqValueCalculator
				- the best unit tests ARE SIMPLE AND FOCUSES
			*/
			/*
			Solution
				- Adding A mock object to a unit test. 
				- this process is of telling Moq what kind of object you want to work with,
				configuring its behavior and then applying the object to the test target
			*/
		}
		[TestMethod]
		public void Sum_Products_Correctly2()
		{
			// arrange

			// - this tells what type of object the mock is representing
			Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();

			/*
			- use 'setup' to add a method to the mock object 'ApplyDiscount'
			- also I need to configure what parameter value it needs to be
			this can be done using 'It' class which has methods that are used
			with generic type paramters
			*/
			mock.Setup(m => m.ApplyDiscount(It.IsAny<decimal>()))	 
                // - defining the method(s)
                // - define the parameters
				.Returns<decimal>(a => a);	// defining the return
			var target = new LinqValueCalculator(mock.Object);
				// - this returns an implenentation of the IDiscountHelper  interface
				// where the ApplyDiscount method returns the value of the 'decimal'
				// parameter it is passed.
				// - this enables me to focus only on the LinqValueCalculator
				// and disregarding the implementation of the DiscountHelper

			// act
			var result = target.ValueProducts(products);
			Assert.AreEqual(products.Sum(e => e.Price), result);

			/*
			Is<T> (predicate)			
				- specifies values of type T for which the predicate will return 'true'.
			IsAny<T> ()
				- specifies any value of type 'T'
			IsInRange<T>(min, max, kind)
				- matches if the parameter is between the defined values and of type 'T'
				The final parameter is a value from the 'Range' enumeration and can be either
				'Inclusive' or "Exclusive'
			IsRegex(expr)
				- Matches a string parameter if it matches the specified regular expression
			*/
		}
		
		private Product[] createProduct(decimal value)
		{
			return new[] { new Product { Price = value } };
		}

		[TestMethod]
		[ExpectedException(typeof(System.ArgumentOutOfRangeException))]
		public void Pass_Throught_Variable_Discounts()
		{
			// arrange
			Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();

			// general ---> specific
			// the order affects the behavior of the mock

			// general
			mock.Setup(m => m.ApplyDiscount(It.IsAny<decimal>()))
				.Returns<decimal>(total => total);

			// if the delegate is true, then it uses that method which returns what is declared
			mock.Setup(m => m.ApplyDiscount(It.Is<decimal>(v => v == 0)))
				.Throws<System.ArgumentOutOfRangeException>();

			// if the vaalue is greater than 100, then return that
			mock.Setup(m => m.ApplyDiscount(It.Is<decimal>(v => v > 100)))
				.Returns<decimal>(total => (total * 0.9M));

			// if is betwene 10 and 100
			mock.Setup(m => m.ApplyDiscount(It.IsInRange<decimal>(10, 100, Range.Inclusive)))
				.Returns<decimal>(total => total - 5);

				// another way to write
				//mock.Setup(m => m.ApplyDiscount(It.Is<decimal>(v => v >= 10 && v <= 100)))
				//	.Returns<decimal>(total => total - 5);

			var target = new LinqValueCalculator(mock.Object);

			// act
			decimal FiveDollarDiscount = target.ValueProducts(createProduct(5));
			decimal TenDollarDiscount = target.ValueProducts(createProduct(10));
			decimal FiftyDollarDiscount = target.ValueProducts(createProduct(50));
			decimal HundredDollarDiscount = target.ValueProducts(createProduct(100));
			decimal FiveHundrededDollarDiscount = target.ValueProducts(createProduct(500));

			// assert
			Assert.AreEqual(5, FiveDollarDiscount, "$5 Fail");
			Assert.AreEqual(5, TenDollarDiscount, "$10 Fail");
			Assert.AreEqual(45, FiftyDollarDiscount, "$50 Fail");
			Assert.AreEqual(95, HundredDollarDiscount, "$100 Fail");
			Assert.AreEqual(450, FiveHundrededDollarDiscount, "$500 Fail");
			target.ValueProducts(createProduct(0));

		}
	}
}

