﻿/*
What?
====
	- VS has an internal unit testing tool
	- when a "test project" is created, it has no special knowledge about what is being tested. 	
    - a test-project has multiple "test fixture" s
    - a "test fixure" is a c# class that DEFINES a set of 'test methods'
        - each method is for each behavior you want to verify

How?
====
- create a new type of object that is "Unit Testing"
- [TestClass] and [TestMethod] marks the type to be used as testing units
	- testclass is like a testsuite
	- testmethod is a test
- there are many conventions in how to write unit testing, one is A/A/A
- if the unit test method fails, it throws an exception
- there is an attribute [ExpectedException] which is used when an assertion
succeeds only if the unit test (a test but not the suite) throws an exception of the type specified
by the "ExceptionType" parameter

Classes
	TestInitializeAttribute
		- fires for every testmethod
	TestCleanupAttribute
		- after every testmethod 
	TestMethodAttribute
		- ok
	TestClassAttribute
		- ok


Why?
====

*/