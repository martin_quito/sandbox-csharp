﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTesting_DataToTest.Models;
using UnitTesting_DataToTest.Controllers;

namespace UnitTesting_Tests {
    [TestClass]
    public class AdminControllerTests {
        [TestMethod]
        public void CanChangeLoginName() {
            // Arrange
           
            // create new user
            User user = new User() { LoginName = "Bob" }; 
            // create repo
            FakeRepository repositoryParam = new FakeRepository();
            // act on repo
            AdminController target = new AdminController(repositoryParam);
            string oldLoginParam = user.LoginName;
            string newLoginParam = "Joe";

            // Act
            target.ChangeLoginName(oldLoginParam, newLoginParam);

            // Assert
            Assert.AreEqual(newLoginParam, user.LoginName);
            Assert.IsTrue(repositoryParam.DidSubmitChanges);
        }
    }

    class FakeRepository : IUserRepository {
        public List<User> Users = new List<User>();
        public bool DidSubmitChanges = false;

        public void Add(User newUser) {
            Users.Add(newUser);
        }

        public User FetchByLoginName(string loginName) {
            return Users.First(m => m.LoginName == loginName);
        }

        public void SubmitChanges() {
            DidSubmitChanges = true;
        }
    }

   
}
