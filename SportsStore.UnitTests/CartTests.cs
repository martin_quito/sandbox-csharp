﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Abstract;
using SportsStore.WebUI.Controllers;
using System.Web.Mvc;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests {
	[TestClass]
	public class CartTests {
		[TestMethod]
		// =================
		// adding items the first time
		// =================
		public void Can_Add_New_Lines() {
			// arrange - create some test products
			Product p1 = new Product { ProductID = 1, Name = "P1" };
			Product p2 = new Product { ProductID = 2, Name = "P2" };

			// Arrange - create a new cart
			Cart target = new Cart();

			// Act
			target.AddItem(p1, 1);
			target.AddItem(p2, 1);
			CartLine[] results = target.Lines.ToArray();

			// Assert
			Assert.AreEqual(results.Length, 2);
			Assert.AreEqual(results[0].Product, p1);
			Assert.AreEqual(results[1].Product, p2);
		}

		// =================
		// Adding the same item
		// =================
		[TestMethod]
		public void Can_Add_Quantity_For_Existing_Lines() {
			// Arrange - create some test products
			Product p1 = new Product { ProductID = 1, Name = "P1" };
			Product p2 = new Product { ProductID = 2, Name = "P2" };
			
			// Arrange - create a new cart
			Cart target = new Cart();

			// Act
			target.AddItem(p1, 1);
			target.AddItem(p2, 1);
			target.AddItem(p1, 10);

			CartLine[] results = target.Lines.OrderBy(c => c.Product.ProductID).ToArray();

			// Assert
			Assert.AreEqual(results.Length, 2);
			Assert.AreEqual(results[0].Quantity, 11);
			Assert.AreEqual(results[1].Quantity, 1);
		}

		// =================
		// Removing feature
		// =================
		/*
		*/
		[TestMethod]
		public void Can_Remove_Line() {
			// Arrange - create some test products
			Product p1 = new Product { ProductID = 1, Name = "P1" };
			Product p2 = new Product { ProductID = 2, Name = "P2" };
			Product p3 = new Product { ProductID = 3, Name = "P3" };

			// Arrange - create a new cart
			Cart target = new Cart();

			// Arrange - add some products to the cart
			target.AddItem(p1, 1);
			target.AddItem(p2, 3);
			target.AddItem(p3, 5);
			target.AddItem(p2, 1);

			// Act
			target.RemoveLine(p2);

			// Assert
			Assert.AreEqual(target.Lines.Where(c => c.Product == p2).Count(), 0);
			Assert.AreEqual(target.Lines.Count(), 2);
		}

		// =================
		// Calculate the total cost of the items in the cart
		// =================
		[TestMethod]
		public void Calculate_Cart_Total() {
			// Arrange - create some test products
			Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
			Product p2 = new Product { ProductID = 2, Name = "P2", Price = 50M };

			// Arrange - create a new cart
			Cart target = new Cart();

			// Act
			target.AddItem(p1, 1);
			target.AddItem(p2, 1);
			target.AddItem(p1, 3);
			decimal result = target.ComputeTotalValue();

			// Assert
			Assert.AreEqual(result, 450M);
		}

		// =================
		// reset
		// =================
		[TestMethod]
		public void Can_Clear_Contents() {
			// Arrange - create some test products
			Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
			Product p2 = new Product { ProductID = 2, Name = "P2", Price = 50M };

			// Arrange - create a new cart
			Cart target = new Cart();

			// Arrange - add some items
			target.AddItem(p1, 1);
			target.AddItem(p2, 1);

			// Act - reset the cart
			target.Clear();

			// Assert
			Assert.AreEqual(target.Lines.Count(), 0);
		}

		// =================
		// the addToCart method should add the selected product to the customer's cart
		// =================
		[TestMethod]
		public void Can_Add_To_Cart() {
			// Arrange - create the mock repository
			Mock<IProductRepository> mock = new Mock<IProductRepository>();
			mock.Setup(m => m.Products).Returns(new Product[] {
				new Product {ProductID = 1, Name = "P1", Category = "Apples"},
			}.AsQueryable());

			// Arrange - create a cart
			Cart cart = new Cart();

			// Arrange - create the controller
			CartController target = new CartController(mock.Object, null); 

			// Act - add a product to the cart
			target.AddToCart(cart, 1, null);

			// Assert
			Assert.AreEqual(cart.Lines.Count(), 1);
			Assert.AreEqual(cart.Lines.ToArray()[0].Product.ProductID, 1);
		}

		// =================
		// after adding a product to the cart, the user should be redirected to the
		// Index view
		// =================
		[TestMethod]
		public void Adding_Product_To_Cart_Goes_To_Cart_Screen() {
			// Arrange - create the mock repository
			Mock<IProductRepository> mock = new Mock<IProductRepository>();
			mock.Setup(m => m.Products).Returns(new Product[] {
				new Product {ProductID = 1, Name = "P1", Category = "Apples"}
			}.AsQueryable());

			// Arrange - create a Cart
			Cart cart = new Cart();

			// Arrange - create the controller
			CartController target = new CartController(mock.Object, null);

			// Act - add a product to the cart
			RedirectToRouteResult result = target.AddToCart(cart, 2, "myUrl");

			// Assert
			Assert.AreEqual(result.RouteValues["action"], "Index");
			Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
		}

		// =================
		// the url that the USER can follow to return to the catalog should
		// be correctly passed to the Index action method
		// =================
		[TestMethod]
		public void Can_View_Cart_Contents() {
			// Arrange - create a Cart
			Cart cart = new Cart();

			// Arrange - create the controller
			CartController target = new CartController(null, null);

			// Act - call the Index action method
			CartIndexViewModel result 
				= (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

			// assert
			Assert.AreSame(result.Cart, cart);
			Assert.AreEqual(result.ReturnUrl, "myUrl");
		}

		// =================
		// only process an order if they are items in the cart AND the customer
		// has provided valid shipping details
		// =================
		[TestMethod]
		public void Cannot_Checkout_Empty_Cart() {
			// Arrange - create a mock order processor
			Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
			// Arrange - create an empty cart
			Cart cart = new Cart();
			// Arrange - create shipping details
			ShippingDetails shippingDetails = new ShippingDetails();
			// Arrange - create an instance of the controller
			CartController target = new CartController(null, mock.Object);

			// Act
			ViewResult result = target.Checkout(cart, shippingDetails);

			// Assert - check that the order hasn't been passed on to the processor
			mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());

			// Assert - check that the method is returning the default view
			Assert.AreEqual("", result.ViewName);

			// Assert - check that I'm passing an invalid model to the view
			Assert.AreEqual(false, result.ViewData.ModelState.IsValid);

		}

		// ===========================
		// injects an error into the view model to simulate a problem
		// reported by the model binder
		// ===========================
		[TestMethod]
		public void Cannot_Checkout_Invalid_ShippingDetails() {
			// Arrange - create a mock order processor
			Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();

			// Arrange - create a cart with an item
			Cart cart = new Cart();
			cart.AddItem(new Product(), 1);

			// Arrange - create an instance of the controller
			CartController target = new CartController(null, mock.Object);

			// Arrange - add an error to the model
			target.ModelState.AddModelError("error", "error");

			// Act - try to checkout
			ViewResult result = target.Checkout(cart, new ShippingDetails());

			// Assert - check that the order hasn't been passed on to the processor
			mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());

			// assert - check that the method is returning the default view
			Assert.AreEqual("", result.ViewName);

			// Assert - check that I'm passing an invalid model to the view
			Assert.AreEqual(false, result.ViewData.ModelState.IsValid);

		}

		// ===================
		// process orders when appropriate
		// ===================
		[TestMethod]
		public void Can_Checkout_And_Submit_Order() {
			// Arrange - create a mock order processor
			Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();

			// Arrange - create a cart with an item
			Cart cart = new Cart();
			cart.AddItem(new Product(), 1);

			// Arrange - create an instance of the controller
			CartController target = new CartController(null, mock.Object);

			// Act - try to checkout
			ViewResult result = target.Checkout(cart, new ShippingDetails());

			// Assert - check that the order has been passed on the processor
			mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once());

			// Assert - check that the method is returning the Completed View
			Assert.AreEqual("Completed", result.ViewName);

			// ASsert - check that I'm passing a valid model to the view
			Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
		}

	}
}
