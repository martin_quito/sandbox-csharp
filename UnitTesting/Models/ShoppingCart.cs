﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UnitTesting_DataToTest.Models
{
	public interface IDiscountHelper
	{
		decimal ApplyDiscount(decimal totalParam);

	}

	public class MinimumDiscountHelper: IDiscountHelper {

		public decimal ApplyDiscount(decimal totalParam)
		{
			if (totalParam < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			else if (totalParam > 100)
			{
				return totalParam * 0.9M;
			}
			else if (totalParam >= 10 && totalParam <= 100)
			{
				return totalParam -5;
			}
			else
			{
				return totalParam;
			}
		}
	}

	public class Product
	{
		public int ProductID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string Category { get; set; }
	}

	public interface IValueCalculator
	{
		decimal ValueProducts(IEnumerable<Product> products);
	}

	public class LinqValueCalculator : IValueCalculator
	{
		private IDiscountHelper discounter;
		private static int counter = 0;

		public LinqValueCalculator(IDiscountHelper discountParam)
		{
			discounter = discountParam;
            Console.WriteLine("ASDASD");
            System.Diagnostics.Debug.WriteLine("AAAAAAAAAAA");
			System.Diagnostics.Debug.WriteLine(string.Format("Instnace {0} created", ++counter));
		}

		public decimal ValueProducts(IEnumerable<Product> products)
		{
			return discounter.ApplyDiscount(products.Sum(p => p.Price));
		}
	}

	public class ShoppingCart
	{
		private IValueCalculator calc;
		public ShoppingCart(IValueCalculator calcParam)
		{
			calc = calcParam;
		}
		public IEnumerable<Product> Products { get; set; }
		public decimal CalculateProductTotal()
		{
			return calc.ValueProducts(Products);
		}
	}
}