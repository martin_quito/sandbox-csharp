﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UnitTesting_DataToTest.Models {
    public class User {
        public string LoginName { get; set; }
    }

    public interface IUserRepository {
        void Add(User newUser);
        User FetchByLoginName(string loginName);
        void SubmitChanges();
    }

    public class DefaultUserRepository : IUserRepository {
        public void Add(User newUser) {
            throw new NotImplementedException();
        }
        public User FetchByLoginName(string loginName)
        {
 	        throw new NotImplementedException();
        }
        public void SubmitChanges()
        {
 	        throw new NotImplementedException();
        }
    }
    
}