﻿/*
 * What about validation?
 *	- it is typically applied to the domain model type
 *	- you define the validation criteria in one place and have it take effect anywhere within the app
 *	- ASP.NET MVC supprots DECLARATIVE VALIDATION RULES from the System.ComponentModel.DataAnnotations namespace
 *	- THE VALIDATION HAPPENS DURING THE MODEL-BINDING PROCESS
 */

using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC1_PartyInvite.Models {
    public class GuestResponse {
		[Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }
		[Required(ErrorMessage = "Please enter your email address")]
		[RegularExpression(".+\\@.+\\..+", ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }
		[Required(ErrorMessage="Please enter your phone number")]
        public string Phone { get; set; }
		[Required(ErrorMessage = "Please specifi where you'll attend")]
        public bool? WillAttend { get; set; }
    }
}