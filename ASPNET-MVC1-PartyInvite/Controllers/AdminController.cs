﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestingDemo
{
    public class AdminController : Controller
    {
		private IUserRepository repository;

        // GET: Admin
        public AdminController(IUserRepository repo)
        {
			repository = repo;
        }

		// this is the "TEST-FIXURE"
		public ActionResult ChangeLoginName(string oldName, string newName)
		{
			User user = repository.FetchByLoginName(oldName);
			user.LoginName = newName;
			repository.SubmitChanges();
			// render some view to show the result
			return View();
		}
    }
}

namespace TestingDemo
{
	public class User
	{
		public string LoginName { get; set; }
	}
	public interface IUserRepository
	{
		void Add(User newUser);
		User FetchByLoginName(string loginName);
		void SubmitChanges();
	}
	public class DefaultUserRepository : IUserRepository
	{

		public void Add(User newUser)
		{
			throw new NotImplementedException();
		}

		public User FetchByLoginName(string loginName)
		{
			throw new NotImplementedException();
		}

		public void SubmitChanges()
		{
			throw new NotImplementedException();
		}
	}
}

namespace TestingDemo.Test
{
	[TestClass]
	public class AdminControllerTests
	{
		[TestMethod]
		public void CanChangeLoginName()
		{
			// arrange (set up a scenario)
			User user = new User() { LoginName = "Bob" };
			FakeRepository repositoryParam = new FakeRepository();
			repositoryParam.Add(user);
			AdminController target = new AdminController(repositoryParam);
			string oldLoginParam = user.LoginName;
			string newLoginParam = "Joe";

			// act (attempt the opration)
			target.ChangeLoginName(oldLoginParam, newLoginParam);

			// Assert (verify the result)
			Assert.AreEqual(newLoginParam, user.LoginName);
			Assert.IsTrue(repositoryParam.DidSubmitChanges);
		}
	}

	class FakeRepository : IUserRepository
	{
		public List<User> Users = new List<User>();
		public bool DidSubmitChanges = false;
		public void Add(User user)
		{
			Users.Add(user);
		}
		public User FetchByLoginName(string loginName)
		{
			return Users.First(m => m.LoginName == loginName);
		}
		public void SubmitChanges()
		{
			DidSubmitChanges = true;
		}
	}
}

