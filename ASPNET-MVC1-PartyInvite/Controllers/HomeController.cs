﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPNET_MVC1_PartyInvite.Models;

namespace ASPNET_MVC1_PartyInvite.Controllers
{
    public class HomeController : Controller
    {
        // returning a stirtng
		// this is called an ACTION METHOD
        public string Index()
        {
            return "Hello World";
        }

        // using a view
        public ViewResult Index2() {
            return View();	// returns default view "Views/Home/Index2.cshtml/

        }

        // using view bag
        public ViewResult Index3() {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            return View();
        }

		[HttpGet]
		public ViewResult Index4()
		{
			return View();
		}

		[HttpGet] // this attribute tells MVC that this action method only accepts GET requests
        // simple data-entry application
		public ViewResult RsvpForm()
		{
            return View();
        }

		[HttpPost] // This attribute tells MVC that his action method only accepts POST requests
		// - model binding happening here, it will parse the key/value data to populate dmoain model types
		// So, in other words, its the opposite of of an HTML helper method. Model binding is from
		// Data to model, wherewas the other is from model to data.
        public ViewResult RsvpForm(GuestResponse guestResponse) {
			if (ModelState.IsValid)
			{
				// TODO Email resposne tot he party organizer
				return View("Thanks", guestResponse);
			}
			else
			{
				// there is a validation error
				return View();
			}
        }
    }
}