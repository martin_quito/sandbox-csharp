﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ControllerAndActions.Controllers;
using System.Web;
using System.Web.Mvc;

namespace ControllerAndActions_Test {
    [TestClass]
    public class ActionTests {
        // ==============
        //  testing a simple view
        // ==============
		/*
			- to test the view athat an action method renders, you 
			can inspect the ViewResult object that it returns. You test
			if the ViewResult contains the correct view file name. This test
			guarantees that the action method is chooseing the correct view file.
		*/
        [TestMethod]
        public void ControllerTest() {
            // arrage 
            DerivedController t = new DerivedController();

            // act
            ViewResult result = t.SimpleHTML();

            // Assert
            Assert.AreEqual("HomePage", result.ViewName);
				/*
				- if "view()" then use "" which is how it
				signals razor to use the default view
				- the mvc framework actually gets the name of the
				method from the "routedata.values["action"]" value
				*/
        }

        // ==============
        // testing data being passed - view model objects
        // ==============
        [TestMethod]
        public void ViewSelectionTest() {
            // arrange
            DerivedController t = new DerivedController();
            // act
            ViewResult result = t.ViewModel();

            // assert
            Assert.AreEqual("", result.ViewName);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(System.DateTime));
        }

		// ==============
		// testing view bag
		// ==============
		[TestMethod]
		public void TestingViewBag() {
			DerivedController t = new DerivedController();
			ViewResult result = t.MyViewBag();
			Assert.AreEqual("Hello", result.ViewBag.Message);
		}

		// ==============
		// testing a redirect on a literal url
		// ==============
		[TestMethod]
		public void TestOnLiteralUrl() {
			DerivedController t = new DerivedController();
			RedirectResult result = t.Redirect();
			Assert.IsFalse(result.Permanent);
			Assert.AreEqual("/Example/Index", result.Url);
		}

		// ==============
		// testing on a system routing url
		// ==============
		[TestMethod]
		public void TestOnASystemRoutingUrl() {
			DerivedController t = new DerivedController();
			RedirectToRouteResult result = t.Redirect3();
			Assert.IsFalse(result.Permanent);
			Assert.AreEqual("Example", result.RouteValues["controller"]);
			Assert.AreEqual("Index", result.RouteValues["action"]);
			Assert.AreEqual("MyID", result.RouteValues["Id"]);
		}

        // ==============
        // testing routed redirection
        // ==============
        [TestMethod]
        public void Routing() {
            DerivedController t = new DerivedController();
            RedirectToRouteResult result = t.Redirect3();
            Assert.IsFalse(result.Permanent);
            Assert.AreEqual("Example", result.RouteValues["controller"]);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("MyID", result.RouteValues["ID"]);
        }

    }

}
