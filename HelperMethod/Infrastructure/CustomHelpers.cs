﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

/*
HtmlHelper
    Properties
        RouteCollection - returns the set of routes by the app
        ViewBag - returns view bag data passed from the aciton method to the
            view that has called the helper method
        ViewContext - returns a "ViewContext" object which provides access
            to details of the request and how it has been handled

ViewContext
    Properties
        Controller - returns the controller processing the current request
        HttpContext - returns the HttpContext object that describes the current
                    request
        IsChildAction - Returns 'true' if the view that has called the helper
                        is being rendered by a child action 
        RouteData   - returns the routing data for the request
        View        - returns the instance of the "IView" implementation that
                    has called the helper method.

TagBuilder
	- build HTML strings without needed to deal with all the escaping
	and special characters
    - part of the "System.Web.WebPages.Mvc" Assembly, but uses a feature 
	"type forwarding" as to appeart as thought it is part of the "System.Web.Mvc" assembly

    - Properties
        InnerHtml - set the contents of the elemnets as an HTML string. The
                    value assgiend to this property will not be encoded which
                    means it can be used to nest HTML elements
        SetInnerText(string)
                - similar to INnerHTml but its is encoded
        AddCssClasss(string)
                - adds a CSS class to the HTML element
        MergeAttribute(string, string, bool)
                - adds attrbiutes to the HTML element. The first param is the
                name of the attr and the second is the value. The bool specifies
                if the existing attr of the same name should be replaced.

*/

/*

The result of the HTML helper method is a "MvcHtmlString" object contains the
content that is retuned to the View engine so that it can be inserted into the
response. 

*/

namespace HelperMethod.Infrastructure {
    public static class CustomHelpers {
        public static MvcHtmlString ListArrayItems(this HtmlHelper html,
            string[] list)
        {
            TagBuilder tag = new TagBuilder("ul");
            foreach(string str in list) {
                TagBuilder itemTag = new TagBuilder("li");
                itemTag.SetInnerText(str);
                tag.InnerHtml += itemTag.ToString();
            }
            return new MvcHtmlString(tag.ToString());
        }
    }
}