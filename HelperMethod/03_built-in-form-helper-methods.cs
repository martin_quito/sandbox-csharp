﻿/*

Why use Built-in Form Helper methods?
    - Helpers are there for convenience, rather than because they
    create essential or special HTML. By using these guys, it ensures that the 
    HTML is in sync with the application so that for example, changes
    in routing configuration will be reflected automatically in your forms. 
    Another reason would be showing standard and compliant HTML. 

How to create form elements?	
	- Html.BeginForm, Html.EndForm
		- these two generate valid action attribute for the form that is 
		based on the routing mechanism for the app
	- Html.BeginForm
		- they are 13 oveloads
	
How to specify the route used by a form?
	- when you use the "BeginForm" method, MVC finds the first route
	in the routing configuration that can be used to geneate a URL
	that will target the required action and controller. If you want
	to ensure that a particular route is used, then you can use
	the 'BeginRouteForm' helper method instead. (see page 607 first-book)

How?
	- example 1, manually, standardly created form
		- Person.cs
		- HomeController, CreatePerson, CreatePerson (HttpPost)
		- CreatePerson.cshtml
		- Layout.cshtml

	- example2, using the Html.BeginForm and Html.EndForm
		- Person.cs
		- HomeController, CreatePerson2, CreatePerson2 (HttpPost)
		- CreatePerson2.cshtml
		- Layout.cshtml

	- example 3, using a more common way of Html.BeginForm, known as the
		"self-closing form", simpel form
		- Person.cs
		- HomeController, CreatePerson3, CreatePerson3 (HttpPost)
		- CreatePerson3.cshtml
		- Layout.cshtml

	- example 4, compelx self-closing form
		- Person.cs
		- HomeController, CreatePerson4, CreatePerson4 (HttpPost)
		- CreatePerson4.cshtml
		- Layout.cshtml

	
*/