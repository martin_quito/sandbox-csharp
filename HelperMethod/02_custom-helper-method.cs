﻿/*
Two techniques in creating custom helper methods?
	- inline helper method
	- external helper method

Everything about InlineHelper method?
    - this is good for only one view but its not good if it needs to be
    reused in other views or it becomes complex where the view becomes
    hard to read.
	- example 
		- see index2.html
		- homecontroller.cs, index2

Everything about External helper method
	- is a bit awkward to write because C# doesn't naturally handle
	HTML elements generation elegantly
	- expressed as a C# extension method
	- we use external helper methods BECAUSE they are simple and keep
	formatting consisttent
	- ensure that the namespace that contains the helper extension method is
	in scope. If you are developing a lot of custom helpers then I should
	want to add the namespaces that contains them to the "/Views/Web.config"
    - example
		- this external helper method takes an array of strings and generates
		HTML
		- see index3.html, homecontroller.cs, index3
		- customhelper.cs

When to use helper methods?
	- reduce the amount of dupication in views
	- for complex markup and content, you can use partial views and
	child action when I need to manipulate the model
	- keep my helpers simple

How to  manage string enconding in a helper method
    - razor encodes data values automatically when they are used in view,
    but helper methods need to be able to generate HTML, as a consequence,
    they are given a higher level of trust by the view engine

		Problem:
		I have a string with html tags
			This is an HTML element: <input>
	
		In the model, Im going to display in two forms
			@Model
				- the string rendered in the browser is encoded
				"
				This is the Message:
				This is the HTML element <input>
				"

			@Html.DisplayMEssage(model)
				It uses this 
				public static MvcHtmlString DisplayMessage (this HtmlHelper html\
				, string msg) {
					string result = String.Format(This is the message: <p>{0}</p>, msg)
				}

				- The helper method is trued to generate safe content, so it
				will rendered as
				"
				This is the Message: 
				This is the Html Element [------]
				" 
				
		

        - to make the HTML helper method return encoded html, you can
        change the return type to "string" and use "toString()" This 
        technique causes Razor to encode all of the content that is returned
        by the helper which is a problem when you are genertic HTML elements. 

			
				public STRING MvcHtmlString DisplayMessage (this HtmlHelper html\
				, string msg) {
					return msg
				}

				This returns:

				"
				This is the message: <p> This is an HTml element: <input></p>
				"
					
	
		Looks good but our <p> is still encoded. When I want to use both, 
		where the  HTML helper method needs to output html to be rendered 
		and data to be encoded, I can use "HtmlHelper.Encode"
            
			string encodedMessage = html.Encode(msg);
			string result = String.Format("message: <p>{0}</p>", encodedMessage);
			return new MVcHtmlString(result);

*/