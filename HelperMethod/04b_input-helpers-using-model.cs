﻿/*
// ===========
Example 1 - using the view model's property to partiually generate html
// ===========
	- HomeController.cs, InputHelpers 2, INputHelpers2 (HttpPost)
	- Laytout.cshtml
	- InputHelpers2.cshtml

	The problem here is that we still to ensure that the value I pass as the 
	first arrgument whic is the 'name' attribute need to match with the 
	name of the mode's property. If not, then the MVC will not be able to 
	reconstruct the model object from the form data because the 'name 
	attribute and the forms values of the 'input' elements will not match. Solution
	is to gene

// ===========
Example 2 - generating the input element from a model property (not strongly type yet)
// ===========
	- HomeController.cs, InputHelpers 3, INputHelpers3 (HttpPost)
	- Laytout.cshtml
	- InputHelpers3.cshtml

// ===========
// Using storngly type input helpers
// ===========
	<div class="dataElem">
		<label> Personid </label>
		@Html.TextBoxFor(m => m.PersonId)
	</div>

	<div class="dataElem">
		<label>First Name</label>
		@HTml.TextBoxFor(m => m.FirstName)
	</div>
	- compare the previous with Example 1
	
	- is the same as the previous section with the exception that I don't need
    to provide the attribute 'name'. It redecues the changes of causing an error
	by mistpying a property name



// ===========
// Strongly types - Select elements
// ===========
	- HomeController.cs, InputHelpers 4, INputHelpers4 (HttpPost)
	- Laytout.cshtml
	- InputHelpers4.cshtml

	
// ===========
// Storngly type - Person.Role
// ===========
	- HomeController.cs, InputHelpers5, INputHelpers5 (HttpPost)
	- Laytout.cshtml
	- InputHelpers5.cshtml


*/