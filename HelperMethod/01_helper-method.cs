﻿/*
Why?
	- allows you to package chunk sof code and markup so that they 
	can be used throughout your MVC app

When to use helper methods?
    - only to reduce duplication in views and ONLY for the simples content. For
    more complex markup and content, use "partial views" and use "child actions"
    when manipulation of model data is needed. Keep your helper methods as simple
    as possible. 
  
Show the problem that helpe method solves?
	- HomeController.cs, Index, 
	- Home/Index.cshtml


How to consome them?
	- an extension method is available for use only when the namespace that contains
	it is in scope.

*/