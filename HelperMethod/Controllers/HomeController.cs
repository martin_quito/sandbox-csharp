﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelperMethod.Models;

namespace HelperMethod.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Fruits = new[] { "Apple", "Orange", "Pear"};
            ViewBag.Cities = new[] { "New York", "London", "Paris" };
            string message = "This is an HTML element: <input>";
            return View((object)message);
        }


		public ActionResult Index2() {
			ViewBag.Fruits = new[] { "Apple", "Orange", "Pear" };
			ViewBag.Cities = new[] { "New York", "London", "Paris" };
			string message = "This is an HTML element: <input>";
			return View((object)message);
		}

		public ActionResult Index3() {
			ViewBag.Fruits = new[] { "Apple", "Orange", "Pear" };
			ViewBag.Cities = new[] { "New York", "London", "Paris" };
			string message = "This is an HTML element: <input>";
			return View((object)message);
		}

		// ===========

		// ===========
        public ActionResult CreatePerson() {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePerson(Person person) {
            return View(person);
        }

		public ActionResult CreatePerson2() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult CreatePerson2(Person person) {
			return View(person);
		}

		public ActionResult InputHelpers() {
			return View();
		}

		[HttpPost]
		public ActionResult InputHelpers(Person person) {
			return View();
		}

		public ActionResult InputHelpers2() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult InputHelpers2(Person person) {
			return View(person);
		}

		public ActionResult InputHelpers3() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult InputHelpers3(Person person) {
			return View(person);
		}

		public ActionResult InputHelpers4() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult InputHelpers4(Person person) {
			return View(person);
		}


		public ActionResult InputHelpers5() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult InputHelpers5(Person person) {
			return View(person);
		}
    }
}