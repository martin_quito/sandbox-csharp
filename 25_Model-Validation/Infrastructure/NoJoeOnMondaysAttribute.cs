﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using _25_Model_Validation.Models;

namespace _25_Model_Validation.Infrastructure {
    public class NoJoeOnMondaysAttribute : ValidationAttribute{
        public NoJoeOnMondaysAttribute() {
            ErrorMessage = "Joe cannot book appointments on Mondays";
        }
        public override bool IsValid(object value) {
            Appointment_5 app = value as Appointment_5;
            if (app == null || string.IsNullOrEmpty(app.ClientName) || app.Date == null) {
                return true;
            }
            else {
                return !(app.ClientName == "Joe" && app.Date.DayOfWeek == DayOfWeek.Monday);
            }
        }
    }
}