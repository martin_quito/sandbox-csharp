﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace _25_Model_Validation.Infrastructure {
    public class MustBeTrueAttribute : ValidationAttribute {
        public override bool IsValid(object value) {
            return value is bool && (bool)value;
        }
    }
}