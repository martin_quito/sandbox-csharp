﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using _25_Model_Validation.Infrastructure;

namespace _25_Model_Validation.Models {
    [NoJoeOnMondays]
    public class Appointment_6 {

        [Required]
        public string ClientName { get; set; }

        [DataType(DataType.Date)]
        //[Required(ErrorMessage="Please enter a date")]
        //[FutureDate(ErrorMessage="please enter a date in the future")]
        public DateTime Date { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must accept the terms")]
            // this won't work with the standard client side validation in mvc3
        //[MustBeTrue(ErrorMessage="You must accept the terms!!!")]
            // this work with the standard client side validation in mvc3
        public bool TermsAccepted { get; set; }
    }
}