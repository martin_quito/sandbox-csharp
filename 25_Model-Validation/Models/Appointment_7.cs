﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using _25_Model_Validation.Infrastructure;

namespace _25_Model_Validation.Models {
    [NoJoeOnMondays]
    public class Appointment_7 {

        [Required]
        public string ClientName { get; set; }

        [DataType(DataType.Date)]
        //[Required(ErrorMessage="Please enter a date")]
        //[FutureDate(ErrorMessage="please enter a date in the future")]
        [Remote("ValidateDate", "Home")]
        public DateTime Date { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must accept the terms")]
        [MustBeTrue(ErrorMessage="You must accept the terms!!!")]
        public bool TermsAccepted { get; set; }
    }
}