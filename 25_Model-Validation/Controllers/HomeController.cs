﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _25_Model_Validation.Models;

namespace _25_Model_Validation.Controllers
{
    public class HomeController : Controller
    {
        // ==============
        // - Explicitly Validation a Model
        // ==============
        public ViewResult MakeBooking() {
            return View(new Appointment { Date = DateTime.Now });
        }

        /*
            - the user must provide a name
            - user must provide a date (in the mm/dd/yyy format) that is in the future
            - user must have checked the check box to accept the terms and conditions
        */
        [HttpPost]
        public ViewResult MakeBooking(Appointment appt) {

            if (string.IsNullOrEmpty(appt.ClientName)) {
                ModelState.AddModelError("ClientName", "Please enter your name");
            }

            if (ModelState.IsValidField("Date") && DateTime.Now > appt.Date) {
                ModelState.AddModelError("Date", "Please enter a date in the future");
            }

            if (!appt.TermsAccepted) {
                ModelState.AddModelError("TermsAccepted", "You must accept the terms");
            }

            // a model-level error
            if (ModelState.IsValidField("ClientName") && ModelState.IsValidField("Date")
                && appt.ClientName == "Joe" && appt.Date.DayOfWeek == DayOfWeek.Monday) {
                    ModelState.AddModelError("", "Joe cannot book appointsment on Modnays");
            }

            if (ModelState.IsValid) {
                // statements to store new Appoint in a repository
                // woudl go here in a real project
                return View("Completed", appt);
            }
            else {
                return View();
            }
                        
        }

        // ==============
        // validation rules using metadata
        // ==============
        public ViewResult MakeBooking2() {
            return View(new Appointment_2 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking2(Appointment_2 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        // ==============
        // custom validation attgribute from scratch
        // ==============
        public ViewResult MakeBooking3() {
            return View(new Appointment_3 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking3(Appointment_3 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        // ==============
        // derivie from built-in validation attrtibutes
        // ==============
        public ViewResult MakeBooking4() {
            return View(new Appointment_4 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking4(Appointment_4 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        // ==============
        // custom model validation attribute
        // ==============
        public ViewResult MakeBooking5() {
            return View(new Appointment_5 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking5(Appointment_5 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        // ==============
        // client side validation
        // ==============
        public ViewResult MakeBooking6() {
            return View(new Appointment_6 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking6(Appointment_6 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        // ==============
        // performing remote validation
        // ==============
        public ViewResult MakeBooking7() {
            return View(new Appointment_7 { Date = DateTime.Now });
        }

        [HttpPost]
        public ViewResult MakeBooking7(Appointment_7 appt) {
            if (ModelState.IsValid) {
                return View("Completed", appt);
            }
            else {
                return View();
            }
        }

        public JsonResult ValidateDate(string Date) {
            DateTime parsedDate;
            if (!DateTime.TryParse(Date, out parsedDate)) {
                return Json("Please enter a valid date (mm/dd/yyyy)"
                            , JsonRequestBehavior.AllowGet);
            }
            else if (DateTime.Now > parsedDate) {
                return Json("Please enter a date in the future",
                            JsonRequestBehavior.AllowGet);
            }
            else {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

    }
}