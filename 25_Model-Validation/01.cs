﻿/*
What is the process of mdoel validation?
    - is the process of binding the data received to a model
    and if if errors exist, provide useful info

Why?
    - not receive bad data to not damage the app 

What is the process?
    - first is validating the data, receive or reject
    - second, helping the user know and/or correct the data 
   
How to explicitly validation a model
    I do a number of if statements for each property. For example
    String   =>     string.IsNullOrEmpty
    Model.IsValidField  => whether the model binder was able to assign a value
    ...
    After that I called "ModelState.IsValid" which returns true if
    I called the 'Model.State.AddModelError' during the checks OR
    if the model binder had any problems creating the Appointment object.
    If it faisl, I should render the default view
    
How to display validation errors to the users    
    Calling back the View where the tempalted view hlpers will generate html
    for any validation errors. 
        
        a class name is used by mvc is 'input-validation-error'
    
How to display validation messages?
    in the view use "Html.ValidationSummary" which if any errors, it will display them
    with a class "validation-summary-errors"
   
    Html.ValidationSummary()        - generates a summary for all validation error
    Html.ValidationSummary(bool)        - if the bool parameter is true, then only model-level
                                        are display. If parameter is false, then all errors are
                                        shown                                        
    Html.ValidationSummary(string)      - Displays a message (contained in the string parameter)
                                        before a summary of all the validation errors
    Html.ValidationSummary(bool, string) - Displays a message before all the validation
                                            errors. If the bool parameter is true, only
                                            model-level errors will be shown.

    Model-level-error vs Property-level-error is good to know. model level are good when
    an interaction between two or more property values is problematic. 

        ModelState.AddModelError("", "Joe cannot book appoint ...")

How to display property-level validation messages?
    The reason why you would want to restrict the validation suymmary to model-level errors
    is to display property-level errors alongside the fields themseleves in which you
    will not want to duplicate the property-specific messages. 

    You can use the @Html.ValidationMessageFor(m => ...)

    This generates a class "field-validation-error"
        
Validation techniques
    - explicitly validating inside the action
        - see before
    - performing validation in the model binder
        - the default-model-binder performs validation as part of the binding process.
        For example, if i enter an invalid date and submit the form. The error display
        for the Date field has been added because the model-binder wasn't able to create
        'DateTime' object from the empty field posted in the form. So, the default
        model binder can perform some basic validation for each of the properties in
        the model object.

        DefaultModelBinder methods for adding validation to the Model-Binding-Process
        
        OmModelUpdated  
            - called when the binder has tried to assign values to all
            of the properties in the model object. 
            - default implementation: applies the validation rules defined by the
            model 'metadata' and registers any errors with 'ModelState'. I describe
            the use of 'metadata' for validation later.
        
        SetProperty
            - called when the binder wants to apply a value to a specific property
            - Default implementation: if the property cannot hold a null value and there
            was no value to apply, then the 'The <name> field is required' error is
            registered with 'ModelState'. If there is a value but it cannot be parsed
            then the 'The value <value> is not valid for <name>' error is registered.
            
        These two methods can be ovewrriden to push validation logic into the
        binding process when creating a custom model binder. But, it feels validation
        shouldn't go here because of MVC patterns, it should be with the Model. Althought,
        as with so much in an MVC app, it is a matter of personal tatest and preference.
        
    - specifying validation rules using metadata
        mvc supports the use of metadata to express model validation rules. The advanctate of
        this is that the validation rules are ENFORCED ANYWHERE that the binding process is
        applied throughout the application, not just in a single action method. The validation
        attributes are detected and enforced by the built-in 'default model binder class'
        'DefaultModelBinder'. 

            Built-in validation attributes
            ---------------------------------

            Compare     
                - [Compare("MyOtherProperty")]
                - two properties must have the same value. This is useful when you
                ask the user provide the same info twice, such as an email address or
                password
            Range       
                - [Range(10, 20)]
                - a numeric value (or any property type that implement IComparable)
                must not lie beyond the specified min and max values. To specify a boundary
                on only one side, use a 'MinValue' or 'MaxValue' constant
                [Range(int.MinValue, 50)]

            RegularExpression 
                - [RegularExpression("pattern")]
                - single value must match the specified regular expression pattern
                Note that the pattern has to match the 'entire' user-supplied value
                not just a substring withing it. By default, it matches case
                sensitively, but it can changed by applying the (?i) modifier
                [RegularExpression("(?i)pattern")]

            Required
                - [Required]
                - value must not be empty or be a string consitent only of spaces. If you
                want to treat whitespace as valid use
                [Required(AllowEmptyStrings=true)]

            StringLength
                - [StringLength(10)]
                - a string value must not be longer thant he specified max length. You can 
                also specify a min length: [StringLength(10, MinimumLength = 2)]

            * All of the validation attributes support specify custom error message by
            seting a value for the 'ErrorMessage'
                [Required(ErrorMessage="Please enter a date")]
            If there are no custom messages, the defaults are used. 
            
    - create custom property validation attribute
        - class derivces from the 'ValidationAttribute' class and implementing custom
        validation logic. You will override the 'IsValid' method which is called by
        the model-binder to validate properties to which the attribute is applied passing
        the value that the user has provided as the parameter
        
    - deriving from the built-in validation attrtibutes
        - you can also derive new classes from the built-in attributes which
        gives me the ability to extend their behavior. You again dervied from a 
        built-in attriubute like 'RequiredAttribute' and override the 'IsValid' method
            
    - creating a model validation attribute
        You can create a validation attribute of a model-level type.  

    - self-validating models
        the model implements 'IValidatableObject' where there is one method 
        "Validate(ValidationContext c)" The validationContext type is not MVC-specific
        and isn't a great deal of use. The nice thinga bout this that you can
        have all your property and model level errors in once place and in the same file
        as the model. 

How to do client-side validation?
    - note that client side validation is focused on validating individual properties and
    in fact, it is hard to setup model-level client-side validation using the built-in support
    that comes with the MVC framework. To that end, most mvc apps use the client-side validation
    for property-level issues and rely on server-side validation for the overall model.

    - to enable it, you need to modify the 'Web.config' file 
        <add key="ClientValidationEnabled" value="true" />
        <add key="UnobtrusiveJavaScriptEnabled value="true" />

        both need to be true for client-side validation to  workk. 
        you can also confiogure client-side validation on per-view basis by setting
        the "HtmlHelper.ClientValidationEnabled" and "HtmlHelper.UnobtrusiveJavaScriptEnabled"
        in a razor code block. 

        you need:
            jquery-1.10.2.js
            jquery.validate.js
            jquery.validate.unobtrusive.js

        The implementation of IValidateObject interface has no effect on client-side validation

Understanding How client side validation works?
    - one benefit is that I don't have to write JS code. The templated html helpers generate
       data attributes so they can be interpreted by the jquery validation library to
       provide the client-side validation. 
    - a nice feature about this mvc client side validation is that the same attributes used 
      to specify validation are applied at the client and at the server. So if js is disabled,
      the model is still subject to the same validation without require any additional effort.

Avoid conflcits with browser validation
    - if jquery and browser are both validting, you can turn off the browser by 
    adding 'novalidate'

Remote Validation
    - an example would be to remotely validate whether a username is avaiable in application.
    The benefits would be 
        1) only some props will be validated remotely and the client-side validation 
        benefits still apply to all the other data values
        2) request is reliatevly lightweight and is focused in validation rather than
        processing an entire object
        3) it is performed in the background
    - it is a balannbce between clint-side and server side. 
    - the first step is to to create an action method that can validate
    one of the model properties. Note that there are some limitations
    of what you can validate with the 'standard client-side validation
    features'. 
        "The MVC client-validation features are built on top of the 
        jQuery validation library. If you prefer, you can use the
        Validation library directly and ignore the MVC features. The
        Validation library is flexible and feature-rich. It is well
        worth exploring, if only to understand how to customize the
        MVC features to take best advantage of the available 
        validation options."
    
        The action method that suports 'remote validation' must
        return the 'JsonResult' type which tells the MVC framework
        that Im working with JSON. Also the action method needs to have
        the parameter name match with the data field that is validated. 
        
            For example, public JsonResult ValidateDate(string Date) {...}
            - Its better to use a string than a DateTime object because
            I could have taken advantaaged of the model so the parameter
            to my action method would be a 'DateTime' but doing so it
            would mean that the validation method wouldn't be called if the
            user entered a nonsense value like 'apple'. This is because the
            model binder wouldn't have been able to create a 'DateTime'
            object from 'apple' and throws an exception when it tries. The
            remote validation feature DOESN't have a way to express that
            exception and so it is quietly discarded. This has an 
            unfortunate effect of not highlighting the data field and so
            creating the impression that the value that the user has entered
            is valid. As a general rule, the best approach to remote validation
            is to accept a 'string' parameter in the action method and 
            perform anyt time converisons, parsing, or model binding
            explicitly. 

        To use the action-method-remote-validation method, I need to
        apply the 'Remote' attribute to the property I want to validate
        in the model. The arguments for that atttribute are the name of
        the 'action' and the 'controller' THAT should be used to generate
        the URL that the JS validation library will call to perform the
        validation. 

        

*/