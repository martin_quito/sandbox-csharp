﻿/*
What is the purpose of views?
    - allow to render parts of your domain model as a user interface

How to add "Dynamic Content" to Views?
    - dynamic content is generated at runtime which can be different
    for each and every request
    - this is diff than 'static content' such as HTML

    - You can use the following methods to add dynamic content
        InlineCode
            - use for small, self-contained pieces of view logic
            - not covered in this project
        Html Helper MEthods
            - use to generate single html elements or small collections of them
            , typically based on view model or view data values. THe mvc framework
            provies a number of useful of HTML helpers and its easy to create your own.
            - not coveretd in this project
        Sections
        PartialVIews
        ChildACtions
*/