﻿/*
Why?
    - includes code fragments
    - supports layouts
    - compiled to optimized performance 


How does Razor render 'View's?
    - views are translated into C# classes and then compiled
        - compiles improves performance
        - the compile class derives from "System.Web.Mvc.WebViewPage"
        - the compile process in the initial request to the MVC app which compiles
        all the views
        - compiled means from .cshtml to .cs files 
        - you can find the geneated C# and compile classes in the 
            C:\Users\<loginName>\AppData\Local\Temp\Temporary ASP.NET files 
            
            generated C# file from the view file
                - class is derived from the WebViewPage<T> where T
                is the model type 

                - both the "Write" and "WriteLiteral" write content to the "TextWriter" 
                object.  This sis the same object that is passed to the
                "IView.Render" method 

                - THE GOAL of a compiled Razor view is to generate the static and 
                dynamic content and send it to the client via the "TextWriter"

How to configure the view search locations?
    - know that when razor looks for the view, its not looking for the
    .cshtml files; instead, its looking for the compiled C# classes that 
    represent these views.
        - the .cshtml files are just templates containing C# statemnts

    - you can change the view files that Razor searches for by 
    creating a subclass of "RazorViewEngine" which is the Razor
    
    - example
        - see page 571 from first-book
    
*/