﻿/*
Why?
    - demostrate how the request processing pipeline works and
    complete my knowledge of how the MVC framework operates
    - understand how much freedom view engines have in translating
    a "ViewResult" into a response to the client

What is the role of a view engine?
    - translate request that are for views into ViewEngineResult objects
        - purpose is to produce 'ViewEngineResult' that contains either a
        'IView' or a list of the places that searched for a suitable view
   
How?
    - view engines implement the IViewEngine
        - you can use the built-in view engine - Razor View Engine
    
    - IViewEngine
        
        public interface IViewEngine {
            ViewEngineResult FindPartialView(ControllerContext controllerContext
                string partialViewName, bool useCache);

            ViewEngineResult FindView(ControllerContext controllerContext, 
                string viewName, string masterName, bool useCache);
        
            void ReleaseView(ControllerContext controllerContext, IView view)
        }


        FindView, FindPartialView
            - the params describe the request and the controller that
            processed it, the name of the view, and its layout, and whether
            the view engine is allow to reuse the previous result from its cache
            
            - these methods are called when the "ViewResult" is being processed
            
        ReleaseView
            - called when view is no longer needed

    - the "ViewEngineResult" allows a view engine to respond to the MVC framework
    when A VIEW IS REQUESTED
        
        public class ViewEngineResult {
            
            public ViewEngineResult(IEnumerable<string> searchedLocations){}
            public ViewEngineResult(IView view, IViewEngine viewEngine){}
            public IEnumerable<string> SearchedLocations {get; private set;}
            public IView View {get; private set;}
            public IViewEngine ViewEngine {get; private set;}
        }
        
        - the first const, if your view engine cannot prpovidfe a 'view' for a request
        , then you use this constructor

        - the secon const is used if you want your view engine to provide
        a 'view' for a request

    - IView
        
        public interface IView{
            void Render (ViewContext viewContext, TextWriter writer)
        }

        - an implementation of this is passed to the constructor of the
        ViewEngineREsult object, which is then returned from the view-engine methods.
        The MVC framework then calls the 'Render' method.

        - ViewContext - provides info about the request from the client

            Controller
            RequestContext
            RouteData
            TempData
            View
            ViewBag
            ViewData
                - returns a ViewDataDictionary object
                ViewDataDictionary
                    Props
                        Keys 
                            - collection of key values for the data in the
                            dictionary, which can be used to acess view bag
                            properties

                        Model - returnes the 'view model' for the request
                        ModelMetadata
                            - returns the 'ModelMetadata' object that can be
                            used to reflect on the model type
                        ModelState
                            - returns inform about the 'state' of the model

        - TextWriter - writing output to the client


    - how the IViewEngine, IView, ViewEngineResult works is by creating a ViewEngine
        - example
            We are creatgin a custom view engine instead of relying on Razor                

            Files:
            HomeBController.cs
            DebugDataView.cs
            DebugDataViewEngine
            Global.asax
                Application_start method
                    "ViewEngines.Engines.Add(new DebugDataViewEngine());
         
            We can learn from this example that
                - an app can have multiple view engines
                - *** when a "ViewREsult" is being processed, the 'action invoker'
                obtains the set of installed view engines and calls their 'FindView' method
                in return. The 'action invoker' stops calling 'FindView' method
                as sson as it receives a 'ViewEngineResult' object that contains
                an 'IView'. This means that the order in which engines are added to the
                ViewEngines.Engines collection is significant ... see page 566 from first-book
*/