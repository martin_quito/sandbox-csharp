﻿/*
What?
    - are action methods INVOKED from within a VIEW. A view can invoked
    an action method.

    - helps to avoid repeating controller logic that you want
    to use in several places in the app

    - use for creating REUSABLE UI Controls or widgets tha tneed
    to contain business logic. 

    - use it whenever you want to display some data-drive widget
    that appears on multiple pages AND contains DATA UNRELATED
    to the MAIN ACTION THAT IS RUNNING. <<<IMPORTANT>>>

How do create it one?
    - have an action method return a PartialView
    - you can opt in to have a "ChildActionONly" attribute to
    constaint the action method to only be used as an achild action. Its not required
    though but it has good benefvits like user-requests

How do you invoked it?
    @Html.Action("Time")
    You can also pass parameters 

                    
*/