﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Views.Controllers
{
    public class HomeBController : Controller
    {
        public ActionResult Index() {
            ViewBag.Message = "Hello, World";
            ViewBag.Time = DateTime.Now.ToShortTimeString();
            return View("DebugData");
        }

        public ActionResult List() {
            return View();
        }

        /*
            - if I want to only have my custom view-egine to be used
            then I can do "ViewEngine.Engines.Clear()" in the global.asax
        */
    }
}