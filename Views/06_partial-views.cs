﻿/*
What?
    - good for sharing subsections of view markuup between views
    - can contain
        - inline code
        - helper methods
        - references to other partial views
    - cannot
        - invoked an action method
            - cannot be used to perform BUSINESS LOGIC

    - what makes a view parial ... its content. (it only contains a
    a fragment of HTML, rather than a complete HTML document, and it doesn't
    refernce layouts, and the way it is used. 

    - onf of the most common uses of partial views is to render content
    in layouts

How do you consume from a view?
    @Html.Partial("MyPartial")
    - example
        - List.cshtml
        - HomeControler.cs
        - MyPartial.cshtml
    
    - no need of file extension
    - the view engine will look for the partial view in the common locations
        - /VIews/Controller
        - /Views/Shared

        Razor View engine looks for paritual views in the same way that it
        looks for regular views. /Views/Controller/ and /Views/Shared 
        which means you can create specifialized versions of partial views
        that are controller-specific and override parital views of the same
        name in the 'shared' folder

What are storngly type partial views?
    - example
        - list.cshtml
        - HomeController.cs
        - MyStronglyTypePartial.cshtml

    - this means you can pass 'view model' objects to be used when
    partial view is rendered


*/