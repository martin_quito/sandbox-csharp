﻿/*
What?
    - use for creating section of content that will be inserted in a 
    layout at specific locations

How?
    - sections are defined in views using the Razor "@section" tag followed by
    the name. In layouts, sections are define with the "@RenderSection" helper method
    
            @section Header {...}
            @RenderSection("Header")
    
            @RenderBody()

            @section Body {...} // view
            @RenderSection("Body") // layout

    - the convention is to define the sections at either the
    start or the end of the view, to make it easier to see which regions
    o fcontent will be treated as sections and which will be cappture
    by the 'RenderBody' helper. Another approach is to define the view
    solely in terms of sections 
        - for the second approach see
            - index.cshtml
            - _Layout.cshtml

    - you can also test for sections in layouts to provide a default
    if the view will not provide it
        @if (IsSectionDefined("Footer")) {
            ...
        }
        - example
            - see index.cshtml
            - _layout.cshtml

    - by default a view has to contain ALL of the sections that are defined
    to render. If sections missing, an error will be thrown. To make a
    section optional in the layout, you can do this

        @RenderSection("scripts", false)

        - This creates an optional section, the contents of which will 
        be inserted into the result IF the view defines it. This is a more
        elegant approach than the 'IsSectionDefined'

*/