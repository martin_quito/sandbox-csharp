﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace PRONETMVC5_LanguageFeatures.Models
{
	public class Product
	{
		// instead of doing this normal properties
		private int productID;
		public int ProductID
		{
			get { return productID; }
			set { productID = value; }
		}

		// we can do this, using automaticall implemented property
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string Category { get; set; }
		// - remmeber I cannot do a mix-match, is either one or the other
	}

	public class ShoppingCart: IEnumerable<Product>
	{
		public List<Product> Products { get; set; }

		IEnumerator<Product> IEnumerable<Product>.GetEnumerator() {
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			throw new NotImplementedException();
		}
	}

	public static class MyExtensionMethods
	{
		public static decimal TotalPrices(this ShoppingCart cartParam)
		{
			decimal total = 0;
			foreach (Product prod in cartParam.Products)
			{
				total += prod.Price;
			}
			return total;
		}

		public static decimal TotalPrices2(this IEnumerable<Product> productEnum)
		{
			decimal total = 0;
			foreach (Product prod in productEnum)
			{
				total += prod.Price;
			}
			return total;
		}

		public static IEnumerable<Product> FilterByCategory(this IEnumerable<Product> productEnum, string CategoryParam)
		{
			foreach (Product prod in productEnum)
			{
				if (prod.Category == CategoryParam)
				{
					yield return prod;
				}
			}
		}

		public static IEnumerable<Product> Filter(this IEnumerable<Product> productEnum, Func<Product, bool> selectorParam)
		{
			foreach (Product prod in productEnum)
			{
				if (selectorParam(prod))
				{
					yield return prod;
				}
			}
		}
	}
}