﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRONETMVC5_LanguageFeatures.Models;
using System.Text;

namespace PRONETMVC5_LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public string Index()
        {
			return "Navigate to a URL to show an example";
        }

		// properties and automatic properties
		public ViewResult AutoProperty()
		{
			// create a new product object
			Product myProduct = new Product();

			// set the property value
			myProduct.Name = "Kayak";

			// get the property
			string productName = myProduct.Name;

			// generate the view 
			// casting to use the correct overloaded method
			return View("Result", (object)String.Format("Product name: {0}", productName));
		}

		// Object initializer Feature
		public ViewResult CreateProduct()
		{
			// create a new Product object
			Product myProduct = new Product();

			// set the property values
			myProduct.ProductID = 100;
			myProduct.Name = "Kayak";
			myProduct.Description = "A boat for one person";
			myProduct.Price = 275M;
			myProduct.Category = "Watersports";

			// or I can do this using the Object initializer Feature
			Product myProduct2 = new Product
			{
				ProductID = 100,
				Name = "Kayak",
				Description = "A boat for one person",
				Price = 275M,
				Category = "Watersports"
			};

			return View("Result", (object)String.Format("Category {0}", myProduct.Category));
		}
		// or I can also use the collection initialization
		public ViewResult CreateCollection()
		{
			string[] stringArray = { "Apple", "Orange", "Plum" };
			List<int> intList = new List<int> { 10, 20, 30, 40 };
			Dictionary<string, int> myDict = new Dictionary<string, int> {
				{"apple", 10}, {"orange", 20}, {"plum", 30}
			};
			return View("Result", (object)stringArray[1]);
		}

		// extension methods
		// - a convenient way to add methods to classes you do not won and cannot modify
		public ViewResult UseExtension()
		{
			// create and populate ShoppingCart
			ShoppingCart cart = new ShoppingCart
			{
				Products = new List<Product> {
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M}
				}
			};

			// get the total
			decimal cartTotal = cart.TotalPrices();

			return View("Result", (object)String.Format("Total: {0:c}", cartTotal));
		}

		public ViewResult UseExtensionEnumerable() {
			// extension method on an interface
			IEnumerable<Product> products = new ShoppingCart
			{
				Products = new List<Product> {
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M}
				}
			};

			Product[] productArray = {
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M},
					new Product {Name = "Kayak", Price = 275M}
			};

			// get the total value
			decimal cartTotal = products.TotalPrices2();
			decimal arrayTotal = productArray.TotalPrices2();
			return View("Result", (object)String.Format("Cart Total: {0}, Array Total: {1}", cartTotal, arrayTotal));
		}

		// extension method for filtering
		public ViewResult UserFilterExtensionMethod()
		{
			IEnumerable<Product> products = new ShoppingCart
			{
				Products = new List<Product> {
					new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
					new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
					new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
					new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.5M}
				}
			};
			decimal total = 0;
			foreach(Product prod in products.FilterByCategory("Soccer")) {
				total += prod.Price;
			}

			return View("Result", (object)String.Format("Total: {0}", total));
		}

		// using lamba expressions
		public ViewResult UsingLambdaExpressions()
		{
			Func<Product, bool> categoryFilter = delegate(Product prod)
			{
				return prod.Category == "Soccer";
			};

			// or
			Func<Product, bool> categoryFilter2 = (prod) => prod.Category == "Soccer";


			IEnumerable<Product> products = new ShoppingCart
			{
				Products = new List<Product> {
					new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
					new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
					new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
					new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.5M}
				}
			};
			decimal total = 0;

			foreach (Product prod in products.Filter(categoryFilter))
			{
				total += prod.Price;
			}

			// or
			foreach (Product prod in products.Filter(x => x.Category == "Soccer"))
			{
				// ...
			}

			return View("Result", (object)String.Format("Total: {0}", total));
		}

		// using anonymous types
		public ViewResult CreateAnonArray()
		{
			var oddsAndEnds = new[] {
				new {Name = "MVC", Category = "Pattern"},
				new {Name = "Hat", Category = "Clothing"},
				new {Name = "Apple", Category = "Fruit"}
			};
			StringBuilder result = new StringBuilder();
			foreach (var item in oddsAndEnds)
			{
				result.Append(item.Name).Append(" ");
			}
			return View("Result", (object)result.ToString());
		}

		// using linq
		public ViewResult UsingLINQ()
		{
			Product[] products = {
				new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
				new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
				new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
				new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.5M}
			};

			// sort
			Product[] foundProducts = new Product[3];
			Array.Sort(products, (item1, item2) =>
			{
				return Comparer<decimal>.Default.Compare(item1.Price, item2.Price);
			});

			// get the first three items in the array as the results
			Array.Copy(products, foundProducts, 3);

			// OR
			var foundProducts2 = from match in products
								 orderby match.Price descending
								 select new { match.Name, match.Price };

			
			// create the result
			StringBuilder result = new StringBuilder();
			foundProducts2.ToList().ForEach(x => result.AppendFormat("Price: {0} ", x.Price));
			return View("Result", (object)result.ToString());	
		}
    }
}