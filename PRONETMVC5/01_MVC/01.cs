﻿/*
What is ASP.NET MVC ?
    - ASP.NET MVC is a web development framework from Microsoft that includes the best of the MVC architecture, 
    the best up-to-date ideas from agile development, and the best from the existing platform ASP.NET platform.
    - alternative to the ASP.NET Web forms
    - .NET is a multi-language code paltform
    - ASP.NET is a way to host .NET Apps in the IIS (microsoft web server) and letting you 
    to interact with HTTP req and res
    - ASP.NET Web forms are a set of UI compoements plus a stateful, object oriented GUI programming model
        - with web forms, it hid many details from the developer such as HTTP and HTML. It is a layer
        that made Web development feel like Windows Form development
        - what is wrong with ASP.Web Forms
            - large blocks data transferred between server and client to  maintain a state
            across states
            - delicate page life cycle
            - false sense of separation of concerns
            - limited control over HTML
            - leaky abstraction
            - low testability
Why
    - key benefits of ASP.NET MVC?
        - it has a variant of MVC
        - extensibility
            - use, derive, or replace routing system
            - use, derive, or replace the default view engine
            - use, derive, or replace the default controller factor
        - tight control over HTML and HTTP 
            - the HTML helper methods can produce standard-compliance makrup
            - you can control the HTTP
        - testability
            - testing becomes much easier because of separation of concerns
        - features
            - routing system
            - authentication
            - memebership
            - roles
            - profiles
            - internationalization

How?
    - see PRONETMVC5_PartyInvite
*/