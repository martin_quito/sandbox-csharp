﻿/*
How does the "Built-in controller factory" work?
    - "DefaultControllerFactory" class 
    - when a request is made, this builtin factory looks at the 
    routing data to find the value of the 'controller' property 
    and tries to find a class in the web app that meets the following criteria
        - the class must be public
        - the class must be concrete (not abstract)
        - the class must NOT take generic parameters
        - the name of the class must end with "Controller"
        - the class must implement the "IController" interface

    - maintains a list of such classes in the app so that it does not need
    to perform a search every time a request arrives

    - if a suitable class is found, then an instance is created using
    the "controller" activator and then the job is complete
        - the creation is customiziable

    - follows the convetion-over-configuration pattern because I don't
    need to register my controllers in a confioguration file

How to prioritize namespaces?
    The problem is where controller classes have the same name but reside
    in different namespaces. 
        - It is the "DefaultControllerFactory" that processes the list
        of namespaces and prioritizes them

    How?
        - see page 537, First Book

How to customize 'DefaultControllerFactoryu' Controller instantiation?
    There are nnumber of ways to customize how the 'DefafultControlelrFactory' class
    instantiates controller objects. The , by far, most common reason to do this
    is to add SUPPORT FOR DI. There are several ways to do this, the most
    suitable technique depends on how you are using DI elsewhere in you app. 
        Methods
            Using the Dependency Resolver
                - the DCF class will use the depedency resolver to create controllers
                if one is available (see page 538 from first book)
            Using a Controller Activator
                - (see page 538-540)
    
    You can override DefautlControllerFactory Methods
        - see page 538-540, 
       
*/