﻿/*
What is a controller factoruy?
    - responsbile for creating controllers to service a request, maps
    requests to controllers
    - one of the reasons its not recommneded creating custom
    controller is complicated like finding them and instantitating
    them. 
  

How?
    How to create a "custom Controller Factory"?
        - implement IControllerFactory
        - then set it in the "Application_Start()"

    How to register a custom Controller Factory
        - see page 536 from First Book. 
    

Example
    1 - create a custom Controllger factory
        - Models.Result
        - Shared.cshtml
        - ProductController.cs
        - CustomControllerFactory.cs

        Learning
            - great flexibility, very powerful
            - you can define your own conventions in your controller factory,
            you will still need to know what the conventions are for other parts
            of the MVC framework. 
*/