﻿/*
How to use "Action Method selectors"?
    - often a controller will have multiple methods with the same name but
    different parametsr or because I used the "ActionName" attribute to 
    represent multiple methods to one action. MVC will need some help
    in selecting the correct method. This process is called "action method
    selection" The "Action invoker" uses the action method selectors (attrs)
    to resolve the ambiguity when selection the action and the invoker gives
    preference to methods with selectors. For example, if I have two methos
    "checkout" and one has [httppost], the action invoker gives preference
    to the action methods that have "selectors", in this case, it will be
    the method with "HttpPost" will be evaluateed first and if it fails, 
    then it will continue to select the other. There are many hTTP Attributes 
    (selectors). There is also "[NonAction]" to work to be not a candidate 
    as a action method. This is similar to declaring it
    to a private.
    
    Example:
    - [HttpPost] derives from "ActionMethodSelectorAttribute"

Can I build custom action method selectors
    - you can build your own "Custom action method selectors"
    
What is the action method disambiguation process? (page 550 from first-book)
    - the action invoker starts the process with a list of possible
    candidates which are the controller methods that meet the action method
    criteria. then it follows this
        - invoker discards any method based on name. only methods that have the
        same name as the 'target action' or have a suitable 'ActionName'
        attribute are kep on the list
        - invoker discards any  method that has a'method selector attribute'
        that returns flase for the current request
        - if there is extractly one method left, that is invoke, if not, an
        exception is thrown
        - if there are no methods with 'selectors', then invoker loosk at
        those without selectors, if there is one, then it executes it, if not
        it throws an error. 


How are unknown actions handled?
    - if the built in action invoker cannot find an action to invoke, then it
    returns 'false' from its 'InvokeAction' method and when this happens,
    in the 'controller' class, it calls the 'HandleUnknownAction'
    method which by default it returns a '404-Not'. You can override the
    'HandledUnknownAction'
*/