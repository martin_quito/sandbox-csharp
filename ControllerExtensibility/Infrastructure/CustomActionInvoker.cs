﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllerExtensibility.Infrastructure {
    public class CustomActionInvoker : IActionInvoker {
        public bool InvokeAction(ControllerContext controllerContext, string actionName) {
            if (actionName == "Index") {
                controllerContext.HttpContext.Response
                        .Write("This is output from the Index action");
                return true;
            }
            else {
                return false;
            }
        }

        /*
            - the action invoker doesn't care ABOUT METHODS in the controller
            class. If fact, it deals with actions itself. For example, if the
            request is for the "Index" action then the invoker wreites a message
            directly to the Response. If the request is for any other action, then
            it returns false, which causes a '404 Not found' error to the displayed
            to the user. 

            - to obtained the action invoker associated witha controller, you can
            do 'Controller.ActionInvoker' property. This means different controllers
            in the same app can use different action invokers. 
        */
    }
}