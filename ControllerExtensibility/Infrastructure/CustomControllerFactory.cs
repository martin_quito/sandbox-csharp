﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using ControllerExtensibility.Controllers;

namespace ControllerExtensibility.Infrastructure {
    public class CustomControllerFactory : IControllerFactory {

        /*
        - purpose is to create instances of controller classes
        that can handle the current request. THere is no really restrictions
        in how to do this as long as you return a "IController" type. 

        The convention of user not appending the "Controller" word
        after controller names is part of the default controller factory.
        */
        public IController CreateController(RequestContext requestContext, string controllerName) {
            Type targetType = null;
            switch (controllerName) {
                case "Product": 
                    targetType = typeof(ProductController);
                    break;
                case "Customer":
                    targetType = typeof(CustomerController);
                    break;
                default:
                    // this needs to happen because by default MVC 
                    // selects a 'view' based on the "controller" value
                    // in the routing data, not the name of the
                    // controller class. 
                    requestContext.RouteData.Values["controller"] = "Product";
                    targetType = typeof(ProductController);
                    break;
            }

            return targetType == null ? null :
                (IController)DependencyResolver.Current.GetService(targetType);
            /*
                - there are no rules in how you instantiate your controlelr classes, but 
                a good practice is to use the dependency resolver. This allows to keep
                your custom controlelrs factory focused ON MAPPING request to controller classes
                and leaves issues like dependency inject to he handled separatly and for
                the entire application
            */
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName) {
            return SessionStateBehavior.Default;
        }
        /*
            - is used by the MVC Framework to determine if session data should be
            maintained for a controller
        */

        public void ReleaseController(IController controller) {
            IDisposable disposable = controller as IDisposable;
            if (disposable != null) {
                disposable.Dispose();
            }
        }
        /*
            - is called when a controller object created by the "CreateController"
            method is no longer needed. 
        */
    }
}