﻿/*
Why create a custom "Action Invoker"?
    - once the controller factory has created an isntance of a class, the
    framework neds a way of invoking an action on that instance
        - if you derived your controller from the "Controller" class,
        then this is the responsability of an 'action invoker'
            - if not, if you created the controller directly
            from the IController interface, then I'm responsible for
            executing the action yourself. 

How to use the custom ActionInvoker?
    How does the interface look like?
        public interface IActionINvoker{
            bool InvokeAction(ControllerContex controllerContext, string actionName)
        }
            - trrue, the 'action' was found and invoked
            - false, the controller has no matching actions

    Show me an example 1?
        CustomActionInvoker.cs        
        ActionInvokerController

When to use the AcitonINvoker?
    - use the built-in action invoker

What is the built in action invoker?
    - "ControllerActionInvoker" class
    - it operates in methods
        - to "qualify" as an action, a method must meet the following criteria
            - the method must be 'public'
            - the method must NOT be 'static'
            - the method must NOT be present in 'System.Web.Mvc.Controller'
            or any of its base classes
            - the method must 'not' have a special name
                - no generic parameters

    - by default, it finds a method that has the same name as the
    required 'action'. So if the 'action' value that the routing system
    provudes is 'index', then the ControllerActionInvoker will loook
    for a method called 'Index" that fits the action criteria. You can override
    this behavior using the "ActionName" attribute. You woudl do this for 2
    reassons: 
        1) accept action names that wouldn't be legal in a C# method name
            like 'User-Registration'
        2) have two different C# methods that accept the same set of params
        and should handle the same action name but in response to different 
        HTTP request types. 

            [HttpPost]
            [ActionName("Checkout")]
            public ActionResult Checkout()...

            [HttpGet]
            [ActionName("Checkout")]
            public ActionResult CheckoutLol()...

    

*/
