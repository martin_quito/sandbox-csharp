﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Domain.Entities;
using System.Data.Entity;

/*
In order to use entity farmework to query, insert, update, and delete
data using .NET Objects, you first need to "create a model" which
maps the entities and relationships that are defined in your model
to tables in a datas 
*/

namespace SportsStore.Domain.Concrete {
    class EFDbContext : DbContext {
        public DbSet<Product> Products { get; set; }

        /*
            the name of the property specifies the table
        and the type parameter of the DbSet result specifies
        the model type  that the Entity Framework should use to
        represent rows in that table
        */
    }
}
