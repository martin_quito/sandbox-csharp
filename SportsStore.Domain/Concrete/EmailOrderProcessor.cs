﻿using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;


namespace SportsStore.Domain.Concrete {
	public class EmailSettings {
		public string MailToAddress = "orders@example.com";
		public string MailFromAddress = "sportsstore@example.com";

		public bool UseSsl = true;

		public string Username = "Username";
		public string Password = "Password";
		public string ServerName = "smtp.example.com";
		public int ServerPort = 587;
		public bool WriteAsFile = false;
		public string FileLocation = @"c:\sports_store_emails";
	}

	public class EmailOrderProcessor : IOrderProcessor{
		private EmailSettings emailSettings;

		public EmailOrderProcessor(EmailSettings settings) {
			emailSettings = settings;
		}

		public void ProcessOrder(Cart cart, ShippingDetails shippingInfo) {
			// processing order
		}
	}
}
