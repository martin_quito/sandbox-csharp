﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics;

/*
Resources:
https://github.com/Moq/moq4/wiki/Quickstart
*/

namespace Moq4 {
	// =============
	// Assumptions
	// =============
	public class Bar {
		public virtual Baz Baz { get; set; }
		public virtual bool Submit() { return false; }
	}

	public class Baz {
		public virtual string Name {get; set; }
	}

	public interface IFoo {
		//Bar bar { get; set; }
		string Name { get; set; }
		//int Value { get; set; }
		bool DoSomething(string value);
		bool DoSomething(int number, string value);
		string DoSomethingStringy(string value);
		bool TryParse(string value, out string outputValue);
		bool Submit(ref Bar bar);
		//int GetCount();
		bool Add(int value);

	}

	public class Base {
		private IFoo myFoo;
		public Base(IFoo foo) {
			myFoo = foo;
		}	

		
	}

	[TestClass]
	public class QuickStart {
		[TestMethod]
		public void Basic() {
			// ==============
			// Methods
			// ==============
			Mock<IFoo> mock = new Mock<IFoo>();
			mock.Setup(foo => foo.DoSomething("ping")).Returns(true);
			Debug.WriteLine(mock.Object.DoSomething("ping1"));
				// -> true
				// - this means that if I do a method invocation with 
				// "DoSomething(ping") then I will get true, otherwise, nothing

			Debug.WriteLine("=== AB === ");
			Mock<IFoo> mockAB = new Mock<IFoo>();
			Debug.WriteLine(mockAB.Object.DoSomething("ping1"));
				// -> false
				// this tells me that anything that I don't implement from the
				// interface, it will return false
				// because from the preivous example, I did "mock" the method
				// so its found when I call it 
			
			// out argumnets
			var outString = "ack";
			mock.Setup(foo => foo.TryParse("ping", out outString)).Returns(true);
			Debug.WriteLine(mock.Object.TryParse("ping1", out outString));
				// -> false
				// - > TryParse will return true, and the the 'out' argument
				// will return 'ack', lazy evalutated

			// ref arguments
			var instance = new Bar();
			mock.Setup(m => m.Submit(ref instance)).Returns(true);
			Debug.WriteLine(mock.Object.Submit(ref instance));
				// -> True
				// Only matches if the 'ref' argument to the invocation is the
				// same instance

			// ==============
			// Maching Arguments
			// ==============
			// Match any value
			Debug.WriteLine("=== 2 === ");
			Mock<IFoo> mock2 = new Mock<IFoo>();
			mock2.Setup(foo => foo.DoSomething(It.IsAny<string>())).Returns(true);
			Debug.WriteLine(mock2.Object.DoSomething("MEOW"));
				// -> True
				// - this says that any method invocation on 'DoSomething'
				// with whatever value that is string, it will return true
				// otherwise, it returns false
			/*

				It
					// Summary:
					//     Allows the specification of a matching condition for an argument in a method
					//     invocation, rather than a specific argument value. "It" refers to the argument
					//     being matched.
					//
					// Remarks:
					//     This class allows the setup to match a method invocation with an arbitrary
					//     value, with a value in a specified range, or even one that matches a given
					//     predicate.

				It.IsAny
					// Summary:
					//     Matches any value of the given TValue type.
					//
					// Type parameters:
					//   TValue:
					//     Type of the value.
					//
					// Remarks:
					//     Typically used when the actual argument value for a method call is not relevant.
			*/

			Debug.WriteLine("=== 3 === ");
			Mock<IFoo> mock3 = new Mock<IFoo>();
			mock3.Setup(foo => foo.DoSomething(It.IsAny<string>())).Returns<string>(x => x == "CAT");
			Debug.WriteLine(mock3.Object.DoSomething("CAT"));
				// -> True
				// - if method invocation is done on DoSomething with any string variable as the
				// argument, then it will return IF the 

			/*
			IReturnsResult<TMock> Returns(Delegate valueFunction);
				- 


			IReturnsResult<TMock> Returns(Func<TResult> valueFunction);
			IReturnsResult<TMock> Returns<T>(Func<T, TResult> valueFunction);
			IReturnsResult<TMock> Returns<T1, T2>(Func<T1, T2, TResult> valueFunction); 
			IReturnsResult<TMock> Returns<T1, T2, T3 ... T16>(Func<T1, T2, T3 ... T16, TResult> valueFunction);
				- overloads stretch from this until <T1, T2>
				- "Specifies a function that will calculate the value to return from the method,
					retrieving the arguments for the invocation.
				- as it explained above from the assembly's doc
				

			IReturnsResult<TMock> Returns(TResult value);
				- this specifies the value to return
	
			*/

			Debug.WriteLine("=== 3.1 === ");
			Mock<IFoo> mock31 = new Mock<IFoo>();
			mock31.Setup(foo => foo.DoSomething(It.IsAny<int>(), It.IsAny<string>())).Returns<int, string>((num, st) => num == 1 && st == "CAT");
			Debug.WriteLine(mock31.Object.DoSomething(1, "CAT"));
				// -> True
				// -> this shows multiple parametrs overloads avaiable

			Debug.WriteLine("===Mock4===");
			Mock<IFoo> mock4 = new Mock<IFoo>();
			mock4.Setup(m => m.Add(It.IsAny<int>())).Returns(true);
			Debug.WriteLine(mock4.Object.Add(10000));
				// -> True


			// Any value passed in a 'ref' parameter (requires moq 4.8 or later)
				// skipped

			// Matcing Fun<int>, lazy evaluated
			Debug.WriteLine("===Mock6===");
			Mock<IFoo> mock6 = new Mock<IFoo>();
			mock6.Setup(m => m.Add(It.Is<int>(i => i % 2 == 0))).Returns(true);
			Debug.WriteLine(mock6.Object.Add(4));
				// -> True
				// matches not just by a straight comparison as before
				// or accepting any value, but througha  func delegate
				// And if it matches, it return strue

			// matching ranges
			//mock.Setup(foo => foo.Add(It.IsInRange<int>(0, 10, Range.Inclusive))).Returns(true); 

			// matching regex
			//mock.Setup(x => x.DoSomethingStringy(It.IsRegex("[a-d]+", RegexOptions.IgnoreCase))).Returns("foo");

			// ==============
			// Properties
			// ==============
			Debug.WriteLine("===Mock7===");
			Mock<IFoo> mock7 = new Mock<IFoo>();
			mock7.Setup(foo => foo.Name).Returns("bar");
			Debug.WriteLine(mock7.Object.Name);
				// -> bar

			// expects an invocation to set the value to 'foo'
			Debug.WriteLine("===Mock8===");
			Mock<IFoo> mock8 = new Mock<IFoo>();
			mock8.SetupSet(foo => foo.Name = "foo");
			Debug.WriteLine(mock8.Object.Name);

			// ==============
			// customizng mock behavior
			// ==============
			Debug.WriteLine("===Mock9===");
			/*
				"Make mock behave like a "true Mock", raising exceptions for 
			anything that doesn't have a corresponding expectation: in Moq 
			slang a "Strict" mock; default behavior is "Loose" mock, which 
			never throws and returns default values or empty arrays, 
			enumerables, etc. if no expectation is set for a member"
			*/
			var mock9 = new Mock<IFoo>(MockBehavior.Strict);
			mock9.Setup(foo => foo.DoSomething("ping")).Returns(true);
			Debug.WriteLine(mock9.Object.DoSomething("ping1"));
				// the expectation is set with 'ping' and I'm calling
				// 'pin1' which Im calling an expcetion that doesn't exist
				// which is why an exception is thrown


			// ==============
			// Advanced features
			// ==============
			// implemetning multiople interfaces in mock
			Debug.WriteLine("===Mock10===");
			var mock10 = new Mock<IFoo>();
			var disposableFoo = mock10.As<IDisposable>();
			// now the IFoo mock also implements IDisposable :)
			disposableFoo.Setup(disposable => disposable.Dispose());

			// implemenmting multple interfaces in a single mock
			Debug.WriteLine("===Mock11===");
			Mock<IFoo> mock11 = new Mock<IFoo>();
			mock11.Setup(foo => foo.Name).Returns("Fred");
			mock11.As<IDisposable>().Setup(disposable => disposable.Dispose());
			

			// when you mock overloaded methods, do them from genereal to specific
		}
	}
}
