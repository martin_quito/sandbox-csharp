﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Concrete;
using System.Configuration;

namespace SportsStore.WebUI.Infrastructure {
    public class NinjectDependencyResolver : IDependencyResolver{
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam) {
            kernel = kernelParam;
            AddBindings2();
        }

        // implemented
        public object GetService(Type serviceType) {
            return kernel.TryGet(serviceType);
        }

        // implemented
        public IEnumerable<object> GetServices(Type serviceType) {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings() {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new List<Product> {
                new Product {Name = "Football", Price = 25, Description = "Round and small"},
                new Product {Name = "Surf board", Price = 179},
                new Product {Name = "Running shows", Price = 95}
            });
            kernel.Bind<IProductRepository>().ToConstant(mock.Object);
            // this returns the same mock for every request rather than create
            // a new instance of the implementation object each time
        }
        // next step is to create a bridget between the this class
        // and the MVC support for dependency injection in the
        // App_Start/NinjectWEbCommon.cs file which one of the Ninject Nuget
        // packages added to the project

        private void AddBindings2() {
            kernel.Bind<IProductRepository>().To<EFProductRepository>();
            /*
                - it tells ninject to create instances of the EFProductRepository class
                to service requests for the IProductRepository interface. 
            */

			EmailSettings emailSettings = new EmailSettings {
				WriteAsFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false")
			};

			kernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>()
				.WithConstructorArgument("settings", emailSettings);
        }
    }
}