﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MyTypeViewer {
    class Program {
        static void Main(string[] args) {   
            Console.WriteLine("*** Welcome to MyTypeViewer");
            string typeName = "";
            do {
                Console.WriteLine("\nEnter a type name to evaluate");
                Console.WriteLine("or enter Q to quit: ");
                // get name of type
                typeName = Console.ReadLine();

                /*
                Reflecting Generic types
                    System.Collections.Generic.List `1
                    System.Collections.Generic.List<t>

                    System.Collections.Generic.Dictionary `2
                    System.Collections.Generic.Dictionary<TKey, TValue>

                */

                // Does user want to quit?
                if (typeName.ToUpper() == "Q") {
                    break;
                }
                try {
                    Type t = Type.GetType(typeName);
                    Console.WriteLine("");
                    ListVariousStats(t);
                    ListFields(t);
                    ListProps(t);
                    ListMethods(t);
                    ListInterfaces(t);
                } catch {
                    Console.WriteLine("Sorry can't find type");
                }
            } while (true);

        }

        // reflecing on methods
        static void ListMethods(Type t) {
            Console.WriteLine("*** Methods ***");
            MethodInfo[] mi = t.GetMethods();
            foreach (MethodInfo m in mi) {
                // get return type
                string retVal = m.ReturnType.FullName;
                string paramInfo = "( ";
                foreach (ParameterInfo pi in m.GetParameters()) {
                    paramInfo += string.Format("{0} {1} ", pi.ParameterType, pi.Name);
                }
                paramInfo += " )";

                Console.WriteLine("-> {0} {1} {2}", retVal, m.Name, paramInfo);
            }
        }

        // reflectin on fields and properties
        static void ListFields(Type t) {
            Console.WriteLine("*** Fields ***");
            var fieldNames = from f in t.GetFields() select f.Name;
            foreach (var name in fieldNames) {
                Console.WriteLine("-> {0}", name);
            }
        }

        static void ListProps(Type t) {
            Console.WriteLine("*** Properties ***");
            var fieldNames = from f in t.GetProperties() select f.Name;
            foreach (var name in fieldNames) {
                Console.WriteLine("-> {0}", name);
            }
        }

        // reflecting on implemented interfaces
        static void ListInterfaces(Type t) {
            Console.WriteLine("*** Interfaces ***");
            var fieldNames = from f in t.GetInterfaces() select f.Name;
            foreach (var name in fieldNames) {
                Console.WriteLine("-> {0}", name);
            }
        }

        // stats
        static void ListVariousStats(Type t) {
            Console.WriteLine("*** Various Stats ****");
            Console.WriteLine("Base class is: {0}", t.BaseType);
            Console.WriteLine("Is type abstract?: {0}", t.IsAbstract);
            Console.WriteLine("Is type sealed?: {0}", t.IsSealed);
            Console.WriteLine("Is type generic?: {0}", t.IsGenericTypeDefinition);
            Console.WriteLine("Is type a class type?: {0}", t.IsClass);
        }


    }
}
