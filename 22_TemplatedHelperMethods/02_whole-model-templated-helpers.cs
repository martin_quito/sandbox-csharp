﻿/*
What are they?
    - 'template helpers' vs 'scaffolding helpers'. The template helpers
    generate output for a single property but the scaffolding helpers
    generate for entire objects

Who are they?
DisplayForModel
    - Html.DisplayForModel()
    - Renders a read-only view of the entire model object

EditorForModel     
    - Html.EditorForModel()
    - Renders editor elements for the entire model object

LabelForModel       
    - Html.LabelForModel()
    - Renders an HTML <label> element referring to the entire model obj


*/

/*
// =============
// whole-model templated helpers
// =============
Example files
    - Scaffolding.cs
    - HomeController.cs, 

Learned
    - Some problems
        - the 'LabelForModel' generated something not useful
        - not everything is visible (Address property)
        - what is visible is not always useful (role property)    

*/