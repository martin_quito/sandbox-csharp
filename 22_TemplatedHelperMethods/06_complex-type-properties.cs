﻿/*
Why?
    - the 'template' process relies on the 'object' template where each 
	property is examined and a 'template' is selected to render the HTML 
	to represent the property and its data value . When it encounters a 
	complex object, it skips it. This is because objects only operate on 
	'simple types' which are those that can be parse from a string value using 
	the 'getconverter' method on the system.ComponentModel.TypeDescriptor' class. 
	The supported types include the instrinsic C# types. To render HTMl for 
	a complex type, I need to to it explicitly by making a separate call to a 
	"Templated Helper Method"

How to create a custom editor template?
	- place the custom template file in the /Views/Shared/EditorTemplates/ 
	and ensure the name of the file matches with the name of the complex type
	- the files on the EditorTemplates can be used by any templated helper method

How does the template search order work?
	1. The templated passed to the helper. For example, Html.EditorFor(m => m.SomeProperty)
	, "MyTemplate") would lead to "MyTemplate" being used
	2. Any template that is specified by metadata attributes such as UIHint
	3. The template associated with any data type specified by metadata, such
	as the 'DataType' attribute
	4. Any template that corressponds to the .NET class name of the data type
	begin processed
	5. The built-in string template if the data type being processed is a simple type
	6. Any template that corresponds to the base classes of the data type
	7. If the data type implements IEnumeable, then the built in 'Collection'
	template will be used
	8. If all else fails, the 'Object' template will be used, subject to the rule
	that scaffolding is not recursive

	* At each stage in the template search process, the MVC framework looks
	for template called 'EditorTemplates/<name>' for editor helper methods
	or 'DisplayTemplates/<name>' for display helper methods. 

	* custom templates are found using the same search pattern as regular views, which
	means I can create a controller-specific custom templates and place it
	in the ~/Views/<contrller>/EditorTemplates folder to override the templates
	found in ~/Views/Shared/EditorTemplates

Replacing Built-in templates
	- if I create a custom template that has the same name as one of the
	built-in tempaltes, the MVC framework will use the custom version in preference
	to the built-in one
*/


// ============
// Example 1 - using the stronglty typed EditorFor helper method to 
// handle complex type properties
// ============
/*
- files
	- Person.cs
	- HomeController.cs (TemplateHelperMethod, TemplateHelperMethod (httppost)
	- TemplateHelperMethod.cshtml
- learned
	- we use the strongly type 'EditorFor' helpe method to display the complex
	type
	- "The 'HomeAddress' property is typed to return an 'Address' object
	and I can apply all of the same metadata to the 'Address' class
	as I did to 'Person' class. The 'Object' template is invoked explciitly
	when I use the 'EditorFor' helpers on the 'HomeAdress' property, and
	so all of the metadata conventions are honored."


// ============
// Example 2 - custom editor template
// ============
- create a custom template for the 'Role' property in the PersonC class
	- the property is of type 'RoleC' but the what is rendered is just a regular
	input element.
	- the MVC framework looks for 'custom editor templates' in 
	the 'Views/Shared/EditorTemplates' folder

- Files
	- PersonC.cs
	- HomeController (CustomTemplate, CustomTemplate httpost)
	- Role.cshtml
	- CustomTemplate.cshtml

- when I use ANY, ANY of the templated helper methods to generate an
editor for the 'RoleC' type, my /EditorTemplates/Role.cshtml file will be used. 

- this satisfies number 4 in teh template search order

// ============
// Examples 3 - using a single templateed helper method to use a custom editor template
// ============
- files
	- PersonC.cs
	- HomeController (CustomTemplateOnASingleTemplateHelper)
	- CustomTemplateOnASingleTemplateHelper.cshtml

- it works with a single tempalted helper method


*/