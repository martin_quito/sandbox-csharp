﻿/*
What?
    - templed templates (scafoolding or single) have no special knowledge
    about the app and use their best guess to generate HTML. Template helpers
    can be impvoed by using 'model metadata' to provide guidance
    how to handle model types. These metadata is expressed using C# attributes
  
Model metadata attributes?

    [HiddenInput]
        - makes the input type 'hidden' but the value is still renderd
        <div class="editor-field">
            0 
            <input id="PersonId" name="PersonId" type="hidden" value="0"/>
        </div>
            The hidden input element is helpful for HTML forms
            because it ensures that a value for the property is submitted along
            with the rest of the form
            

        - [HiddenInput(DisplayValue=false)]
            The label and the value are ommitted
        
        - if I chose to use a simple templated helper, the output is the same

    [ScaffoldColumn(false)]
        - completely exclude a property from the generated HTMl
        - does not work with single template helpers         

    [DisplayName]
        - use for 'Label', 'LabelFor', 'LabelForModel', 'EditforModel'
        - requires 'System.ComponentModel.DataAnnotations' and
        'System.ComponentModel'
        [DisplayName("New Person")]
            - appleid to classes
        [Display(Name="Last")]
            - applied to properties
            - example
                [Display(Name="Birth Date")]
                public DateTime BirthDate {get;set;}
                
                =>
                <label for="BirthDate">Birth Date</label>
                instead of (without the attribute)
                <label for="BirthDate">BirthDate</label>
                
    [DataType]
        - example
        [DataType(DateType.Date)]
        public DateTime BirthDate {get;set;}
        
        - a list of the most useful DateType enumeration
            - DateTime
            - Date
            - Time
            - Text
            - PhoneNumber
            - MultilineText
            - Password
            - Url
            - EmailAddress

        - the effect of these values depends on the type of property they are
        associated with and the helper being used. Like the 'MultilineText'
        will be ignored by "display" helpers

		- to provide instructions about how a model property should be displayed
		, is this referrering to the value? or the html controller? 

		- changes the type of the input element
*/

// ==========
// use metadat to control editing and visibilty
// ==========
/*
- helps me to control the editablity and visibility 
- the attributes are
    - [HiddenInput]
    - [ScaffoldColumn]

- Applies to:
    - Editor
    - EditorFor
    - EditorForModel


*/

// ==========
// using metata for labels
// ==========
/*
- by default 'Label', 'LabelFor', 'LabelForModel', and 'EditoForModel'
helpers use the names of properties as the content for label elements they 
generate. But with [DisplayName], you can add metadata in how you 
want to display those labels
*/

// ==========
// using metadata for data values
// ==========
/*
- example files:
	- homecontroller.cs, MedataDataForValues, MedataDataForValues (host)
	- MedataDataForValues.cshtml
	- _Layout.cshtml

- learned
	- use the whole template helper for a property with DateTime as the type, it
	renders as

		<div class="editor-field">
			<input class="text-box single-line" 
				id="BirthDate" 
				name="BirthDate" 
				type="datetime" 
				value="1/1/0001 12:00:00 AM"> 
		</div>

		- but if I add the model metadata attribute [DataType(DataType.Date)]

			<div class="editor-field">
				<input class="text-box single-line" 
					id="BirthDate" 
					name="BirthDate" 
					type="date" 
					value="1/1/0001"> 
			</div>
		- it changes the 'type' of the html input element
*/