﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _22_TemplatedHelperMethods;

namespace _22_TemplatedHelperMethods
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Fruits = new string[] { "Apple", "Orange", "Pear" };
            ViewBag.Cities = new string[] { "New York", "London", "Paris" };
            string message = "This is an HTML element: <input>";
            return View((object)message);
        }

        public ActionResult CreatePerson() {
            return View(new Person());
        }

        //[HttpPost]
        //public ActionResult CreatePerson(Person person) {
        //    return View(person);
        //}

        [HttpPost]
        public ActionResult CreatePerson(Person person) {
            return View("DisplayPerson", person);
        }


        public ActionResult Scaffolding() {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult Scaffolding(Person person) {
            return View(person);
        }

		public ActionResult MedataDataForValues() {
			return View(new PersonB());
		}

		[HttpPost]
		public ActionResult MedataDataForValues(PersonB person) {
			return View(person);
		}

		public ActionResult TemplateHelperMethod() {
			return View(new Person());
		}

		[HttpPost]
		public ActionResult TemplateHelperMethod(Person person) {
			return View(person);
		}

		public ActionResult CustomTemplate() {
			return View(new PersonC());
		}

		[HttpPost]
		public ActionResult CustomTemplate(PersonC person) {
			return View(person);
		}

		public ActionResult CustomTemplateOnASingleTemplateHelper() {
			return View(new PersonC());
		}

		public ActionResult GenericCustomTemplate() {
			return View(new PersonD());
		}

    }
}


