﻿/*
Why?
    - sometimes it will not be possible to apply metadata to entity model classes
    because the models are generated. In this case we can use a buddy class
    that has the metadata and apply it to the model. The buddy class andmust be
    in the same namespace as the model class.

How?
	- see more in page 637, first-book
*/