﻿/*
What?
	- Im not limited to creating type-specific templates. I can
	create a template that works for all enumerations and then specify
	that this template be selected using the 'UHint' attribute


*/

// ========
// Example 1 - generic template for enum
// ========
/*
Files
	- Enum.cshtml
	- HomeController.cs
	- PersonD.cs
	
*/