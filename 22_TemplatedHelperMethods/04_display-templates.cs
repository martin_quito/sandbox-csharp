﻿/*
What?
	- whole or single templated helpers use 'display templates' to generate
	HTMl. The template that is used is based on the 'type' of the property
	being processed and the kind of helper being used. We can use 'UIHint'
	attribute to specifiy the template used to render HTML for a property

What are the buitl-in list of  MVC Framework View Templates?

    Boolean
    Collection
    Decimal
    DateTime
    Date
    EmailAddress
    HiddenInput
    Html
    MultilineText
    Number
    Object
        - it is the template used by the 'scaffolding helpers' to generate
        HTML for a 'view model object' This tempaltes examnies each of the
        properties of an object and selects the most suitable template for the
        property type. The 'object' template takes metadata such as the 'uihint'
        and 'datatype' attributes into account (this is referring to the applied
		attributes to the properties)
    Password
    String
    Text
    Tel
    Time
    Url

*/