﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace _22_TemplatedHelperMethods {
    public class PersonD {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
		public AddressD HomeAddress { get; set; }
        public bool IsApproved { get; set; }

		[UIHint("Enum")]
        public RoleD RoleA { get; set; }
    }

    public class AddressD {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public enum RoleD {
        Admin,
        User,
        Guest
    }
}