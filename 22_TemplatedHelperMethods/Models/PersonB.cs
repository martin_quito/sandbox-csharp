﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace _22_TemplatedHelperMethods {
    public class PersonB {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

		[DataType(DataType.DateTime)]
        public DateTime BirthDate { get; set; }
        public Address HomeAddress { get; set; }
        public bool IsApproved { get; set; }
        public Role Role { get; set; }

		[DataType(DataType.EmailAddress)]
		public string EmailA { get; set; }

		public string EmailB { get; set; }
    }

    public class AddressB {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public enum RoleB {
        Admin,
        User,
        Guest
    }
}