﻿/*
Why Templated helper methods?
	- because when using jsut helper methods, I need to decide
	what kind of html element shoul be used to represent the model properties
	and to manually update the 'views' and to manually update the 'views'
	if the 'type' of the 'property' changes. But when using 'templated helper methods'
	, I specify the property I want to display and let the MVC framework 
	figure out what HTML elements ae required. 


List of MVC Templated helper methods
    Display    
        - Html.Display("FirstName")
        - Renders a read-only view of the specified model 
        property, choosing an HTML element according
        to the property's type and metadata

    DisplayFor  
        - Html.DisplayFor(m => m.FirstName)
        - Strongly typed version of the previous helper

    Editor      
        - Html.Editor("FirstName")
        - Renders an editor for the specified model property
        choosing an HTML element according to the property's
        TYPE and METADATA
    
    EditorFor   
        - Html.EditoFor(m => m.FistName)
        - Strongly type version of the previous helper

    Label       
        - Html.Label("FirstName")
        - Renders an HTML <label> element referring to the
        specified model property

    LabelFor    
        - Html.LabelFor(m => m.FirstName)
        - Strongly typed version of the previous helper

*/






/*
// ==========
// example1 , templated helper methods - Editor, EditorFor
// ==========
/*
Example, files
    - see Person.cshtml
    - HomeController.cs, createPerson, createPerosn (httoPost)
    - Person.cs model
    - Layout.cshtml

Learned
    - @Html.Editor, @Html.EditorFor. Both gives the same output. The former
        requires a string and the latter requeires a lamba expresion. The former
        does a search for the property in the ViewBag and Model (see 22_HelperMethods,
        InputHelper3.cshtml). The latter reduces the changes of causes an error
        by mistying the property name. 

    - notice the assigned html 'type' attribute 
        PersonId    - number
        FirstName   - text
        LastName    - text
        Role        - text
        DateTime    - datetime
    
        Some Browsers have default implemention for input's types. 'text' is the default

// ==========
// example 2, generating label and display elements
// ==========
Example, files
    - see Person.cshtml
    - HomeController.cs, createPerson, createPerosn (httoPost) with the "display person"
    - Person.cs model
    - Layout.cshtml

Learned
    - the display methods do not generate an HTMl element by default, 
    they just emit the value of the property`
    - behavior can change in order to produce output that is much more the kind 
    of thing that you would want to display of users
    - it genereates CSS attributes on the generated HTML
*/

