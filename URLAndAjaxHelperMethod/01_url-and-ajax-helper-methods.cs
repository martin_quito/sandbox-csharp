﻿/*



    - how to handle graceful degration?
        - what if broweser doesn't support JS. I can specify the target url to the server
        in the ajax options rather than specifying the action name as an argument
        to the 'ajax.Beginform'. If js is enabled, then the url in the ajax options will be used,
        if not, then the browser will use the regular form posting techinque, which
        takes the 'target URL' from the 'action' attribute which points back at the
        action method that will generate the complete HTML page. 

        - how about in ajax links?
            same thing, specify the url in the ajax options, but don't forget to add
            the actionName for a full refresh as an argument in the ajaxLink
    - How to work with JSON?
        - use the JSON Result
        - when returning a jsonresult, you would return something like this

                return Json(data, JsonRequestBehavior.AllowGet);
                
                    - by default it will only respond to post, to set to
                    'get', use the JsonRequestBehavior. This is due to a security
                    issue where third party in some browsers can intercept get requests
        - when detecting ajax requests, be careful, there are some limitations
            - 'IsAjaxRequest' is true when the browser has included 
            the 'X-Request-With' header with a value 'XMLHttpRequest' and
            it assumes that all ajax request requests require JSON data. 
*/