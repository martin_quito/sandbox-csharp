﻿/*
How to detecet if the reques tis ajax?
	- use 'IsAjaxRequest'
		- limitations
			- returns true if the browser has included 'X-Requested-With' header in 
			its request and set the value to 'XMLHttpRequest' 
				- this is a widely used convention but isn't universal 
			- it assumes that all ajax request require JSON data
				- maybe is betters to seaprate action methods for data formats
				instead of using an 'if' statement using 'IsAjaxRequest'

*/

// =========
// Example 1
// =========
/* 
Files
	- PeopleController.cs | "deteect ajax requests in action method" peace
	- detectajax.cshtml
*/