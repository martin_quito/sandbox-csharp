﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using URLAndAjaxHelperMethod.Models;

namespace URLAndAjaxHelperMethod.Controllers
{
    public class PeopleController : Controller
    {
        private Person[] personData = {
            new Person {FirstName = "Adam", LastName = "FreeMan", Role = Role.Admin},
            new Person {FirstName = "Jackqui", LastName = "Griffyh", Role = Role.User},
            new Person {FirstName = "John", LastName = "Smith", Role = Role.User},
            new Person {FirstName = "Anne", LastName = "Jones", Role = Role.Guest},
        };

        public ActionResult Index()
        {
            return View();
        }

        // ==============
        // urland links
        // ==============
        public ViewResult UrlAndLinks() {
            return View("UrlAndLinks");
        }

        // ==============
        // synchronous
        // ==============
        public ActionResult Synch() {
            return View("Synchronous", personData);
        }

        [HttpPost]
        public ActionResult Synch(string selectedRole) {
            if (selectedRole == null || selectedRole == "All") {
                return View("Synchronous", personData);
            }
            else {
                Role selected = (Role)Enum.Parse(typeof(Role), selectedRole);
                return View("Synchronous", personData.Where(p => p.Role == selected));
            }
        }

        // ==============
        // get people async
        // ==============
        public ActionResult Async(string selectedRole = "All") {
            return View("Asynchronous", (object)selectedRole);
        }

        public PartialViewResult AsyncPartial(string selectedRole = "All") {
            IEnumerable<Person> data = personData;
            if (selectedRole != "All") {
                Role selected = (Role)Enum.Parse(typeof(Role), selectedRole);
                data = personData.Where(p => p.Role == selected);
            }
            return PartialView("AsynchronousPartial", data);
        }


        // ==============
        // working with JSON
        // ==============
        private IEnumerable<Person> GetData(string selectedRole) {
            IEnumerable<Person> data = personData;
            if (selectedRole != "All") {
                Role selected = (Role)Enum.Parse(typeof(Role), selectedRole);
                data = data.Where(x => x.Role == selected);
            }
            return data;
        }

        public JsonResult GetPeopleDataJson(string selectedRole = "All") {
			// send the original object
            //IEnumerable<Person> data = GetData(selectedRole);

            // or send what is needed
            var data = GetData(selectedRole).Select(p => new {
                FirstName = p.FirstName,
                LastName = p.LastName,
                Role = Enum.GetName(typeof(Role), p.Role)
            });

            return Json(data, JsonRequestBehavior.AllowGet);
            /*
                - by default it will only respond to post, to set to
                'get', use the JsonRequestBehavior. This is due to a security
                issue where third party in some browsers can intercept get requests
            */
        }

        public PartialViewResult GetPeopleDataPartialView(
            string selectedRole = "All") {
                return PartialView("jsonpartialview", GetData(selectedRole));
        }

        public ActionResult GetPeople(string selectedRole = "All") {
            return View("json", (object)selectedRole);
        }

        // ==============
        // deteect ajax requests in action method
        // ==============
        public ActionResult DetectAjaxIndex(string selectedRole = "All") {
            return View("detectajax", (object)selectedRole);
        }

        public ActionResult DetectAjax(string selectedRole = "All") {
            IEnumerable<Person> data = personData;
            if (selectedRole != "All") {
                Role selected = (Role)Enum.Parse(typeof(Role), selectedRole);
                data = data.Where(x => x.Role == selected);
            }
            if (Request.IsAjaxRequest()) {
                var formattedData = GetData(selectedRole).Select(p => new {
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Role = Enum.GetName(typeof(Role), p.Role)
                });
                return Json(formattedData, JsonRequestBehavior.AllowGet);
            }
            else {
                return PartialView("detectajaxpartialview", data);
            }
        }
    }
}