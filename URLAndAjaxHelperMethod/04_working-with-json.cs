﻿/*
What?
	- instead of returning HTML fragments, return JSON


*/

// ========
// Example 1 -
// ========
/*
File
	- PeopleController.cs | working with JSON section
	- json.cshtml
	
Learned
	- when returng a jsonresult, and using the helper 'json' method,
	apply 'JsonRequestBehavior.AllowGet' to have the method respond
	to 'get' requests too. 
	- you can be creative and return two different formats of data (HTML or JSON)

*/