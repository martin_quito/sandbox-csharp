﻿/*

What does it mean "unobtrusive ajax"
	- you use helper methods to define ajax features, rather than having to
	add blocks of code throught your views.
	- this based on jQuery
	- MVC framework contains built-in support for Unobstrusive ajax

How to prepare the project for unobstrusive ajax?
	- turn on "UnobtrusiveJavascriptEnabled" to true in the Web.config file
	- add references to the jquery js libraries
	<script src="~/Scripts/jquery-1.10.2.js"></script>
	<script src="~/Scripts/jquery.unobtrusive-ajax.js"></script>

How does unobtrusive ajax work?

    - the ajax.begin 'helper method' and its options generate html with attributes
    which then js scans the html for ajax attributes (data-ajax) which configures
    it to work ajax. I don't have to use this method, i can use other methods or
    jquery directly.

 
How to setup unobstrsuive ajax?
	- enable in Web.config file, the one in the root folder, in 
	'configuration/appSettings' that contain an entry, 'UnobtrusiveJavaScriptEnabled'
	set to true
	- add the jquery js library 
		<script src="~/Scripts/jquery-1.10.2"></script>
		<script src="~/Scripts/jquery.unobtrusive-ajax.js"></script>


The Ajax options?
	Confirm
		- sets a message to be displayed to the user in a confirmation
		window befoire maing the ajax request
	HttpMethod
		- sets the http method that will be used to make the request - must be
		either 'Get' or 'Post'
	InsertionMode
		- specifes the way which the content retrieved from the server is 
		inserted into the HTML. The three choices are as expressed as values 
		from the 'InsertionMode' enum 'InsertAfter', 'InsertBefore', 'Replace' 
		(which is the default)

	LoadingElementId
		- specifies the duration of the animation used to reveal the elemnt 
		specified by 'LoadingElementId'

	LoadingElementDuration
		- specifies the duration of the animation used to reveal the element 
		specified by 'LoadingElementId'

	UpdateTargetId
		- sets the ID of the HTML element into which the content retrieved 
		from the server will be inserted
	Url
		- sets the URL that will be requested from the server


How to ensure graceful degration?
	- js disabeld (see p658 from book1)



*/

// ===========
// Example 1 - a basic example of synchrnous
// ===========
/*
Files
	- syncrhnous.cshtml
	- person.cs
	- peoplecontroller.cs | sync

*/

// ===========
// Example 2 - same as before but ajax - sending html fragments
// ===========
/*
What
	- trying all ajax options
		- providing the user with feedback while making an ajax request
	- creating ajax links
Files
	- person.cs
	- peoplecontroller.cs | get people async
	- asynchrnous.cshtml
	- asynchrnouspartial.cshtml

*/

