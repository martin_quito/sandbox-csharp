﻿/*
What
	- send specific erro messages and HTTP result codes
	- most apps don't require these because MVC framework will autom generate
	these kind of responds

How
	Send HTTP stats code to the browser 
		- see ErrorController.cs
		- using the 'HttpStatusCodeResult' class
*/