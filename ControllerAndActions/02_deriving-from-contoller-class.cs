﻿/*
Why?
	- build a controller that uses features that the Microsoft MVC Frmaework 
	team has provided

What?
	Three main features that the Controller class provides
		- Action method - A controller's behavior is partitioned into
			multiple methods (instead of of having just one single Execute()
			method) Each action method is exposed on a different URL and is
			invoked with parameters extracted from the incoming request

		- Action results
			Returns an object that describes the result of an action (for
			example rendering a view, redirecting to a diff URL or action method)
			Which is then executed on your behalf. The separation between
			"specifying" reults and "executing" them simpliefies unit testing

		- Filters
			encapsulate reusable behaviors as filters and then
			tag each behavior onto one or more controllers OR action
			methods by putting an attribute in your source code

	The Controller class is also link to the Razor View system

How?
	Example ./Controllers/DerivedController.cs

	How to receive request data
		- extract it from a set of context objects
		- have the data passed as parameters to your action method
		- explicitly invoked the framework's "model binding" feature
			- not explained here!!!
	
	How to receive request data?
		- Getting data from context objects
			When you derive your controller from the Controller Class, you get 
			some properties ato access info about the request. The following 
			properties could be called as "convenience propertiers" because
			each retrieve DIFFERENT types of data from the request's
			'ControllerContext'
				Request
				Response
				RouteData
				HttpContext
				Server

			Request.QueryString
			Request.Form
			Request.Cookies
			Request.HttpMethod
			Request.Headers
			Request.Url
			Request.UserHostAddress
			RouteData.Route
			RouteData.Values
			HttpContext.Application
			HttpContext.Cache
			HttpContext.Items
			HttpContext.Session
			User
			TempData

		- Using action method parameters
			- Action methods can take parameters
			- neater way to receive incoming data from extracting it
			manually from context objects
			- helps with unit testing because I don't need to mock the "conveience
			properties" of the controller class. 

			- cannot have "out" or "ref" paramters
            
			- HOW ARE PARAMETERS OBJECTS ARE INSTANTIATED
				The base "controller" class get values for the
				action method parametes using MVC framework components
				"value providers" and "model binders". The value provider
				fetch items from the Request.Form, Request.QueryString, Request.Files
				and passess them to the model binder that tries to MAP them into 
				the types that your action methods require as parameters
					- more on this in a different project

			- OPTIONAL AND COMPULSORY PARAMETERS
				- value type paramters (compulsory by default)
					- these guys are compulstory because if mvc cannot find a
					value for a value type parameter, it will throw an error
					. To make it optional either make the pamatere NUllable
					or provide a default
				- reference type parameters (optional by default)
					- when mvc cannot find a value, it will assing null,
					, to make it require, add some code to reject "null" values

				You can set defaults 

			- also note if a request contain a value that cannot be converted to the
			correct type, mvc will pass the default value for that parameter type
			and will register the attemped value as an validation error
			in a special context object called "ModelState"


	Producing output
		
		Example in our bare-metal controller, BasicController-producing-output.cs
			- using the "RequestContext.HttpContext.Response.Write"
			- using the "RequestContext.HttpContext.Response.Redirect"			

		- low level 
			- controllers process request and need to generate a response which can be
			done using the "Response.Write" or "Response.Redirect" or other similar
			methods
				Controller.Response (equals) requestContext.HttpContext.Response
					- the "Response" is a HttpResponseBase class

			This approach has some problems
				- the controller classes must contain details of HTML 
				or URL structure which makes the classes harder to read and 
				maintain
				- it is hard to unit test a controller that generates its 
				response directly to the output.???

				- handling the fine detail of every reswponse this way is tedious
				and error-prone
				
				The soltion is action results

		- understanding "Action Results"
			- instead of working with the Response object, mvc uses "Action Results"
			to separate "stating intentions" from "Executing intentions". So,
			an action method returns an object dervied from the "ActionResult"
			class that DESCRIBES what THE RESPONSE from controller WILL (e.g. 
			rendering a view or redirecting or another URL or action method) and 
			this doesn't generate teh resposne directly, the ActionResult object
			gets passed to the MVC Framework to produce the result for you AFTER
			the action method has been invoked.
				- when the MVC framwork receives an "ActionResult" from an action
				method, it calls the "ExecuteResult" method defined in the object.
				Then the action result implementation deals with the "Response"
				object for "generating" the otuput that corressponds to your itnention
				In other words "Action Results" is a wrapper to use the low-level
				response class,  "HttpResponseBase"
					Example: ./Infrastructure/CustomRedirectResult.cs
							./Controllers/DerivedController.cs 

				- this makes testing of controllers so much easier
					- I can test actions and controllers outside a web server
					The context objects are accessed through their base classes
					(HttpRequestBase) which are easy to mock!!

					- I don't need to parse any HTML to test the result of the action method. 
					You can inspect the ActionResult object that is returned to esnure that you 
					receive the expected result

					- I don't need to simulate client requests, The MVC
					framework model binding system allows you to write action
					methods that receive input as method parameters. To test 
					an action method, you simply call the action method 
					direcetly and provide the parameter values that interest you
					


			Built-in "Action Result" TYPESS!
			----------------------------

			ViewResult
				- renders the specified or default view template 
				- helper methods
					- View

			PartialViewResult
				- renders the specifeid or default partial view template
				- helper methods
					- PartialView

			RedirectRouteResult
				- Issues an HTTP 301 or 302 redirection to an action
				method or specific route entry, generating a URL according to your
				routing configuration
				- helper methods
					- RedirectToAction
					- RedirectToActionPermanent
					- RedirectToRoute
					- RedirectToRoutePermanent

			RedirectResult
				- Issues an HTTP 301 or 302 redirection to a specific URL
				- helper methods
					- Redirect
					- RedirectPermanent

			ContentResult
				- returns a raw textual data to the browser, optionally setting
				a content-type header
				- helper methods
					- content    

			FileResult
			JsonResult
				- serializes a .NET object in JSON format and sends it as the response.
				This kind of response is more typically generated using the Web API
				feature.

			JavascriptResult
			HttpUnauthorizedResult
				- sets the response HTTP status code to 401 (meaning "not authorized") 

			HttpNotFoundResult
			HttpStatusCodeResult
			EmptyResult
				- does nothing
				- helper method
					- None


		- Returning html by rendering a view
			public ViewResult Index() {
				return View("Homepage")
			}

			public ViewResult Index() {
				return View("")
			}

			- when mvc calls the executionMethod of the ViewResult, a search
			begins for the view that I specified
				/Areas/<AreaName>/Views/<ControllerName>/<ViewName>.cshtml
				/Areas/<AreaName>/Views/Shared/<ViewName>.cshtml

				or with out the areas

				.aspx, .ascx, .vbhtml are supported too

			- you can also specivy a view by its path (but think what are you
			trying to achieve)
        
	Passing data from an action method to view
		- view model object
			- weakly typed view
			- storng type view
		- passing by view bag (dynamic object)
			- its easyt to send multiple objects 
			- if i was restriected to using 'view models', then I would
			need to create a new type that had a 'string' and 'DateTime' members
			in order to get the same effect
			- example: ./Controllers/DerivedControllers.cs

	Performing redirections
		- use the POST/REDIRECT/GET pattern
			- prevent user resubmitting the POSt a second time
		- when you redirect, you send one of the two HTTP codes to the vrowser
			- 302. temporary redirerction. usualyl used on post/redirect/get
			pattern
			- 301. permanent redreiection.  ???

		- how
			- redirecting to a literal url
				- see the "Example" controller and unit testing
			- redirecting to a routing system url
				- see the "Example" controller and unit testing
			- redirecting to an Action MEthod
				- see the "Example" controller and un				

	How to preserve data across redirection
		- use "TempData" feature
			- use when you want to preserve data across a redirection. A
			redriection causes the browser to submit a new http request, which means
			that you do not have access to the details of the original request
			- similar to the 'Session' data except that thje values in the
			'TempData' are maked for deletion when they are read, and they
			are removed when the request has been processed (not the redirection request)
			- you can use this instead of the viewbag
				- you can use the TempData in the view "@TempData["Message"]
				- to prevent marking for deletiong when reading, you can use
				the peek method 
					DateTime time = (DateTime)TempData.Peek("Date")
				- to preserve a value in the TempData, do
					TempData.Keep("Date")


	How to return Errors and HTTP Codes
		- not concerned at this momen (see page 480)
*/