﻿using System;
using System.Diagnostics;

namespace SomeLibraryYouCantModify {
	public static class SomeStaticClass {
		public static bool SomeStaticMethod(string input) {
			return !string.IsNullOrEmpty(input);
		}
	}
}

namespace SomeLibraryYouCanModify {
	// this class is tightly coupled to the class, I want to separated!
	public class SomeUntstableClass {
		public int SomeMethod(string input) {
			var value = SomeLibraryYouCantModify.SomeStaticClass.SomeStaticMethod(input);
			return value ? 1 : 0;
		}
	}

	// interace
	public interface IStaticWrapper {
		bool SomeStaticMethod(string input);
	}

	// non-static wrapper class (this can be mokced)!
	public class StaticWrapper : IStaticWrapper {
		public bool SomeStaticMethod(string input) {
			return SomeLibraryYouCantModify.SomeStaticClass.SomeStaticMethod(input);
		}
	}

	// this is the wrapper to the tighly coupled class I had earlier
	// this is the better version of 'SomeUnstableClass'
	public class WrapperMethod {
		IStaticWrapper _wrapper;
		public WrapperMethod(IStaticWrapper wrapper) {
			_wrapper = wrapper;
		}
		public int SomeMethod(string input) {
			var value = _wrapper.SomeStaticMethod(input);
			return value ? 1 : 0;
		}
	}

	// testing classes and methods
	public class WrapperMethodTests {
		private class TestWrapper : IStaticWrapper {
			public bool SomeStaticMethod(string input) {
				return !string.IsNullOrEmpty(input);
			}
		}

		public void SomeMethod_GiveNull_ShouldReturnZero() {
			var wrapper = new TestWrapper();
			var wm = new WrapperMethod(wrapper);
			var output = wm.SomeMethod(null);
			// Assert.AreEqual(0, output)
		}
	}
}

