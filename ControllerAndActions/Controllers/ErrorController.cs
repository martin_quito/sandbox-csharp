﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllerAndActions.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
			ViewBag.Message = "Hello";
			ViewBag.Date = DateTime.Now;
			return View();
        }

		public RedirectToRouteResult Redirect() {
			return RedirectToAction("Index");
		}
		

		// Sending a 404 Result
		public HttpStatusCodeResult StatusCode() {
			return new HttpStatusCodeResult(404, "URL cannot be servied");
		}

		// same as the preivous 
		public HttpStatusCodeResult StatusCodeA() {
			return HttpNotFound(); // helper
		}

		// Sending a 401 Result
		public HttpStatusCodeResult StatusCodeC() {
			return new HttpUnauthorizedResult();
		}
    }
}