﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControllerAndActions.Infrastructure;

namespace ControllerAndActions.Controllers
{
    public class DerivedController : Controller
    {
		// =============
        // simple
		// =============
        public ActionResult Index()
        {
            ViewBag.Message = "Hello from the Dervied Controller index method";
            return View("MyView");
        }

		// =============
		// getting from context objects
		// =============
        // getting info from the controller's props (Request, etc.)
        public ActionResult RenameProduct() {
            // access various properties from context objects
            string userName = this.User.Identity.Name;
            string serverName = this.Server.MachineName;
            string clientIP = this.Request.UserHostAddress;
            DateTime dateStamp = this.HttpContext.Timestamp;

            //AuditRequest(userName, serverName, clientIP, dateStamp
                //, "Renaming Product");
            string oldProductName = Request.Form["OldName"];
            string newProductName = Request.Form["NewName"];
            //bool result = AttemptProductRename(oldProductName, new ProductName);

            //ViewData["RenameResult"] = result;
            return View("ProductRenamed");
        }

		// =============
		// getting it from action method parameters
		// ==============
        // one way to get data from request
        public ActionResult ShowWeatherForecast() {
            string city = (string)this.RouteData.Values["city"];
            DateTime forData = DateTime.Parse(Request.Form["forData"]);
            return View();
        }

        // another way to get data (action method parameters)
        public ActionResult ShowWeatherForecast(string city, DateTime forData) {
            // ...
            return View();
        }

        // default parameters
        public ActionResult Search(string query = "all", int page = 1) {
            //
            return View();
        }

		// ==============
		// Producing output
		// ==============
        // redirect
        public void ProduceOutput() {
            if (Server.MachineName == "TINY") {
                this.Response.Redirect("/Basic/Index");
            }
            else {
                Response.Write("Controller: Derived, Action: ProduceOutput");
            }
        }

		// sing action results
        public ActionResult ProduceOutput2() {
            if (Server.MachineName == "IQ111") {
                return new CustomRedirectResult { Url = "/Basic/Index" };
                //new RedirectResult("/Basic/Index");
            }
            else {
                Response.Write("Controller: Derived, Action: ProductOutput");
                return null;
            }
        }

		// ==============
		// use the follwoing for test
		// ==============
        public ViewResult SimpleHTML() {
            return View("HomePage");
        }

        public ViewResult Specific() {
            return View("~/Views/Other/Index.cshtml");
        }


		// ==============
		// passing a View Model Object 
		// ==============
		// - you can access the obejct in the view using the Razor Model keyword
		// - see ./Views/Derived/ViewModel
        public ViewResult ViewModel() {
            DateTime date = DateTime.Now;
            return View(date);
        }

		// ==============
		// passing a View Bag
		// ==============
		public ViewResult MyViewBag() {
			ViewBag.Message = "Hello";
			ViewBag.Date = DateTime.Now;
			return View();
		}


        // ===================
        // Redirection
        // ===================
        // redirect literl url (302)
        public RedirectResult Redirect() {
            return Redirect("/Example/Index");
        }

        // 301 - permanent
        public RedirectResult Redirect2() {
            return RedirectPermanent("/Example/Index");
        }
        // the problems with these is that if we update the
        // routing schema, these url literals must get updated

        // redirecting to a routing system url
        public RedirectToRouteResult Redirect3() {
            // create a redirection so it follows the routing schema
            return RedirectToRoute(new {
                controller = "Example",
                action = "Index",
                ID = "MyID"
            });

            // for permanent, use the "RedirectToRoutePermanent"
        }

		// this is just a rwapper to RedirectToRoute method
		public RedirectToRouteResult Redirect4() {
			return RedirectToAction("Index");
			//return RedirectToAction("Index", "ControllerName);
		}

        // ===================
        // TempData
        // ===================
        public RedirectToRouteResult RedirectToRoute() {
            TempData["Message"] = "Hello";
            TempData["Date"] = DateTime.Now;
            return RedirectToAction("Receive");
        }

        public ViewResult Receive() {
            ViewBag.Message = TempData["Message"];
            ViewBag.Date = TempData["Date"];
            return View();
        }

    }
}