﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllerAndActions.Controllers
{
    public class ExampleController : Controller
    {
        // GET: Example
        public ActionResult Index()
        {
			ViewBag.Mesage = "Hello";
			ViewBag.Date = DateTime.Now;
			return View();
        }
    }
}