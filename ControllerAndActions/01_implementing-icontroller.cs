﻿/*
What?
	- all controllers implement IController 
	
	

	- HttpContext and HttpContextBase are the same except the base is newer and 
	abstract. HttpContextBase instance is pass to the Execute method defined 
	by the IController. This "base" classes came to be more well suited for 
	testability. 
		- if you need an instance of the newwer class, you can wrapped 
		withHttpContextWrapper
			HttpContext myContext = getOriginalObjectFromSomwhere();
			HttpContextBase myBase = new HttpContextWrapper(myContext);

	- you can implement IController to any way you want to handle requests and 
	output results. Don't like aciton methods, create your own way. Or you can 
	use the "System.Web.Mvc.Controller" MVc class that MIcrosoft provides. 
             
How?
	public interface IController {
		void Execute(RequestContext requestContext)
	}

		- Execute is invoked when a request is targeted at the controller 
		class. MVC framework knows which controller class has been targeted in 
		a request by reading the value of "controller" property genertaed by 
		the routing data or cstom routing classes
			- MVC uses the routing data to determine what controller to use?
			- you can create a controller by implementing IController 
			but it takes a lot of work
				- example: ./Controllers/BasicController.cs

		- the Execute expects an argument "System.Web.Routing.RequestContext"
		that provides info about the request and the route that got matched
			- properties of the "RequestContext" class

				- "HttpContext"
					- returns an "HttpContextBase" obj that describes
					the describes the current request

				- "RouteData"
					- returns an "RouteData" object that describes the route
					that matched the request
					- some properties
						- "Route"
						- "RouteHandler"
						- "Values"


Why?
	- buildign controllers from scratch provides great flexibility


*/