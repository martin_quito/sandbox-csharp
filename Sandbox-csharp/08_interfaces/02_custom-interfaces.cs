﻿/**
 * What?
 *	- use the 'interface' keyword
 *	- never specify a base class (not even SYstem.Object)
 *	- can specify base interfaces
 *	- members of an interface never specify an access modifier (all interface members are implicitly public and abstract)
 *	- members of the interface don't need implementation
 *	- cannot have data fields
 *	- cannot have constructors
 *	- can have property prototypes
 *	- they can be used as PARAMETERS
 *	- they can be used as return types
 */
using System;
namespace customInterfaces {
	
	public class Utensil { }

	public abstract class Shape 
	{
		public string Name { get; set; }
		public Shape() { }
		public Shape(string name)
		{
			this.Name = name;
		}

		public abstract void Draw();
	}

	// ===============
	// simple interfaces
	// ===============
	public interface IPointy
	{
		// implicitly public and abstract
		//byte GetNumberOfPoints();

		// read-write property 
		//int dummy1 { get; set; }

		// write-only property
		//int dummy2 { set; }

		byte Points { get; }
	}

	public interface IDraw3D
	{
		void Draw3D();
	}

	// ===============
	// implemeting
	// ===============
	// - this class derives from System.Object  and
	// implements a single interface
	//public class Pencil : IPointy
	//{

	//}

	// This class also dervies from System.Object 
	// and implements a single interface
	//public class SwitchBlade : Object, IPointy
	//{

	//}

	// This class derives from a custom base class
	// and implements a single interface
	//public class Fork : Utensil, IPointy
	//{

	//}

	// This struct implicitly derives from SYstem.ValuyeType
	// and implements two interfaces
	//public struct PitchFor : ICloneable, IPointy
	//{

	//}

	class Triangle : Shape, IPointy
	{
		public Triangle() { }
		public Triangle(string name) : base(name) { }
		public override void Draw()
		{
			Console.WriteLine("Drawing {0} the Trianage", Name);
		}

		// implen eting
		public byte Points
		{
			get { return 3; }
		}
	}

	class Hexagon : Shape, IPointy, IDraw3D
	{
		public Hexagon() { }
		public Hexagon(string name) : base(name) { }
		public override void Draw()
		{
			Console.WriteLine("Drawing {0} the Hexagon", Name);
		}
		public byte Points
		{
			get { return 6; }
		}

		public void Draw3D()
		{
			Console.WriteLine("Drawing a hexagon in 3D!");
		}
	}

	class Fork : IPointy
	{
		public byte Points { get { return 1; } }
	}

	class PitchFork : IPointy
	{
		public byte Points { get { return 2; } }
	}

	class Knife : IPointy
	{
		public byte Points { get { return 3; } }
	}

	// ================
	// start main
	// ================
	class Program
	{
		static void Main()
		{
			// ==============
			// invoking interface members at the object level
			// ==============
			Hexagon hex = new Hexagon();
			Console.WriteLine("Points: {0}", hex.Points);

			// ==============
			// how to dynamically know if object supports an interface
			// ==============
			// - what if you have a collection of Shape compatible types
			// where only some of which support IPointy, and obviously, if
			// you attemp to invoke the Points property on a type that that has not implemented
			// IPoint, you would receive an error. One way is to use an 
			// explicit CAST!
			Hexagon hex1 = new Hexagon("List");
			IPointy itfPt = null;
			try
			{
				itfPt = (IPointy)hex1;
				Console.WriteLine(itfPt.Points);
			}
			catch (InvalidCastException e)
			{
				//
			}

			// - it would be ideal to determine which interfaces are supported
			// before invoking the interface members

			// ==============
			// using 'as'
			// ==============
			Hexagon hex2 = new Hexagon("Peter");
			IPointy pt2 = hex2 as IPointy;
			if (pt2 != null) {
				Console.WriteLine("Points: {0}", pt2.Points);
			}
			else
			{
				Console.WriteLine("OPPS! Not pointy ...");
			}

			// ==============
			// using 'is'
			// ==============
			Hexagon hex3 = new Hexagon("Peter");
			if (hex3 is IPointy)
			{
				Console.WriteLine("MY points: {0}", hex3.Points);
			}
			else
			{
				Console.WriteLine("OPPS! Not pointy ...");
			}

			// ==============
			// using as paramenters
			// ==============
			Hexagon hex4 = new Hexagon("Martin");
			DrawIn3D(hex4);

			Triangle tri1 = new Triangle("Meow");
			//DrawIn3D(tri1);	// doesn't work

			// ==============
			// interfaces as return values
			// ==============
			Shape[] myShapes = 
			{
				new Triangle("Meow"), new Hexagon("2")
			};
			Console.WriteLine("My Shapes");
			Console.WriteLine(FindFirstPointShape(myShapes).Points);

			// ==============
			// arrays of interface types
			// ==============
			IPointy[] myPointyObjects = 
			{
				new Hexagon(), new Knife(), new Triangle(),
				new Fork(), new PitchFork()
			};
			// this is powerful programming constructs

		}

		static void DrawIn3D(IDraw3D d)
		{
			Console.WriteLine("-> Drawing IDraw3D compatabiel type");
			d.Draw3D();
		}

		static IPointy FindFirstPointShape(Shape[] shapes)
		{
			foreach (Shape s in shapes)
			{
				if (s is IPointy)
				{
					return s as IPointy;
				}
			}
			return null;
		}
	}
}