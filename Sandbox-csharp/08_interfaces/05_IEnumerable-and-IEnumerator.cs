﻿/**
 * What is IENumerable?
 *	- it has a member called "GetNumerator" and any class or struct
 *	that implements that interface can use the 'foreach'. Also note that
 *	the 'GetNumerator' returns a 'IEnumerator'. This IENumerator "Provides
 *	the infrastructure to allow the caller to traverse the internal objects contained by the IEnumerable-compatible container" (book1)
 *	
 * How?
 *	- check the defnitions of IEnumerator and IEnumerable
 *	
 */
using System;
using System.Collections;
namespace e
{
	// ====================
	// iterating
	// ====================
	class Car {
		public string Name { get; set; }
		public int Age { get; set; }
		public Car() { }
		public Car(string name, int age)
		{
			this.Name = name;
			this.Age = age;
		}
	}
	public class Garage
	{
		private Car[] carArray = new Car[4]; 
		public Garage()
		{
			carArray[0] = new Car("Rusty", 30);
			carArray[1] = new Car("Clinker", 30);
			carArray[2] = new Car("Zippy", 30);
			carArray[3] = new Car("Fred", 30);
		}
	}

	public class Garage2 : IEnumerable
	{
		private Car[] carArray = new Car[4];
		public Garage2()
		{
			carArray[0] = new Car("Rusty", 30);
			carArray[1] = new Car("Clinker", 30);
			carArray[2] = new Car("Zippy", 30);
			carArray[3] = new Car("Fred", 30);
		}

		//public IEnumerator GetEnumerator()
		//{
		//	return this.carArray.GetEnumerator();
		//}

		// makes it private
		IEnumerator IEnumerable.GetEnumerator() {
			return this.carArray.GetEnumerator();
		}
	}

	class Program
	{
		static void Main()
		{

			// ====================
			// not implemetngin IENumerable
			// ====================
			Garage carLot = new Garage();
			//foreach (Car c in carLot)
			//{
			//	Console.WriteLine(c.Name);
			//}
			// - it will not work because compiler
			// informs that the Garage class does not implement a method
			// named GetEnumerator()

			// ================
			// using IEnumerable
			// ================
			// - well since we want the Garage type to support thee interafaces
			// , you could take a long road to implement each method from the IEnumerator
			// but you can use delegation. System.Array already implement IEnumerable
			// and IEnumerator. 
			Garage2 carLot2 = new Garage2();
			foreach (Car c in carLot2)
			{
				Console.WriteLine(c.Name);
			}

			// - if you want to hide carLot2.GetEnumerator() you can
			// can make use of explicity interface implementation
		}
	}
}