﻿/*
 * What?
 *   - IComparable or IComparable<T> specifies a behavior that allows
 *   an object to be sorted based on some specified key.
 *   - System.array class defines a static method named "Sort()" When this 
 *   method is used against an array of intrinsic types (int, short, string, etc)
 *  you are able to sort the items in the array in numeric/alphabetic order, as these intrinsic
 *  data types implement IComparable. But what about custom reference types? The reference type
 *  needs to implement IComparable
 *  - if you want to sort with multiple data, use IComparer
 */
using System;
using System.Collections.Generic;
namespace InterfaceIComparable {
    // =============
    // implementing IComparable
    // =============
    public class Car : IComparable<Car>
    {
        public int CarID { get; set; }
        public int CurrentSpeed { get; set; }
        public string PetName { get; set; }
        public Car(string name, int currSp, int id) {
            CurrentSpeed = currSp;
            PetName = name;
            CarID = id;
        }


        int IComparable<Car>.CompareTo(Car other) {
            if (this.CarID > other.CarID) {
                return 1;
            }
            if (this.CarID < other.CarID) {
                return -1;
            }
            else {
                return 0;
            }
            /* return value
             * ------------
             * < 0     => this instance comes before the specified object in the sort order
             * 0       => this instance is equal to the specified object
             * > 0     => this instance comes after the specified object in the sort order
             */
        }

        // - you can streamline the previous implementation given
        // the fact that int32 (carId) implements IComparable
        //int IComparable<Car>.CompareTo(Car other) {
        //    return this.CarID.CompareTo(other.CarID);
        //}
    }

    class P {
        static void Main() {
            // implemetning IComparable
            Car[] cars = {
                new Car("Rusty", 80, 1),
                new Car("Mary", 40, 234),
                new Car("Viper", 40, 34),
            };
            Array.Sort(cars);
            foreach (Car i in cars) {
                Console.WriteLine(i.CarID);
            }

            // sorting using multiple data points

        }
    }

	// =============
	// ICompare
	// =============
	public class Car2
	{
		public int CarID { get; set; }
		public int CurrentSpeed { get; set; }
		public string PetName { get; set; }
		public Car2(string name, int currSp, int id)
		{
			CurrentSpeed = currSp;
			PetName = name;
			CarID = id;
		}

		// custom propertyies and custom sort type
		public static IComparer<Car2> SortByPetName
		{
			get
			{
				return new PetNameComparer();
			}
		}
	}

	public class PetNameComparer : IComparer<Car2>
	{
		int IComparer<Car2>.Compare(Car2 o1, Car2 o2)
		{
			return String.Compare(o1.PetName, o2.PetName);
		}
	}

	public class P2
	{
		static void Main()
		{
			Car2[] cars = {
                new Car2("Rusty", 80, 1),
                new Car2("Mary", 40, 234),
                new Car2("Viper", 40, 34),
            };
			Array.Sort(cars, new PetNameComparer());
			foreach(Car2 i in cars) {
				Console.WriteLine(i.PetName);
			}

			Array.Sort(cars, Car2.SortByPetName);
		}
	}
}
