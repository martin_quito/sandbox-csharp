﻿/*
 * What?
 *	- interfaces can be arranged in an interface hierarchy
 *	- like a class hierarchy, when an interface extends an existing interface, it inherits the abstract members
 *	defined by the parent(s). Of course, unlike class based inheritance, the
 *	dervied interfaces never inherit true implemetnation. Rather a derived interface simply extends its own definiton
 *	with additional absttract members
 *	
 * 
 */
using System;

namespace interfaceHierarchy {
	// ================
	// a simple hierarchy
	// ================
	public interface IDrawable
	{
		void Draw();
	}

	public interface IAdvancedDraw : IDrawable
	{
		void DrawInBoundingBox(int top, int left, int bottom, int right);
		void DrawUpsideDown();
	}

	public class BitmapImage : IAdvancedDraw
	{
		public void Draw()
		{
			Console.WriteLine("Drawing ...");
		}

		public void DrawInBoundingBox(int top, int left, int bottom, int right)
		{
			Console.WriteLine("Drawing in a box ...");
		}

		public void DrawUpsideDown()
		{
			Console.WriteLine("Drawing upside down!");
		}
	}

	public class Program
	{
		static void Main()
		{
			BitmapImage i = new BitmapImage();
			i.Draw();
			i.DrawInBoundingBox(10, 10, 10, 10);
			i.DrawUpsideDown();


		}
	}

	// ================
	// multiple inhertiance with interface types
	// ================
	interface IDrawable2 { void Draw(); }
	interface IPrintable { void Print(); void Draw(); } // note possiel name class here!
	interface IShape : IDrawable2, IPrintable { int GetNumberOfSides(); }

	// - how many methods will it be required to implement?
	// - it depends, if you want to provide a simple implementation of the "Draw()" method
	// you need three members
	class Rectangle : IShape
	{
		public int GetNumberOfSides()
		{
			return 4;
		}
		public void Draw()
		{
			Console.WriteLine("Drawing ...");
		}
		public void Print()
		{
			Console.WriteLine("Printing ...");
		}
	}

	// if you want a specific implementation for each Draw() method
	class Square : IShape
	{
		void IPrintable.Draw()
		{
			// Draw to printer
		}
		void IDrawable2.Draw()
		{
			// Draw to screen
		}
		public void Print()
		{

		}
		public int GetNumberOfSides()
		{
			return 4;
		}
	}
}