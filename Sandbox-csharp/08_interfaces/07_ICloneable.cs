﻿/**
 * What?
 *  - 
 */

using System;
namespace InterfaceICloneable {
    // ==========
    // implementing Clone()
    // ==========
    public class Point : ICloneable {
        public int X { get; set; }
        public int Y { get; set; }
        public Point(int xPos, int yPos) { X = xPos; Y = yPos; }
        public Point() { }

        public override string ToString() {
            return string.Format("X = {0}, Y = {1}", X, Y);
        }
        
        public object Clone() {
            return new Point(this.X, this.Y);
        }
    }
    public class P {

        static void Main() {
            Point p3 = new Point(100, 100);
            Point p4 = (Point)p3.Clone();
            p4.X = 0;
            Console.WriteLine(p3);
            Console.WriteLine(p4);
        }
    }
    // ==========
    // implementing Clone() using MemberwiseClone()
    // ==========
    public class Point2 : ICloneable {
        public int X { get; set; }
        public int Y { get; set; }
        public Point2(int xPos, int yPos) { X = xPos; Y = yPos; }
        public Point2() { }

        public override string ToString() {
            return string.Format("X = {0}, Y = {1}", X, Y);
        }

        public object Clone() {
            // - copies each field of the Point member by member
            // - however, if there are any reference type member variables
            // MemberwiseClone() will copy the references to those objects (shallow copy)
            return this.MemberwiseClone();
        }
    }
    public class P2 {

        static void Main() {
            Point2 p3 = new Point2(100, 100);
            Point2 p4 = (Point2)p3.Clone();
            p4.X = 0;
            Console.WriteLine(p3);
            Console.WriteLine(p4);
        }
    }

    // ==========
    // more elaborate clonign example
    // ==========
    public class PointDescription {
        public string PetName { get; set; }
        public Guid PointID { get; set; }
        public PointDescription() {
            PointID = Guid.NewGuid();
        }
    }
    public class Point3 : ICloneable {
        public int X { get; set; }
        public int Y { get; set; }
        public PointDescription desc = new PointDescription();

        public Point3(int xPos, int yPos, string petName) {
            X = xPos;
            Y = yPos;
            desc.PetName = petName;
        }
        public Point3(int xPos, int yPos) : this(xPos, yPos, "no-name") { }
        public Point3() { }

        // using a shallow copy on references
        //public object Clone() {
        //    return this.MemberwiseClone();
        //}

        // a deep copy
        public object Clone() {
            Point3 newPoint = (Point3)this.MemberwiseClone();
            PointDescription currentDesc = new PointDescription();
            currentDesc.PetName = this.desc.PetName;
            newPoint.desc = currentDesc;
            return newPoint;
        }
    }

    public class P3 {
        static void Main() {
            Point3 p3 = new Point3(100, 100, "Jane");
            Point3 p4 = (Point3)p3.Clone();
            p4.desc.PetName = "My new Point";
            p4.X = 9;

            Console.WriteLine(p3.desc.PointID);
            Console.WriteLine(p4.desc.PointID);
                // - they are pointing to the same object!
                
        }
    }

    // so, if you are cloning a class or structure that contains nothing but value
    // types, implement your Clone() method using MemberwiseClone().
    // However, if you have a custom type that maintains other refernce types, you 
    // might want to create a new object that takes into account each
    // reference type member variable in order to get a "deep copy"
}