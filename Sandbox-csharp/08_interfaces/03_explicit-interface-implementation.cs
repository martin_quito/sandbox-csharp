﻿/**
 * What happens if there is a name clash withing the interfaces the class or struct exptend to?
 * - it will not work as expected which then you will need to use explicit interface implementation 
 * 
 * What?
 *	- these guys don't take access modifiers, they are automatically private
 *  - becauset they are private you cannot access through the object level
 */
using System;

namespace explicitInterface
{
	// ==============
	// name clash!
	// ==============
	// Draw image to a form
	public interface IDrawToForm { void Draw();}

	// Draw to buffer in memory
	public interface IDrawToMemory { void Draw();}

	// Render to the printer
	public interface IDrawToPrinter { void Draw();}

	class Octagon : IDrawToForm, IDrawToMemory, IDrawToPrinter
	{
		public void Draw()
		{
			Console.WriteLine("Drawing the Octagon ...");
		}
	}

	public class A
	{
		static void Main()
		{
			Octagon oct = new Octagon();
			IDrawToForm itfForm = (IDrawToForm)oct;
			itfForm.Draw();
			IDrawToPrinter itfPrinter = (IDrawToPrinter)oct;
			itfForm.Draw();
			IDrawToMemory itfMemory = (IDrawToMemory)oct;
			itfForm.Draw();
		}
	}

	// ==============
	// solution is explicit interface implementation
	// ==============
	class Octagon2 : IDrawToForm, IDrawToMemory, IDrawToPrinter
	{
		void IDrawToForm.Draw()
		{
			Console.WriteLine("Drawing to form ...");
		}
		void IDrawToPrinter.Draw()
		{
			Console.WriteLine("Drawing to printer ...");
		}
		void IDrawToMemory.Draw()
		{
			Console.WriteLine("Drawing to memory ...");
		}
	}

	public class B
	{
		static void Main()
		{
			Octagon2 oct = new Octagon2();
			// we now must use casting to access the Draw();
			IDrawToForm itfForm = (IDrawToForm)oct;
			itfForm.Draw();
			((IDrawToPrinter)oct).Draw();
			((IDrawToMemory)oct).Draw();

			// - as you can see here, the draw method starts from the bottom
			// on the type of the container that is holding
		}
	}
}
