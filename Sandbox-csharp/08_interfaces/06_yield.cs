﻿/*
What?
	- an alternative way to build types that work with the foreach loop. When we want to foreaech a custom collection,
	we need to implement the IEnumerator interface. Instead of doing that, keeping the same name and return type, we can 
	use the keyword "yield" to do the same behavior

	- conclusion. "Remmeber that for your custom types to
	work with the C# foreach keyword, the container must define a method
	GetEnumerator(), which has been formalized by the IEnumerable interface type.
	The implementation of this method is typically achieved by simply delegating it to the
	internal member that is holding onto the subobjects; however, it is also
	possible to make use of the 'yield return' syntax to provide multiple 'named interator' methods" (first)
How?
	- use an 'iterator', see belowq
	- you can use named iterators where it can be powerful because you can define multiple ways
	to request the returned set
*/
using System;
using System.Collections;

namespace yield
{
	public class Car {
		public string Name { get; set; }
	}
	public class Garage
	{
		private Car[] carArray = {
			new Car {Name = "Martin1"},
			new Car {Name = "Martin2"},
			new Car {Name = "Martin3"},
			new Car {Name = "Martin4"}
		};
		// interator method
		public IEnumerator GetEnumerator()
		{
            foreach (Car c in carArray) {
                yield return c;
            }

            // or
            //return carArray.GetEnumerator();
            // -> this says that I don't need to implement the interface
            // because when I loop Garage, it will look
            // if I have the "GetEnumerator" method
		}
		// we can also do this
		//public IEnumerator GetEnumerator()
		//{
			// yield return carArray[0];
			// yield return carArray[1];
			// yield return carArray[0];
			// yield return carArray[3];
		//}

		// named iterator
		// - it can receive parameters
		// - it returns an IEnumerable instead of an IEnumerator
		public IEnumerable GetTheCars(bool AddM)
		{
			foreach (Car c in carArray)
			{
				yield return c.Name + 'M';
			}
		}
	}

	public class Program
	{
		static void Main()
		{
			Garage g = new Garage();
			// get the items using the GetEnumerator();
			foreach (Car c in g)
			{
				Console.WriteLine(c.Name);
			}
			// get the items using named iterator
			foreach (string c in g.GetTheCars(false))
			{
				Console.WriteLine(c);
			}
		}
	}
}