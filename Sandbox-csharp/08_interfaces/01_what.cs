﻿/*
 * What is an interface?
 *	- named set of abstract members that express a behavior that
 *	a given class or structure may choose to support!
 *	
 * Example
 *	- ICloneable. Many classes implement this interface which means that even though
 *	thse types have no common parent (besides the System.Object) WE CAN TREAT
 *	THEM POLYMORPHICALLY VIA the Icloneable interface type
 *	
 * Abstract vs intefcae?
 *	- with an abstract class, you can choose to define
 *	abtrsract memebrs but with interface, it can only
 *	OBTAIN ABSTRACT MEMBERS DEFINITIONS
 * - a polymorphic interface (virtual or abstract) method in an abstract
 * class suffers a couple of limitations
 *		- only derived types support the members defined by the abstract parent
 *		but an interface doesn't, "it can be implemented by any
 *		class or structure, in any hierarchy, within any namespace or any assembly
 *		(written in any .NET programming language)." (Book 1)
 *		- "each derived type must contend with the set of abstract members
 *		and provide an implementation" (Book 1)
 *	
 * Benefits?
 *	- interfaces by themselves don't do anything until they are
 *	implemented by a class or structuer
 *	
 * when would interfaces would be useful?
 *	- when you have a single hierarchy where only a subset of the dervied types supports a
 *	common behavior
 *	- you need to model a common behavior that is found across multiple hierarchies
 *	with no common parent class beyond (System.Object)
 *	
 * 
*/

using System;
namespace interfaces
{
	// =============
	// limitation in an abstract
	// =============
	// - only dervied types can support this "polymorphic interface" 
	// classes in other hierarchy have no access to this abstract member
	public abstract class CloneableType
	{
		
	}
	public class Car {}

	// not possible to have multiple inheritance
	//public class MiniVan : Car, CloneableType { }

	// =============
	// interface to the resuce
	// =============
	//public interface ICloneable {
	//	object clone();
	//}

	// ==========
	// treating unrelated class types to be same 
	// ==========
	class Program
	{
		static void Main()
		{
			string myStr = "Hello";
			OperatingSystem unixOS = new OperatingSystem(PlatformID.Win32Windows, new Version());
			System.Data.SqlClient.SqlConnection sqlCnn = new System.Data.SqlClient.SqlConnection();

			CloneMe(myStr);
			CloneMe(unixOS);
			CloneMe(sqlCnn);
		}

		private static void CloneMe(ICloneable c)
		{
			object theClone = c.Clone();
			Console.WriteLine("Your Clone is a : {0}", theClone.GetType().Name);
		}
	}
}