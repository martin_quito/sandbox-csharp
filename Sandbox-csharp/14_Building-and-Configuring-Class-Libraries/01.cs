﻿/*
 * Why?
 *	- we need custom namespaces because it used to organized my types. When I build large apps with many types, it will
 *	be helpful to group my tpes. Also,
 *	
 * What?
 *	- a container to group namespaces
 *	
 * How?
 * - Explicity defining custom namespaces is important because other developers will refernece my library (or custom library, *.dll files)
 * and will import my custom namespaces in order to use my types
 * - you can create a namespace in one files or in multiple files. 
 * - Resolving Name clashes with "Fully Qualified Names"
 *		- technically speaking, you are noit required to use the C# using keyword when referring types defined in extranl
 *		namespaces
 *		- using it or not does not affect the size of execution speed, it just saves keystrokes, in face, the CIL code
 *		, types are always defined with the fully qualiefd name. C# "Using" keyword is simply a typing time saver.
 *		- it can help in avoiding potential name clashes when multiple namespaces that contain identically name types. See 01c.cs
 *		- you can use aliases to resolve name clashes
 *			- see 01d.cs
 *	- creating Nested namespaces
 *		- its available for more deeper organization. see 01e.cs
 *		- sometimes the root namespace will not contain types and only namespaces
 * 
*/
// ===============
// simple creation
// ===============
using System;
namespace MyShapes
{
	// Circle Class
	public class Circle { }
	// Hexagon Class
	public class Hexagon { }
	// Square class
	public class Square { }
}

namespace My3DShapes
{
	// Circle Class
	public class Circle { }
	// Hexagon Class
	public class Hexagon { }
	// Square class
	public class Square { }
}

// ===============
// splitting
// ===============
// for example, this can be in the Trapezoid.cs file
namespace MyShapes
{
	public class Trapezoid { }
}
// for example, this can be in the 3D.cs file
namespace MyShapes
{
	public class ThreeD {}
}

