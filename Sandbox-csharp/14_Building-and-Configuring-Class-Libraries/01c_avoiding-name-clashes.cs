﻿using MyShapes;
using My3DShapes;

namespace CustomNamespaces
{
	public class Program
	{
		static void Main(string[] args)
		{
			//Hexagon h = new Hexagon();
			//Circle c = new Circle();
			//Square s = new Square();
				// all of them will cry, its ambigious
			My3DShapes.Hexagon h = new My3DShapes.Hexagon();
			My3DShapes.Circle c = new My3DShapes.Circle();
			MyShapes.Square s = new MyShapes.Square();
		}
	}
}