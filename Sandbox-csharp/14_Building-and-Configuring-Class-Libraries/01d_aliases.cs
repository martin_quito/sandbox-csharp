﻿using System;
using MyShapes;
using My3DShapes;

// using it with a type
using The3DHexagon = My3DShapes.Hexagon;
// using it with a namespace
using bfHome = System.Runtime.Serialization.Formatters.Binary;

namespace CustomNamespaces
{
	class _01d
	{
		static void Main(string[] args)
		{
			// This is really creating a a My3DShapes.Hexagon class
			The3DHexagon h2 = new The3DHexagon();

			bfHome.BinaryFormatter b = new bfHome.BinaryFormatter();
		}
	}
}
