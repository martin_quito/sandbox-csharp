﻿/*
 * I'm using the custom namespace I defined in file 01.cs
 */
using System;
using MyShapes;

namespace CustomNamespaces
{
	public class ProgramG
	{
		static void Main(string[] args)
		{
			Hexagon h = new Hexagon();
			Circle c = new Circle();
			Square s = new Square();
		}
	}
}