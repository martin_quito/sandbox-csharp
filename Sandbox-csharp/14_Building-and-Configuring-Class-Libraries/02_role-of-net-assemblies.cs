﻿/*
What?
	- .net applications is contruscted by piecing together any number of "Assemblies"
	- an assembly is a versioned, self-descripting, binary code hosted by the CLR 
	- even those .NET assembleis have the same extension (.dll or .exe) as previous windows binaries,
	they have very little in common with those files under the hood

What are some features
	Assemblies promoto code reuse
		- regardless how the code library is package, .NEt platform allows you to reuse types in a language-
		independent mannger. For example, I could create a code library in C# and reuse that library in any 
		other .NET language. I can even inherite it from. 

		- the console apps I've been building, it might have seen that all of the application's 
		functionality was contained withing the executable assembly I was constructing, but
		in reality, my application was leveraging types from other assemblies, especially
		from mscorlib.dll. (code library)			
			- a code library or class library is a .dll that contains types
			INTENDED to be used by external applications.

	Assemblies establish a type boundary
		- two libraries such as "MyCars.dll and YourCars.dll" that both define
		a namespace (CarLibrary) containing a class named "SportsCar", they are considered
		unique types in the .NET universe

	Assemblies are verionable units
		- .net assemblies are assigned a four-part numerical version number
			<major><minor><build><revision>. This number along with a public key number
			allows multiple version of the same assemble to coexist on a single machine.

		- assemblies that have a public key are termed "strongly named"

	Assemblies are self-describing
		- an assembly has a manifest file that describe the assembly such as the external
		assemblies it needs to functiona properly
		- in addition to the manifest file, it also has a "metadata" file that describes
		the composition (member names, implemented interfaces, base classes, constructors, and so forth)
		of every contained type. Becasuse of this, the CLR does not consult windows system registry
		to resolve its location. The CLR makes use of an entirely new scheme to resolve the
		location of external code libraies. 

	Assemblies are configuratble
		- assemblies can be deploued as private or shared. Private assemblies live in the same directory
		or possibly subdirectory as the client application that uses them. Shared assemblies
		are meant to be consumed by multiple applications in the same machine and are deployed to a specific
		directory termed "global assembly cache" (GAC)

		- you can use the "XML based configuration" to tell teh CLR "probe" for assemblies at a specific location
		, load a specific version of a referenced assembly for a particular clinet, or consult 
		an arbitrary directory on your local machine, your network location, or a web-based URL.

How?
	- A .NET assembly (.dll or .exe) has the following elements
		- windows file header
			- establishs the fact that the assembly can be manipulated by the windows family
			- defins the type of application (console, gui, or .dll code library)
			- care about this when building a new .NET language compiler
		- CLR file header
			- a block fo data that all .NET assemblies must support in order to be hosted by the CLR
			- it contains flags that identity the location of the metadata and resources
			witing the file, the version of the runtime the assembly was built against, the value
			of the public key, and so fort
		- CIL code
			- is a platform-and CPU agnostic intermediate language
			- at runtime, this code, CIL code, is compiled on the fly using JIT compiler according
			to the platform and CPU specific instructions.
		- type metadata
			- completly describes the FORMAT of the contained types as well as the
			FORMAT of EXTERNAL TYPES referenced by this assembly
			- the .NET runtime uses this info to resolve the location of types (and their members)
			within the binary, lay out in memory and facilitate remote method invocations
		- assembly manifest
			- documents each MODULE withing the assembly, establishes the version of the assembly,
			and also documents ANY EXTERNAL ASSEMBLIES REFERENCED BY THE CURRENT ASSEMBLY.
			
		- optional embedded resources 
			- application icons, image files, sound clips, string tables. 
			- .NET paltform supports SATELLITE ASSEMBLIES that contain
			nothing but localized resources
			
	- when using "Add Reference" feature from visual studio, note that the list
	does NOT represent all libraries located in the Global Assembly Cache (GAC) or in the machine.
	Use "Browse" to locate other assemblies.

 Why?
	- 
*/

