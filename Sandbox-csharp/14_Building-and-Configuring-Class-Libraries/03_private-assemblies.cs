﻿/*
What?
	- After adding the reference to the "SampleClassLibrary-CarLibrary", visual studio by default placed
	that assembly as "PRIVATE" under the client application directory (e.g. \bin\debug) all the assemlbies I have made were deployed as 'Private'. A private
	assembly when used, gets copied to the client app using the assembly. This enables the application not
	to consult the system registry when seaching for referenced assembly. So, removing private assemblies does not break other
	apps.

What is the identity of a private assembly?
	- full identity is the friendly name and the numerical version. The friendly name is
	the name of the assembly without the extension. Given the isolated nature of a private assembly, the CLR does
	not bother to use the version number when resolving its location.

Probing process?
	- .net runtime uses the probing process to RESOLVE the location of the private assembly. "Probing is the
	process of mapping an external assembly request to the location of the requested binary file"(first). Strickly
	speaking, a request to load an assembly may be either IMPLICIT or EXPLICIT. An Implicit request is when the
	CLR consults the Manifest to get the location of the assembly. An explicit request is when using "Load()" or
	"LoadFrom()" of the System.Reflection.Assembly. With either request, the CLR extracts the assembly friendly name
	and begins probing the CLIENT's application directory for a file name with that friendly name (e.g. CarLibrary.dll). If not found, then an
	attemp is made to locate an executable assembly (CarLibrary.exe). If neither file exiss, the runtime thows an exception.

Configuring?
	- if you have a managed binary .exe file with its assemblies in the same direectory, it will work. But if you create a subfolter and
place the assemblies to it, it will not work. You need a configuration file. 
		- configuration files MUST have the same name as the launching application and take a *.config
		file extension and they must be deployed in the client's directory
		- the example I use was to take the .exe of this assembly and the "SampleClassLibrary-CarLibrary.dll" and put it on the same direectory
		Then adding a subdirectory to add the .dll assembly. If I add the configuration file such as
	<configuration>
		<runtime>
			<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
				<proving privatePath="MyLibraries"/>
			</assemblyBinding>
		</runtime>
	</configuration>

		- Note that privatePath doesn't accept relative or absoluate paths but contains a name of the subdirectory and
		CLR will look for all subdirectories that matches that name. Multiple directoryu can be assed 
			<proving privatePath="MyLibraries;MyLibraries\Tests"/>
		- Also note that if you need to use a relative or absolute path you can use
		the <codeBase> element
		- Know the CLR will load the very first assembly it finds during the probing process

	- the role of app.config. Since the configuration file needs to be the same name as the launching app, you can create
	a file 'app.config' and then each time I compile my project, visual studio will create a file under "\bin\Debug" with the
	correct name. This only happens if the file is 'app.config'. 

Conclusion
	- visual studio helps with the deployment of private assemblies. User starts by
	adding a reference and then visual studio will copy and place them under \bin\Debug. If a subdirectory exist
	it needs to be configure in the .config file. Private assembleis are those that get copied

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarLibrary;

namespace CSharpCarClient
{
	public class Program
	{
		static void Main(string[] args)
		{
			SportsCar viper = new SportsCar("Viper", 240, 40);
			viper.TurboBoost();

			// make minvan
			MiniVan mv = new MiniVan();
			mv.TurboBoost();

			Console.WriteLine("Done. Press any key to terminate");
			Console.ReadLine();
		}
	}
}


