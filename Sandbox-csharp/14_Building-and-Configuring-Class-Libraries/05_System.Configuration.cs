﻿/*
System.Configuration namespace

What?
	- 
How?
	- the "System.Configuration" namespace proves a small set of tpyes to read custom data from the client's .config file
	- these settings must be inside the <appSettings> element
Why?
	- client-application settings in the .config file

*/
using System.Configuration;
using System;
namespace Configs
{
	class Program
	{
		static void Main()
		{
			AppSettingsReader ar = new AppSettingsReader();
			int n = (int)ar.GetValue("RepeatCount", typeof(int));
			System.Console.WriteLine(n);

			string textColor = (string)ar.GetValue("TextColor", typeof(string));
			Console.WriteLine(textColor);
		}
	}
}