﻿/*
Why?

What?
	- the name diff betweeen a shared and private assembly is that a shared one
	has a single copy of the assembly in a machine to be used for multiple application (e.g. mscorlib.dll)
	- deciding if deploying as private or sharing can be an issue.
	- Global Assembly Cache (GAC) is where shared assemblies are installed.
		- the location of GAB depends on the machine and the installed .NET
		- machines with net 4.0 or lower, it will have its GAC Located in \Windows\assembly. Higher than 4.0, the shared
		assemblies are located on C:\Windows\Microsoft.NET\assembly\GAC_MSIL
			- example
				"C:\Windows\Microsoft.NET\assembly\GAC_64\Microsoft.ConfigCI.Commands.Resources\v4.0_10.0.0.0_en_31bf3856ad364e35\Microsoft.ConfigCI.Commands.Resources.dll"
				
				Microsoft.ConfigCI.Commands.Resource => friendly name 
				v4.0 => .net version
				10.0.0.0 => version
				_en_31312323 => publich assembly token value
	Strong names
		- before deplying it to the GAC, the assembly needs to have a STRONG name to uniquely identify it
		- This is similar to COM globally unique identifiers (GUIDS) but this new way is much more unique
		and resistant to tampering than a single GUID
		- How to build a strnog name
			- friendly name (name of the assemblu without the file extension)
			- version number of the assemblu (assigned using the [AssemblyVersion] attribute)
			- public key value (assigned usign the [AssemblyKeyFile] Attribute)
			- optional culture identity value for locationzation purposes (assiged using the [AssemblyCulture] attribute)
			- embedded digital signature created using a HASH of the assembly's content AND the private key value
				- to do this we need to generate an assembly hash code from the manifest (with publick key), type metadata
				, and CIL and add that with the "Private Key Data" to get "Digital Signature". THe private key is used to 
				create the digital signature. The whole idea of this is to not have idential .net assemblies in the .NET universe.

	Understanding the <codeBase> element
		- instructs the CLR to prove for dependent assemblies for arbitrary liocatiosn (network end, etc.)
		- if the value assigned to the codeBase is on a remote machine, the assembly will be downloaded and placed
		in a specific directory under GAC, termed the 'downloaded cache'
		- <codebase> can be used to probe for assembleis that do not have a strong name (however the assembly's
		location must be relative to the client's application direectory (and thus, is a little more than an
		aliternative to the <privatePath> element

		Example:
			<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
					<dependentAssembly>
						<assemblyIdentity name="Microsoft.VisualStudio.QualityTools.Resource" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" /> (=> describing the target assembly)
						<codeBase version="10.0.0.0" href="file:///C:/Windows/assembly/GAC_MSIL/Microsoft.VisualStudio.QualityTools.Resource/10.0.0.0__b03f5f7f11d50a3a/Microsoft.VisualStudio.QualityTools.Resource.dll" />
							(=> specifies the version and the location)
					</dependentAssembly>
			</assemblyBinding>
How?
	Generate strong names
		- cmd line
		- vs
	
	Installing strongly named assemblies to the GAC
		- ...

	Consuming a shared assembly
		- its not different than consuming a private assembly. The difference is that the assembluy
		will not get copied. 

	Exploring the manifest of a shared assembly
		- when generating a strong name, the entire public key is recorded in the assembly MANIFEST.
		- when a client references a stornglt named assembluy, its manifest RECORDS a condensed hash
		value of the full public key, denoned by the ".publickeytoken" tag.  It you open the manifest of an .exe
		using 'ildasm.exe' you'll see it
	
	Dynamically redirecting to specific versions of shared assembly
		To tell the CLR to load a specific version of a shared assemblu other than the version listed
		in the manifest, i can update the .config file
			<runtime>
				<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
					<dependencyAssembly>
						<assemblyIdentity	name="CarLibrary"			=> friendlyname
											publicKeyToken="2222"		=> you can get it from the manifest of the client file that
																		references the shared assembly. 
											culture="nuetral"/>			=> optional
						<bindingRedirect	oldVersion="1.0.0.0"		=> currently running in the manifest
											newVersion="2.0.0.0"/>		=> version in the GAC to load instead 
					</dependencyAssembly>
				</assemblyBinding>
			</runtime>
		It is possible to set a range in the oldVersion attribute (e.g. "0.0.0.0-3.0.0.1")

	Publicsher Policy Assemblies
		- allows the publisher of a given assembly to ship a binary version of .config that is installed into the GAC
		along with the newest version of the associated assembly. 

Conclusion
	- shared assemblies needs to have a strong name (friendly name, version, public key, digintal signuater)
	- are placed in the GAC
	- client can define what versions to load from the GAC and also the path if is required using the <codeBase>
*/