﻿/**
 * What?
 *	- able to cast types from different hirarchies
*/
using System;
namespace CustomTypeConversions
{
	public class Base { }
	public class Derived : Base { }
	public  struct Rectangle {
		public int Width {get; set;}
		public int Height {get; set;}

		// we cannot declare custom default constructor
		//public Rectangle() { }

		/*
		 * "By calling :this() from your constructo declaration you
		 * let the base ValueType class initialize all the 
		 * backing fields for the automatic properties. We cannot do it
		 * manually on our constructor because we don't have access to the backing field of
		 * an automatic property. ValueType is the base calss of all structs" (Stackoverflow)
		 */
		public Rectangle(int w, int h): this()
		{
			Width = w;
			Height = h;
		}
		public void Draw()
		{
			for (int i = 0; i < Height; i++)
			{
				for (int j = 0; j < Width; j++)
				{
					Console.Write("*");
				}
				Console.WriteLine();
			}
		}
		public override string ToString()
		{
			return string.Format("[Width = {0}; Height = {1}]", Width, Height);
		}
	}

	public struct Square
	{
		public int Length { get; set; }
		public Square(int l)
			: this()
		{
			Length = l;
		}
		public void Draw()
		{
			for (int i = 0; i < Length; i++)
			{
				for (int j = 0; j < Length; j++)
				{
					Console.Write("*");
				}
				Console.WriteLine();
			}
		}
		public override string ToString()
		{
			return string.Format("[Length = {0}]", Length);
		}

		// Rectangles can be explicitly converted into Squares
		public static explicit operator Square(Rectangle r) {
			Square s = new Square();
			s.Length = r.Height;
			return s;
		}
	}
	class P
	{
		static void Main()
		{
			// ===========
			// numerical conversions
			// ===========
			int a = 123;
			long b = a;	// implicit conversions from int to long
			int c = (int)b;		// explicit conversion from long to int, possible lost of data

			// ===========
			// conversions among related class types
			// ===========
			// implicit cast betwen derived to base
			Base myBaseType;
			myBaseType = new Derived();

			// must explicitly base to store base reference
			Derived myDerivedType = (Derived)myBaseType;
			// - this works because they have related hierarchis
		}
	}
}