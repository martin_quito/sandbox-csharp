﻿/*
Region
	What
		- "lets you specify a block of code that you can expand or collapse 
		when using the outlining feature of the Visual Studio Code Editor. 
		In longer code files, it is convenient to be able to collapse or hide one 
		or more regions so that you can focus on the part of the file" 

that you are currently working on. 
	How
		#region
		#endregion
*/