﻿/**
 * What?
 *	- allows you to manipulate the internal collections of subobjects just like a standard array
 *	- is most useful when you are creating custom collection classes (generic or nongeneric)
 *	- indexer methods may be overloaded on a single class or structure
 *		- for example, .NET native database-access API, ... (read more in CHapter 11 in book 1 to learn more)
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace IndexerMethod
{
	public class PersonCollection : IEnumerable
	{
		private ArrayList arPeople = new ArrayList();

		// defining the indexer method (int)
		public Person this[int index]
		{
			get { return (Person)arPeople[index]; }
			set { arPeople.Insert(index, value);}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return arPeople.GetEnumerator();
		}

		public int Count {
			get
			{
				return this.arPeople.Count;
			}
		}
	}

	public class PersonCollection2 : IEnumerable
	{
		private Dictionary<string, Person> listPeople = new Dictionary<string, Person>();

		// defining the indexer method (int)
		public Person this[string name]
		{
			get { return (Person)listPeople[name]; }
			set { listPeople[name] = value; }
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return listPeople.GetEnumerator();
		}

		public int Count
		{
			get
			{
				return this.listPeople.Count;
			}
		}
	}


	public class Person
	{
		public int Age { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public Person() { }
		public Person(string firstName, string lastName, int age)
		{
			Age = age;
			FirstName = firstName;
			LastName = lastName;
		}
		public override string ToString()
		{
			return "Muahaha I override your string";
		}
	}

	public class P
	{
		static void Main()
		{
			// =================
			// indexer method added in a custom collection (nongeneric)
			// =================
			PersonCollection myPeople2 = new PersonCollection();
			myPeople2[0] = new Person("Lisa", "Simpson", 8);
			myPeople2[1] = new Person("Bart", "Simpson", 7);

			for (int i = 0; i < myPeople2.Count; i++)
			{
				Console.WriteLine(myPeople2[i].FirstName);
			}

			foreach (Person p in myPeople2)
			{
				Console.WriteLine(p.FirstName);
			}

			// =================
			// generic types give you this functionatlity (indexer method) out of the box
			// =================
			List<Person> myPeople = new List<Person>();
			myPeople.Add(new Person("Lisa", "Simpson", 8));
			myPeople.Add(new Person("Bart", "Simpson", 7));

			myPeople[0] = new Person("Maggie", "Simpson", 2);

			for (int i = 0; i < myPeople.Count; i++)
			{
				Console.WriteLine("Person number: {0}", i);
				Console.WriteLine("Name: {0} {1}", myPeople[i].FirstName, myPeople[i].LastName);
				Console.WriteLine("Age: {0}", myPeople[i].Age);
			}

			// =================
			// indexing data using string balues
			// =================
			PersonCollection2 m = new PersonCollection2();
			m["Homer"] = new Person("Homer", "Simpson", 8);
			m["Bart"] = new Person("Bart", "Simpson", 7);

			// get homer
			Person home = m["Homer"];
			Console.WriteLine(home.FirstName);
				// => Homer
			// - so again, if you were to use the generic Dictionary<TKEy,TValue> type directly
			// , you'd gain the indexer method functionality out of the box without building
			// a custom, nongeneric class suporrting a string indexer. 

			// - but realize that the data type of any indexer will BE BASED on how the
			// supporting collection type allows the caller to retrieve subitems
		}
	}
}
