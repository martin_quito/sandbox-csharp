﻿/*
 * What?
 *	 - it is possible to overload operators
 *	 
 * How?
 *		+ - ! ~ ++ -- true false			unary operators can be overloaded
 *		+ - * / % & | ^ << >>				binary operators can be overloaded
 *		==  !=  <  >  <=  >=				These comparison operators can be overlaoded
 *		
 *		The rest cannot be overloaded
 *		
 */
using System;
using System.Collections.Generic;
namespace OperatorOverloading
{
	public class Point
	{
		public int X { get; set; }
		public int Y { get; set; }
		public Point(int xPos, int yPos)
		{
			X = xPos;
			Y = yPos;
		}

		// - to equip a custom type to respond uniquely to intrinsic operators
		// , C# provides the 'operator' keyword
		// - must be static
		public static Point operator + (Point p1, Point p2)
		{
			return new Point(p1.X + p2.X, p1.Y + p2.Y);
		}
		public static Point operator -(Point p1, Point p2)
		{
			return new Point(p1.X - p2.X, p1.Y - p2.Y);
		}

		public override string ToString()
		{
 			 return string.Format("[{0}, {1}]", this.X, this.Y);
		}
	}

	class P
	{
		static void Main()
		{
			// =============
			// + operator on intrinsic types
			// =============
			int a = 100;
			int b = 240;
			int c = a + b;

			// =============
			// on refernce types
			// =============
			string s1 = "Hello";
			string s2 = " world!";
			string s3 = s1 + s2;
			Console.WriteLine(s3);
				// when + is applied to number, it  sums the integers
				// when - is applied to strings, the result is string concatenation

			// =============
			// overloading binary operators +-
			// =============
			Point ptOne = new Point(100, 100);
			Point ptTwo = new Point(40, 40);
			Console.WriteLine("ptOne = {0}", ptOne);
			Console.WriteLine("ptTwo = {0}", ptTwo);

			Point p3 = ptOne + ptTwo;
				// its like
				// Point p3 = Point.operator+ (p1, p2)
			Console.WriteLine("Added: {0}", p3);

		}
	}
}

