﻿/*
 * What?
 *  - a custom data type on the fly which supports barebones encapsulations via properties
 *  and acts according to value-based semantics
 *	- use it when you need to, otherwise, use strongly type classes/structures 
 * Why?
 *  - create custom data type on the fly
 *  
 * When?
 *	- typically only used when using LINQ 
 * 
 * Some limitations?
 *	- you can't control the name
 *	- always extend System.Object
 *	- fields and properties of anonymous type are always read-only
 *	- cannot support events, custom methods, custom operators, or custom overrides
 *	- always implicitly seleade
 *	- always created using the default constructor
 * 
 * How?
 *  - two anonymous types will have the same  hash (from GetHashCode) if (and only if) they
 *  have the same set of proeprties that have been assigned the same values.
 *  - toString() and GetHashCode() are fairly straight forward. 
 *  - if you compare two anon types using .Equals, it results to true because it uses the 
 *  value-based semantics when testing for equality (e.g. checking the value of each field of the objects being compared)
 *  - if you use the operator "==" to compare equal anon types (same props and values), it results to false. This is
 *  because the anon types do NOT RECEIVE OVERLOADED versions of the C# quality operators (== and !=). So, it tests
 *  for the references and not the values maintained by the objects are being tested for equality. 
 *  - note that if you declare two anon types with same value-based semantic members, the compiler will only
 *  generare a single anonymous type definition. 
 *  - you can have nested anonymous types
 */
using System;
using System.Collections.Generic;

namespace AnyTypes {
    class P {
		// ===============
		// 
		// ===============
        static void BuildAnonType(string name, string color, int currSp) {
            // - Build anon type using incoming args (this uses the object initialization syntax)
			// - compiler will automatically generate a new class definition at compile time
			// - the object initialization syntax is there to tell the compiler to create
			// private backing fields and (read-only) properties for the newly created type
            var car = new { Make = name, Color = color, Speed = currSp };

            // note you can now use this type to get the property data!
            Console.WriteLine("You have a {0} {1} going {2} MPH", car.Color,car.Make, car.Speed);

            // anon types have custom implementations of each virtual method of system.obecjt
            Console.WriteLine("Tostring() == {0}", car.ToString());
        }
    }
}
