﻿/*
 * What?
 *  - able to modify types without subclassing and without modifying the type directly
 *  - importing extension methods, the scope is withing a namespace that define them or the namespace
 *  that import them.
 *  
 * Why?
 *  - add methods to sealed types awithout modifying the type direcetly to prevnt risking
 *  backward compatibility
 *  - not modify a class in adding methods to prevent risking backward compatibility; and
 *  if I decide to extend the class, then I would have more than one class to maintain, which is not
 *  what we want. 
 *  - this is good when you want to extend functinality of a type but do not want to subclass (or not subclass if the type is sealed)
 *  for the purpose of polymorphism
 *  
 * How?
 *  - must be defined within a statc class
 *  - all extensions are marked as such by using the 'this' keyword as a modifier on the first
 *  on the first (and only the first) parameter of the method in question The 'this qualified'
 *  parameter represents the item being extended
*/

using System;
using System.Data;
using System.Reflection;
using ExtensionMethod;
namespace ExtensionMethod {

    static class MyExtensions {
        // this method allows any object to display the assembly it is defined in
        public static void DispplayDefiningAssembly(this object obj) {
            System.Console.WriteLine("{0} lives here: => {1}\n", obj.GetType().Name, Assembly.GetAssembly(obj.GetType()).GetName().Name);
        }
        // this method allwos any integer to reverse its digits.
        // for example, 56 would return 65
        public static int ReverseDigits(this int i) {
            // Translate int into a string, and then get all characters
            char[] digits = i.ToString().ToCharArray();

            // now revers items in the array
            Array.Reverse(digits);

            // put back into string
            string newDigits = new string(digits);

            // Finally return the modified string back as an int
            return int.Parse(newDigits);
        }

        // - extending types implement specific interfaces
        // - add a new method to any type that implements IEnumerable
        public static void PrintDataAndBeep(this System.Collections.IEnumerable iterator) {
            foreach (var item in iterator) {
                Console.WriteLine(item);
                Console.Beep();
            }
        }
    }

    class P {
        static void Main() {
            // invoking extension methods
            int myInt = 12345;
            myInt.DispplayDefiningAssembly();
            Console.WriteLine(myInt.ReverseDigits());

            // so has the dataset
            DataSet d = new DataSet();
            d.DispplayDefiningAssembly();

            //
            string[] data = { "wow", "this", "is" };
            data.PrintDataAndBeep();
        }
    }
}


namespace Test {
    class P {
        static void Main() {
            int myInt = 23;
            Console.WriteLine(myInt.ReverseDigits());
        }
    }
}