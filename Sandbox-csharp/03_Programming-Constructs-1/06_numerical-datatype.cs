﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _06_numerical_datatype
	{
		static void Main()
		{
			Console.WriteLine("=> Data type Functionality");
			Console.WriteLine("Max of int: {0}", int.MaxValue);
			Console.WriteLine("Max of int: {0}", int.MinValue);

			Console.WriteLine("Max of double: {0}", double.MaxValue);
			Console.WriteLine("Min of double: {0}", double.MinValue);

			Console.WriteLine("double.Epsilon: {0}", double.Epsilon);
			Console.WriteLine("double.PositiveInfinity: {0}", double.PositiveInfinity);
			Console.WriteLine("double.NegativeInfinity: {0}", double.NegativeInfinity);
			Console.WriteLine();
		}
	}
}
