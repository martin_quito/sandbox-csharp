﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _05_variable_declaration
	{
		static void Main()
		{
			Console.WriteLine("=> Data declaration");
			// declare a local variable
			int myInt;
			string myString;

			// declared and initalize
			int myInt2;
			myInt2 = 2;

			// declared and initialized in the same line
			int myInt3 = 100;

			// declare multiple variables of the same underlying type on a single line of code
			bool b1 = true, b2 = false, b3 = true;

			// using their data type name
			System.String name = "Martin";

			// - all intrinsic data types support the default constructor where
			// you create a variable using the 'new' keyword and automatically sets a default value
			bool b7 = new bool();
			int i7 = new int();
			double d7 = new double();
			DateTime dt = new DateTime();
				/* The default values of some instrinsic data types
				 * bool		=> false
				 * numeric data => 0 (0.0 if is a floating point)
				 * char		=> single empty character
				 * BigInteger	=> 0
				 * DateTime	=> 1/1/0001 12:00:00 Am
				 * object references (including strings) are set to null
				 */
					
			// - remmeber that a C# keyword points to a data type such as System.Boolean
			// which is derived from System.ValueType which it derives from System.Object
			Console.WriteLine();
		}
	}
}
