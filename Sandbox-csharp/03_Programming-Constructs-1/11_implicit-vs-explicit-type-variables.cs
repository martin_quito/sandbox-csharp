﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _11_implicit_vs_explicit_type_variables
	{
		static void Main()
		{
			// ================
			// var
			// ================
			// - when var is used, the compiler will infer the value
			// based on the initial value
			int myInt1 = 0;
			var myInt2 = 10;
				// - var is not a C# keyword which it means it is okay
				// to declare variables, parameters, and fields with name "var"
				// without compile errors. However, when its used as a data type
				// it is treated as a keywrod by the compiler

			// ============== 
			// limitations
			// ==============
			/*
			 * - implicity tpying only applies to local variables in a method or property scope
			 * but it cannot be used  to define return values, parameters, or field data of a custom type
			 * - the following should not work
					class This WillNeverCompile
					{
						// Error! var cannot be used as field data!
						private var myInt = 10;

						// Error! var cannot be used as a return value
						// or parameter type!
						public var MyMethod(var x, var y) {}
					}
			 * 
			 * - more limitations would be
			 *		// Error! Must assign a value!
			 *		var myData;
			 *		
			 *		// Error! Must assign value at exact time of declaration!
			 *		var myInt;
			 *		myInt = 0;
			 *		
			 *		// Error! Can't assign null as initial value!
			 *		var myObj = null;
			 *		
			 * 
			 */
			
			// - It is okay to assign an inferred local variable to null after its inital
			// assignment 
			var myCar = new Object();
			myCar = null;

			// or
			var myInt = 0;
			var anotherInt = myInt;

			string myString = "Wake up!";
			var myData = myString;

			// - also its okay to return an implicitly typed local variable to the caller, provided 
			// the method return type is the same underlying type as the var-defined data point
			/*
			 * static int GetAnINt() {
			 *	var retVal = 9;
			 *	return retVal;
			 * }
			 */

			// also remember that implicit data type is strongly typed data 

			// ==============
			// why would we use var
			// ==============
			// - using var just for the sake of doing it, doesn't bring a lot to the table
			// but its useful when using LINQ because its hard to know the returned type or maybe
			// impossible

			int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
			// linq query
			System.Collections.Generic.IEnumerable<int> subset = from i in numbers where i < 10 select i;
			Console.WriteLine("Values in subset: ");
			foreach (var i in subset)
			{
				Console.WriteLine("{0} ", i);
			}
			Console.WriteLine("subset is a: {0}", subset.GetType().Name);
			Console.WriteLine("subset is defined in: {0}", subset.GetType().Namespace);

			// - when using LINQ,you seldom care abut the underlying type of the query's rturn value, you
			// will simply assign the value to an implicitly typed local variable
			// - it could be even argued that the only time to use var is when defining data returned from a LINQ query
			
		}
	}
}
