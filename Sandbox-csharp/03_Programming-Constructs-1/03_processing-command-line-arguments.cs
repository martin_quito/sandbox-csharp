﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _03_Processing_command_line_arguments
	{
		static int Main(string[] args)
		{
			// Process any incoming args
			for (int i = 0; i < args.Length; i++)
			{
				Console.WriteLine("ARg: {0}", args[i]);
			}
			Console.ReadLine();
			return -1;
		}
	}
}
