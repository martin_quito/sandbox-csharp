﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _10_data_conversion
	{
		static void Main()
		{
			// ====================
			// widening (upward cast)
			// ====================
			short num1 = 100;
			int num2 = num1;
			// this implicitly widens from short to int

			// ====================
			// narrowing
			// ====================
			int num3 = 300000;
			//short num4 = num3; // cannot implicitly convert type 'int' to 'short' ...

			// all narrowing conversions result in a compile error
			int num5 = 10;
			//short num6 = num5;	// Error ...
				// - see what i mean ...
				// solution is to explicitly cast it

			// cast it!
			int num8 = 10;
			short num9 = (short) num8;

			// but you could lose data
			int a1 = 60000;
			short a2 = (short) a1;
			Console.WriteLine(a2);
				// -5536

			// use the checked keyword or unchecked keyword to ensure data loss does not escape undetected
		}
	}
}
