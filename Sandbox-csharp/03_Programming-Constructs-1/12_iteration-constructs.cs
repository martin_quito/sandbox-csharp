﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _12_iteration_constructs
	{
		static void Main()
		{
			// ===================
			// for
			// ===================
			for (int i = 0; i < 4; i++)
			{
				Console.WriteLine("Number is {0}", i);
			}

			// ===================
			// foreach
			// ===================
			// - will iterate any object that implements the 'IEnumerable' interface
			string[] carTypes = { "Ford", "BMW", "Yugo", "Honda" };
			foreach (string c in carTypes)
			{
				Console.WriteLine(c);
			}

			// ===================
			// usie of implicit typing withing foreaech
			// ===================
			string[] carTypes2 = { "Ford", "BMW", "Yugo", "Honda" };
			foreach (var c in carTypes2)
			{
				Console.WriteLine(c);
			}

			// ===================
			// while
			// ===================
			string userIsDone = "";
			while (userIsDone.ToLower() != "yes")
			{
				Console.WriteLine("In while loop");
				Console.WriteLine("Are you done? [yes] [no]: ");
				userIsDone = Console.ReadLine();
			}

			// ===================
			// do
			// ===================
			string userIsDone2 = "";
			do {
				Console.WriteLine("In while loop");
				Console.WriteLine("Are you done? [yes] [no]: ");
				userIsDone2 = Console.ReadLine();
			} while (userIsDone2.ToLower() != "yes");


		}
	}
}
