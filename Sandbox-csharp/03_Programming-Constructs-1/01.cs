﻿/**
 * What is about C#?
 *	- C# demands that all program logic must be contained withing a type definition
 *	- C# is case-sensitive 
 *	- It is not possible to greate a global function or global points of data rather all data members and data methods
 *	must be within a type definition
 *	- all C# keywords are lowercase
 *	- namespaces, types, and member names being (by convention) with an initial capital letter and have capitalized
 *	the first letter of any embedded words (e.g. Console.WriteLine) If you get a "undefined symbols" it means to 
 *	check your spelling
 *	
 * What should all executable C# application have in common?
 * - it requires to have the "Main" method which is used to signify the entry point of the application
 * 
 * What is the Main() method?
 *	- the class that has it is called the application object
 *	- it signifies the entry point of the app
 *	- it is possible to have multiple application objects but the compiler needs to know which one to use
 *	- it has different signatures and the one to choose is based on two questions
 *		1 - do you want to return a value to the system when Main() has completed and your program terminates
 *		2 - do yo u need to process any user-supplied command line parameters?
 *	- it must return a value. even if you choose to return 'void' it will return 0. This convention is from the C family
 *		- 0 = good
 *		- -1 = error
 *		- in a windows OS, the application's return value is stored within the system environmental variable %ERRORLEVEL%
 * What is a type?
 *	- it is a "general term referring to a member of the set {cclass, interface, structure, enumeration, delegate}
 *	
 * System Data types and corresponding C# keywords?
 *	- C# data type keywords are actually shorthand notations for full-blown types in the System namespace
 *	- these guys are intrinsic C# data types
 * 
 *	bool		=> System.Boolean	true or false
 *	byte		=> System.Byte      unsigned 8-bit number (0 to 255)
 *	short       => System.Int16		signed 16-bit number (-32,768 to 32,767)
 *	int         => System.Int32		signed 32-bit number (2,147,483, 648 to -2,147,483,647)
 *	long        => System.Int64		signed 64-bit number 
 *	char        => System.Char		single 16-bit unicode character
 *	float       => System.Single	32-bit floating point number
 *	double      => System.Double	64 bit floating point number
 *	decimal     => System.Decimal	128 bit signed number
 *	string      => System.string	represents a set of unicode character
 *	Object      => System.Object	the base of all types in the .NET universe
 *	
 * 
*/