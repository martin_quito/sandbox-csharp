﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _08_char_datatype
	{
		static void Main()
		{
			char MyChar = 'a';
			Console.WriteLine("Char.IsDigit('a'): {0}", char.IsDigit(MyChar));
			Console.WriteLine("Char.IsLetter('a'): {0}", char.IsLetter(MyChar));
			Console.WriteLine("Char.IsWhiteSpace('Hello There', 5): {0}", char.IsWhiteSpace("Hello There", 5));
			Console.WriteLine("Char.IsWhiteSpace('Hello There', 6): {0}", char.IsWhiteSpace("Hello There", 6));
			Console.WriteLine("char.IsPunctuation('?'): {0}", char.IsPunctuation('?'));
		}
	}
}
