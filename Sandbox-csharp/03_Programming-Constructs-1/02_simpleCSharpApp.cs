﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	// this is formally called the application object (the class the defines the Main() method
	class Program
	{
		
		static void Main(string[] args)
		{
			// Display a simple message to the user.
			Console.WriteLine("*** My First C# App ***");
			Console.WriteLine("Hello World!");
			Console.WriteLine();

			// Wait for Enter key to be pressed before shuttting down
			Console.ReadLine();
		}
	}
}
