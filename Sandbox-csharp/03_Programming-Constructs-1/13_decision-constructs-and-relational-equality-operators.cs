﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _13_decision_constructs_and_relational_equality_operators
	{
		static void Main()
		{
			// =============
			// if /ese
			// =============
			// it only works in Boolean expressions. Not -1 or 0

			// =============
			// equality
			// =============
			/*
			 * equality/relation operator
			 *	==		 
			 *	!=
			 *	<
			 *	>
			 *	<=
			 *	>=
			 */

			// =============
			// Conditional operators
			// =============
			// - the && and || are short circuit which means that after a complex expression
			// has been determined to be false, the reaiming subexpressions will not be checked
			/*
			 * Operator
			 *	&&
			 *	||
			 *	!
			 */

			// =============
			// switch construct
			// =============
			Console.WriteLine("Please pick your language preference: ");
			string langChoice = Console.ReadLine();
			int n = int.Parse(langChoice);
			switch (n)
			{
				case 1:
					Console.WriteLine("Good choice, C# is a fine language.");
					break;
				case 2:
					Console.WriteLine("VB: OOP, multithreading, and more!");
					break;
				default:
					Console.WriteLine("Well..good luck with that!");
					break;
			}
				// - C# demands that each 'case' including 'default' contains 
				// executable statements have a terminating break or goto to avoid fall-through

			// in this case we will use 'strings' instead of numeric values
			Console.WriteLine("Please pick your language preference: ");
			string langChoice2 = Console.ReadLine();
			switch (langChoice2)
			{
				case "C#":
					Console.WriteLine("Good choice, C# is a fine language.");
					break;
				case "VB":
					Console.WriteLine("VB: OOP, multithreading, and more!");
					break;
				default:
					Console.WriteLine("Well..good luck with that!");
					break;
			}

			// it is possible to switch on an enumeration data type
			Console.WriteLine("Enter your favorite day of the week");
			DayOfWeek favDay;
			Console.WriteLine(typeof(DayOfWeek));
			try
			{
				favDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Console.ReadLine());
			}
			catch (Exception)
			{
				Console.WriteLine("Bad input");
				return;
			}
			switch (favDay)
			{
				case DayOfWeek.Friday:
					Console.WriteLine("Yes, Friday rules!");
					break;
				case DayOfWeek.Monday:
					Console.WriteLine("Another day, another dollar");
					break;
				case DayOfWeek.Saturday:
					Console.WriteLine("Great day indeed");
					break;
				case DayOfWeek.Sunday:
					Console.WriteLine("Church");
					break;
				case DayOfWeek.Thursday:
					Console.WriteLine("Almost Friday ...");
					break;
				case DayOfWeek.Tuesday:
					Console.WriteLine("At least it is not Monday");
					break;
				case DayOfWeek.Wednesday:
					Console.WriteLine("A fine day");
					break;
			}
		}
	}
}
