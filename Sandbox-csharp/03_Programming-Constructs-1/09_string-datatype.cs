﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._03_Programming_Constructs_1
{
	class _09_string_datatype
	{
		static void Main()
		{
			// ================================
			// parsing values from string data
			// ================================
			bool b = bool.Parse("True");
			Console.WriteLine(b);

			double d = double.Parse("99.884");
			Console.WriteLine(d);

			int i = int.Parse("8");
			Console.WriteLine(i);

			char c = char.Parse("w");
			Console.WriteLine(c);

			// ================================
			// methods
			// ================================
			Console.WriteLine("methods =================== ");
			string name = "Martin";

			// get length of current string
			Console.WriteLine(name.Length);	 // 6

			// determines whether a string contains a specific substring
			Console.WriteLine(name.Contains("Mar")); // true

			// replace
			Console.WriteLine(name.Replace("in", "LLLL"));	// MartLLLL
				// this creates a new string because of immutable nature of strings

			// split
			Console.WriteLine("Martin Quito".Split(' ')); // System.String[]


			// ================================
			// String concatentation
			// ================================
			Console.WriteLine("String Concatenation =================== ");
			string s1 = "Programming the ";
			string s2 = "PsychoDrill (PTP)";
			string s3 = s1 + s2;
			Console.WriteLine(s3);
				// => Programming the PsychoDrill (PTP)
				// the "+" when compiled, emits a call to the static "String.Concat()"

			// ================================
			//	Defining Verbatim strings
			// ================================
			// - using the symbol @ on a string literal, it creates what is termed as a verbatim string
			// which it disables the processing of literal's escape characters and print out a string as is.
			Console.WriteLine("Verbatism =====================");
			Console.WriteLine(@"C:\MyApp\bin\Debug");
			Console.WriteLine("C:\\MyApp\\bin\\Debug");

			// ================================
			// comparing strings
			// ================================
			Console.WriteLine("Comparing strings");
			string s4 = "Hello!";
			string s5 = "Yo!";
			Console.WriteLine("s4 = {0}", s4);
			Console.WriteLine("s5 = {0}", s5);

			Console.WriteLine("s4 == s5: {0}", s4 == s5);			// False
			Console.WriteLine("s4 == Hello!: {0}", s4 == "Hello!");	// True
			Console.WriteLine("s4 == HELLO!: {0}", s4 == "HELLO!"); // False
			Console.WriteLine("s4 === hello!: {0}", s4 == "hello!");// False
			Console.WriteLine("s4.Equals(s5): {0}", s4.Equals(s5));	// False
			Console.WriteLine("Yo.Equals(s5): {0}", "Yo!".Equals(s5));	// True
			// - Even though strings are referenced types, the == or != operators
			// have been refined to compare the values of string objects and not the 
			// the object in memory to which they refer.
			// - the == is case sensitive

			// ================================
			// Strings are Immutable
			// ================================	
			Console.WriteLine("String are Immutable =================== ");
			// set initial string value
			string a = "This is my String.";
			Console.WriteLine("a = {0}", a);						// This is my string

			// Uppercase s1?
			string upperString = a.ToUpper();
			Console.WriteLine("upperString = {0}", upperString);	// THIS IS MY STRING

			Console.WriteLine("a = {0}", a);						// This is my string

			// - System.String is used for small sized and simple manipulation 
			// because if we use it in large scale, it will result in bloated code and a lot of
			// unnecessary copies. So we use the "System.Text.StringBuilder" type

			// ================================	
			// System.Text.String Buidler Type
			// ================================				
			Console.WriteLine("String Builder ============== ");
			StringBuilder sb = new StringBuilder("*** Fantastic Games ***");
			sb.Append("\n");
			sb.Append("Half Life");
			sb.Append("Morrowwind");
			sb.Append("Deus Ex" + "2");
			sb.Append("System Shock");
			Console.WriteLine(sb.ToString());	// Half LifeMorrowwindDeus Ex2System Shock

			// - the appending is going to a buffer which can be used to manipulate the data
			// - also the initial length of value it can hold is 16 but it can be changed
		}
	}
}
