﻿/*
 * What?
 *	- if you want the best of the finalization and Disposable, then you can try this method
 *	where if the object user does remember to call Dispose, I can tell the GC to bypass
 *	the finalization process. If the object user forgets to call Dispose(), the
 *	object will eventually be finalized and have a change to free up
 *	the internal resources. 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FD
{
	// =============
	// try 1
	// =============
	public class MyResourceWrapper : IDisposable
	{
		// the garbage collector will call this method if
		// the object user fogets to call Dispose
		~MyResourceWrapper()
		{
			// clean up internal unmanaged
			// Do ** not ** call Dispose() on any managed objects
		}

		// The object user will call this method to clean up resource ASAP
		public void Dispose()
		{
			// - Clean up unmanaged resources here
			// - call Dispose() on other contained disposable ojects

			// - no need to finalize if  user called Dipose(),
			// so suppress finalization
			GC.SuppressFinalize(this);

		}
	}

	// =============
	// try 2
	// =============
	// the problem with try 1 is that there are two locations
	// to clean unmanaged resources, duplicate code, also 
	// the finalize method is not safeguard to attemp to dispose
	// other managed objects. Lastly, I would like to ensure
	// that calling Dispose multiple times will process
	// without an error!

	// the following is a pattern suggested by Microsoft
	class MyResourceWrapper2 : IDisposable
	{
		// used to determine if Dispose() has already been called.
		private bool disposed = false;
		public void Dispose()
		{
			// - call our helper method
			// - specifiying 'true' signifies that
			// the object user triggered the clean up
			CleanUp(true);

			// now suppress finalization
			GC.SuppressFinalize(this);
		}
		private void CleanUp(bool disposing) {
			// be sure we have not already been disposed!
			if (!this.disposed) {
				// if disposing equals true, dispose all
				// managed resources
				if (disposing)
				{
					// Dispose managed resources
				}
				// Clean up unmanaged resources here
			}
			disposed = true;
		}

		~MyResourceWrapper2()
		{
			// Call our helper method 
			// Specifying 'false' signfies that the GC triggered the clean up.
			CleanUp(false);
		}
	}

	class P
	{
		static void Main()
		{

		}
	}
}
