﻿/*
 * Difference betwen class, reference, and object?
 *	- class is a blueprint to build objects
 *	- the 'new' keyword returns a reference to the recent created object that is ON THE HEAP
 *	- manage heap contsin the created objects
 *	- if I declare a reference variable as a local variable in a method spcoe, it is stored (THE REFERENCE!)
 *	on the stack for further use in your app
 *	
 * Basics of object lifetime?
 *	- in .NET, CLR is in charge of the garbage collection. CLR 
 *	will take care of the managed heap in your direct intervation. The rule is
 *	that when you allocate a class instance onto the manage heap using the 'new' keyword
 *	, you can forget about it. This is because CLR will manage it.
 *	How does the garbage collection determine when an object is no longer
 *	needed? The short (incomplete) answer is that the GC removes an object
 *	from the heap ONLY if it is unreachable by any part of your code base. 
 *	CLR performs garbage collection in a time lapse. Also note that a programing
 *	in a garbabe-collected environment greatly simplifies your application dev. Unlike C++,
 *	where you need to manually delete heap-allocated objects, otherwise, memory leaks happen.
 *	
 * CIL of new?
 * - when C# compliles 'new' it emits a CLI 'newobj' instruction which does
 *		- calculate the total amount of memory required for the object to be allocated 
 *		- examine the manage heap to ensure that there is indeed enough room to host the object allocated
 *		If there is, the specified constructor is called and the caller is ultimately returned a reference
 *		to the new object in memory, whose address just happens to be identical to the last position on the
 *		next object pointer
 *		- finally before returning the reference to the caller, advance the 'next object pointer'
 *		to point to the next avaiable slot on the manage heap
 *	- the 'next object pointer' is what identifies exactly where the next object will be located
 *	
 * What happens when you set an reference to null?
 *	- in C#, it only explicitly cuts the connection between the reference and the object 
 *	it previously pointed to. IT DOES NOT FIRE THE GC at the exact momemtn AND  remove
 *	the object from the heap
 *	
 * How does the GC determine when an object is no longer needed?
 *	- 'application root' is  a storage location containing a reference to an object on the manage heap
 *	- the way it works is when GC starts, it will investigate objects on the 
 *	managed heap whether they are still reachable (ie.g rooted) by the application. For more details,
 *	go to book 1 (chapter 13). After objects have been marked for termination, they are swept from memory.
 *	At this point, the reamining space on the heap is compacted, which in turn
 *	causes the CLR to modify the set of active application roots (and the underlying pointers) to refer
 *	to the correct memory location (this is done automatically and transparently).
 *	
 * Understanding objcet generations
 * - each object belongs to a generation. Generations are used for the CLR to efficientlyo locate objects
 * - Generation 0 - newly allocated objects that has never been marked for collection
 * - Generation 1 - an object that has survived a garbage collection 
 * - GEneration 2 - object that has survived more than one sweep of the garbage collection
 * - Gen 0 and 1 are trmed 'ephemeral generations' The GC treats these the ephemeral gen differently
 * 
 * How to force Garbage collector?
 *	- use the System.GC type to interact with GC
 *	
 * 
 * When to force CG?
 *	- application is about to enter into a block of code that you don't want
 *	to interrupted by a possible garbage collection
 *	- application just finished allocating an extremely large numbers of objects
 *	and you want to remove as much of teh acquired memory as soon as possible
 *	
 * 
 * What are finalizable objects?
 *	- when you override the Finalize() for your cusstom classes, you establish
 *	a specific location to perform any necessary cleanup logic for your type. You cannot
 *	call that method but rather, the garbage collection will call the object's Finalize() object
 *	(if supported) before REMOVING THE OBJECT FROM MEMORY
 *	
 *	- ALWAYS REMEMBER THAT 'The only compelling reason to override Finalize() is if your 
 *	C# class is making use of unmanged resources via PInvoke or complex COM interoperability tasks
 *	(typically via various members defined by the System.Runtime.InteropServices.Marshal type)
 *	The reason is that under snecarios, you are manipulating that the CLR cannot manage" (book 1)
 *	
 *	Or in other words "that the role of the Finalize() method is to ensure that a .NET object
 *	can clean up unmanaged resources when it is garbage-collected" (book1)
 *	
 *  Or, "The only time you would need to design a class that can clean up after itself is when
 *  you are using unmanged resources (such as RAW OS file handles, raw unmanaged database
 *  connections, chunks of unmanaged memory, or other unmanaged resources)" (book 1)
 *  
 * How to mark an object to be be finalizated?
 *	- read chpater 13 in book 1
 *	
 * 
 * What are disposable objects?
 *	- a type that implements the IDisposable interface
 *	
 *	- "Structures and class types can both implement IDisposable (unlike overriding Finalize(), 
 *	which is reserved for class types), as the object user (not the garbage collector) invoke
 *	the Dispose() method" (book1)
 *	
 *	- finalizers can be used to release unmanged resources when the GC kicks in, but
 *	you could also release those unmanged resouces as soon as possible instead
 *	of relying on a gargabe collection. 
 *	
 *	- how it works is that when your type implements IDisposable interface, 
 *	the assumption is ath then the object user (my type) is finished using the object (unmanaged resource),
 *	the object user MANUALLY calls DISPOSE() before allowing the object reference to drop out of scope.
 *	- the rule is 
 *		"It is a good idea to call Dispose() on any object you directly create if the object
 *		supports IDisposable. The assumption you should make is that if the class designer choose
 *		to support the Dispose() method, the type has some cleanup to perform. If you forget,
 *		memory will eventually be cleaned up (so don't panic), but it could take longer
 *		than neccessary"
 *	
 *	- also watch out when class has different method names but with the same purpose such as
 *	the System.IO.FileStream which has 'Dispose' or 'close' which both do the same thing
*/

using System;
namespace LifeTime
{
	// ===============
	// Disposable
	// ===============
	class M : IDisposable
	{
		public void Dispose()
		{
			// clean up unmanaged resources
			// dispose other contained disposable objects...
			// just for a test
			Console.WriteLine("In Dispose");
		}
	}
	class P
	{
		static void Main()
		{
			M r = new M();
			r.Dispose();
				// always remember the rule!

			// ==========
			// 'using' keyword
			// ==========
			// - when handling managed object that implements the 'IDisposable'
			// it is quite common to make use of structure exception-handling to 
			// ensure the type''s Dispose() method is called in the event of a
			// runtime exception
			M r2 = new M();
			try
			{
				// use 'r2'
			}
			finally
			{
				r2.Dispose();
			}
			// while this is okay, developers are not thrilled in wrapping
			// every disposable type withing a try/fainlly block just to ensure
			// the Dispose() method is called. Solution is special syntax
			// 'using'
			// Some rules for this notation
			// - Dispose() is called automatically when the 'using' scope exists
			// - if you attempt to use an object that does not implement IDisposable, you will get a compile error
			// Another thing to know is that CIL code, the 'using' syntax does indeed expand to a
			// try/finally logic
			using (M r3 = new M())
			{
				// use of r3
			} // it calls 'Dispose' after the scope exists

			// you can declare multiple objects OF THE SAME TYPE withing a 'using' scope
			using (M r4 = new M(), r5 = new M()) {
				// it worked!
			}
		}
	}
}