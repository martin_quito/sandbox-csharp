﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lazy_ins
{
	public class Song
	{
		public string Artiest { get; set; }
		public string TrackName { get; set; }
		public double TrackLength { get; set; }

		// represents all songs on a player
		public class AllTracks
		{
			private Song[] allSongs = new Song[10000];
			public AllTracks()
			{
				// assume we will up the array of Song objects here.
				Console.WriteLine("Filling up the songs!");
			}
		}
		// the media player has-an Alltrack object
		public class MediaPlayer
		{
			// Assume these methods do something useful
			public void Play() { /* play a song */}
			public void Pause() { /* Pause a song */}
			public void Stop() { /* Stop playback */}

			public MediaPlayer() {
				Console.WriteLine("Default Constructor");
			}

			//private AllTracks allSongs = new AllTracks(); // BAD
			private Lazy<AllTracks> allSongs = new Lazy<AllTracks>(); // (LAZY)
				// this calls the default constructor

			// doing some extra work
			private Lazy<AllTracks> allSongs2 = new Lazy<AllTracks>(() =>
			{
				Console.WriteLine("Creating all tracks (2) object!");
				return new AllTracks();
			});

			public AllTracks GetAllTracks()
			{
				// return all of the songs (BAD)
				// return allSongs; (LAZY)
				return allSongs.Value;
			}

			public AllTracks GetAllTracks2()
			{
				return allSongs2.Value;
			}
		}
	}

	class P
	{
		static void Main()
		{
			// =================
			// example of problem
			// =================
			// This caller does not care about getting all songs,
			// but indirectly created 10, 000 objects
			Song.MediaPlayer p = new Song.MediaPlayer();
			p.Play();
				// => when p.Play() is just ran, "Filling up the songs" is displayed,
				// but with lazy, then you will need to call GetAllTracks()	which
				// then the allSongs gets really used.

			Song.MediaPlayer p2 = new Song.MediaPlayer();
			p2.GetAllTracks2();
				/* =>
				 * Creating all tracks (2) object!
				 * Filling up the songs!
				 */

			// - so, the implementation of MediaPLayer is assuming that the object user
			// will want to obtain a list of songs via GetAllTracks, what if that doesn't
			// happened. The 'AllTracks' member variable in the MediaPlayer will still be allocated,
			// thereby creating 10,000 song objects in memory. Clarly, i would rather not
			// create 10,000 objects that nobody will use which adds a good deal of streess to the GC. 
			// The solution is to uze the 'Lazy<>' generic type. This class allows you
			// to define data that will NOT be created UNLESS YOUR CODE BASE
			// actually makes use of it

			// - lazy object instantiation is not used to decrease allocation of unnecessary objects
			// but can be used when a given member has expensive code creation such as remote method like
			// connection a relation db, etc.

			// - also know that 
			//	private Lazy<AllTracks> allSongs = new Lazy<AllTracks>();
			// calls the default constructor, what if I want to call another constructor
			// or what if I want to do some extra work when Lazy<> varaible is made. You can pass
			// a delegate (see above)
		}
	}
}
