﻿/*
 * What is an array?
 *  - a collection of data points of the same type (an array of ints)
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _05_arrays {
        static void Main() {
            // ==============
            // simple array
            // ==============
            // create an array of ints containing 3 elements indexed, 0, 1, 2
            int[] myInts = new int[3];

            // fill in
            myInts[0] = 100;
            myInts[1] = 200;
            myInts[2] = 300;

            foreach (int i in myInts) {
                Console.WriteLine(i);
            }

            // what happens if you declare an array but not fill it?
            // - the elements will get the default value, so for bool is false and ints are zeros

            // create a 100 item string array, indexed 0 - 99
            string[] booksOnDoNot = new string[100];

            // ==============
            // array initialization syntax (to fill the items in an array)
            // ==============
            // Array initialization syntax using the new keyword
            string[] stringArray = new string[] { "one", "two", "three" };
            Console.WriteLine("stringArray has {0} elements", stringArray.Length);

            // Array initialization syntax without using the new keyword (array initailizae expression only for arrays)
            bool[] boolArray = { false, false, true };
            Console.WriteLine("boolArray has {0} elements", boolArray.Length);

            // ARray initialization with new keyword and size
            int[] intArray = new int[3] { 22, 23, 0 };
            Console.WriteLine("intArray has {0} elements", intArray.Length);

            // ==============
            // implicitly typed local arrays
            // ==============
            var a = new[] { 1, 10, 100, 100 };
            var b = new[] { 1, 1.5, 2, 2.5 };
            var c = new[] { "hello", null, "world" };
            Console.WriteLine(c[1]);
            //var d = { 1, 2, 3, 4 };   // cannot due
            // - remember, you cannot have mixed types!
           
            // ==============
            // defining an array of objects
            // ==============
            object[] myObjects = new object[3];
            myObjects[0] = 10;
            myObjects[1] = "Form and Void";
            myObjects[2] = false;
            // this is happening because all subitems inherit from objects
            foreach (object i in myObjects) {
                Console.WriteLine(i.GetType());
            }

            // ==============
            // multidimentational array
            // ==============
            // this is regtangular array
            int[,] myMatrix;
            myMatrix = new int[6, 6];

            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    myMatrix[i, j] = i * j;
                }
            }
            // Print (6 * 6) array
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    Console.Write(myMatrix[i, j] + "\t");
                }
                Console.WriteLine();
            }

            // ==============
            // jagged array
            // ==============
            int[][] myJagArray = new int[5][];
            for (int i = 0; i < myJagArray.Length; i++) {
                myJagArray[i] = new int[i + 7];
            }
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < myJagArray.Length; j++) {
                    Console.Write(myJagArray[i][j] + " ");
                }
                Console.WriteLine();
            }

            // ==============
            // arguments or return values
            // ==============
            object[] ints = new object[3] { 2, 5, 6 };
            PrintArray(ints);
            PrintArray(GetStringArray());

            // ==============
            // interesting methods to know
            // ==============
            string[] gothicBands = { "Tones on Tail", "Bauhaus", "Sisters of Mercy" };
            
            // reverse them
            Array.Reverse(gothicBands);
            for (int i = 0; i < gothicBands.Length; i++) {
                Console.WriteLine(gothicBands[i]);
            }

            // clear out
            Array.Clear(gothicBands, 1, 2);
            for (int i = 0; i < gothicBands.Length; i++) {
                Console.WriteLine(gothicBands[i]);
            }
        }

        static void PrintArray(object[] myInts) {
            for (int i = 0; i < myInts.Length; i++) {
                Console.WriteLine("Item {0} is {1}", i, myInts[i]);
            }
        }

        static string[] GetStringArray() {
            string[] theStrings = { "Hello", "from", "GetStringArray" };
            return theStrings;
        }
    }
}
