﻿/*
 * What is the default parameter modifier?
 * - it is assumed that the incoming object (value or reference) will be
 * passed by value which means that the called method receives a copy of the original data
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _02_default_by_value {
        // ===============
        // default by Value Parameter-passing behavior
        // ===============
        static int Add(int x, int y) {
            int ans = x + y;

            // Caller will not see these changes
            // as you are modifying a copy of the original data
            x = 10000;
            y = 88888;
            return ans;
        }
        // - when the method is called, the copy of the values are passed
        // which then the modification inside the method doesn't really matter
        // - numerical data falls under the category of value types
    }
}


