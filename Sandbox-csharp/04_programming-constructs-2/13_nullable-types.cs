﻿/**
 * What is a nullable type?
 *  - it can represent all the value of its underlying type plus the value null. 
 *  
 * Why?
 *  - value types have a fixed range (System.Bookean can be assiged to either true or false)
 *  - remember that all numerical data types are value types and that value types
 *  can never be assigned the value null
 * What is "?" ?
 *  - its a c# symbol to make a variable a nullable type
 *  - it only works when applied to value types
 *  - it is a notation to creating an instance of the generic System.Nullable<T> structure type which it has members
 * 
 * What is the ?? operator?
 *  - allows to assign a value to a nullable type IF the retrieved value is i fact null
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    enum Options {
        start = 1,
        stop = 2
    }
    struct Foot {
        public string name;
        public double size;
    }
	class Car { }
    class _13_nullable_types {
        static void Main() {
            // =============
            // value types
            // =============
            bool? a3 = null;
            byte? a0 = null;
            short? a10 = null;
            int? a1 = null;
            long? a23 = null;
            char? a11 = null;
            float? a100 = null;
            double? a2 = null;
            decimal? a5 = null;
            //string? a7 = null;    
                // - doesn't work, compile error
                // - The type 'string' must be a non-nullable value type in order
                // to use it as parameter 'T'in the generic type or method 'System.Nullable<t>'
            //object? a41 = null;   // doesn't work, compile error
            Foot? f1 = null;
            Options? oStart = null;

            // =============
            // Nullable
            // =============
            System.Nullable<int> nullabeInt = 10; // the same as int? nullableInt = 10
            
            // methods such as .HasValue or value

            // =============
            // working with nullable types
            // =============
            // - these guys can be useful especially when interacting with databases given that
            // columns in a data table may be intentionally empty
            DatabaseReader dr = new DatabaseReader();
            // get int from database
            int? i = dr.GetIntFromDatabase();
            if (i.HasValue) {
                Console.WriteLine("value of 'i' is: {0}", i.Value);
            }
            else {
                Console.WriteLine("Value of 'i' is undefined");
            }

            bool? b = dr.GetBoolFromDatabase();
            if (b.HasValue) {
                Console.WriteLine("value of 'i' is: {0}", b.Value);
            }
            else {
                Console.WriteLine("Value of 'i' is undefined");
            }

            // =============
            // the ?? operator
            // =============
            Console.WriteLine(" *** ?? ***");
            int? result = 100;
            int result2 = 200;  // this will not work because it doesn't accept nulls
            int myData = result ?? 100;

            // instead of doing this
            if (!result.HasValue) {
                myData = 100;
            }

            Console.WriteLine(myData);

			// using with ienumerables
			Console.WriteLine("=======  using with ienumerables ========");
			IEnumerable<int> myInts = new int[] {};
			myInts = myInts ?? new List<int>() { 4, 5 };
			foreach (int a in myInts)
			{
				Console.WriteLine(a);
			}
			int[] please = { 1, 2, 3 };
			
			
        }

        class DatabaseReader {
            // Nullable data field
            public int? numericvalue = null;
            public bool? boolValue = true;

            // Note the nullable return type
            public int? GetIntFromDatabase() {
                return numericvalue;
            }
            // note the nullable return type
            public bool? GetBoolFromDatabase() {
                return boolValue;
            }
        }
    }
}
