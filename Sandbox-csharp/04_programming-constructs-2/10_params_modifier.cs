﻿/*
 * what is the parameter modifier?
 *  - allows to passs into a method a list of identically typed parameters as a SINGLE logical parameter
 *  OR pass a strongly type array
 *  - the purpose of this is for convenience of the caller
 *  - also to avoid ambiguity, C# demands a method only support single params argument, which must
 *  be the final argument in the parameter list
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _10_params_modifier {

        static void Main() {
            // ============
            // simple usage 1
            // ============
            // pass a list of doubles
            double average = CalculateAverage(4.0, 3.2, 5.7, 64.22, 87.2);
            Console.WriteLine("Average: {0}", average);

            // ... or pass an array of doubles
            double[] data = { 4.0, 3.2, 5.7, 64.22, 87.2 };
            double average1 = CalculateAverage(data);
            Console.WriteLine("Average: {0}", average1);

            // Average is 0 is 0!
            Console.WriteLine("Average: {0}", CalculateAverage());
        }

        // - defined to take a parameter array of doubles
        // - return average of 'some number' of doubles
        static double CalculateAverage(params double[] values) {
            Console.WriteLine("You sent me {0} doubles.", values.Length);
            double sum = 0;
            if (values.Length == 0) {
                return sum;
            }
            for (int i = 0; i < values.Length; i++) {
                sum += values[i];
            }
            return (sum / values.Length);
        }
    }
}
