﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _07_typeof_GetType {
        static void Main() {
            // =================
            // GetType() vs typeof (operator)
            // =================
            int i = 10;
            Console.WriteLine(i.GetType());
                // System.Int32
            Console.WriteLine(typeof(int));
                // System.Int32
            // - as you can see, using typeof, you do not need to 
            // have a variable of the entity you wish to obtain a metadata description of
            // - GetType() and typeof retrieves the metadata of the type
        }
    }
}
