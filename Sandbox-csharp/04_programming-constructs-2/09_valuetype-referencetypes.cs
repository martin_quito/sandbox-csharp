﻿/**
What are value types?
    - any object that derives (implicitly or explictly) from System.ValueType is a value type. This includes all 11 system types
that are CLS compatible AND enumerations AND structurs. The funny thing is that those 11 types
are structures. Also know that structures don't have a name represetnation (System.structure) in
the .NET library.
    - they are two kinds of valye types
        - structs
        - enumereatios

What are some major differences between a value type and refernece type
    - a variable of type 'int' may contain a value. A variable of reference type
    contins a REFERENCE, not a value, to an instance of the type (aka objet)
    - when you assign a new value to a variable of a value type, the value gets copied.
    In refernce types, the reference is copied
    - you cannot dereived a new type from a value type, but they can implement interfaces
    - value type variables cannot be NULL by default, hownever, values of the 
    corresponding nullabletypes can be null
    - each value type has an IMPLICIT CONSTRUCTOR THAT initialize the default
    of that type. 
        bool            false       (simple type)                 
        bype            0           (simple type)
        char            '\0'        (simple type)
        decimal         0M          (simple type)
        double          0.0D        (simple type)
        enum            The value produced by the expression (E)0, where E is the enum identifier.
        float           0.0F        (simple type)
        int             0           (simple type)
        long            0L          (simple type)
        sbyte           0           (simple type)
        short           0           (simple type)
        struct          The value produced by setting all value-type fields to their default values and all reference-type fields to null.
        uint            0           (simple type)
        ulong           0           (simple type)
        ushort          0           (simple type)
    
        Simple types
            integral types (integer numeric types and the char type)
            floating point types
            bool

            The simple type are identigied through keywords and these keywords
            are simple aliases for predefined struct types in the system namespace
            
            These guys differe from other struct types 
                - simple types can be initialize by using literals. For example
                'A' is a literal of the type 'char' and '2001' is a literal of the
                type 'int'
                - you can declare onstants of the simple types witht he 'const'
                keyword. Its not possible to have constants of other struc types

    
What is the role of value type?
    - ensure its derived types are allocated on the stack (data allocated on a stack can
    be created and destroyed very quickly)
    - to overwrite the virtual methods defined its base class, System.Object which enables
    us to use the value-based vs referenced based.
    
What happens when you assing one VALUE TYPE to another?
- the values get copied, in the case of System.Int32, only the numerical
    value gets copied. In a case of a custom structure type, the data fields get copied
- remember when declare a structure, you are declaring a value type
    
What happens when you assing a reference type to antoher?
- both will point to the same object
    
What happens when VALUE TYPES objects contain REFERENCE TYPES?
- by default, when a value type contains other reference types, 
assignment results in a copy of the references. If you want to 
perform a deep copy, where the internal referemces is fully copied into a new object,
one to approach is to implement the 'ICloneable' interface
    
What about passing reference types BY value?
    - see below
    
What about passing references types BY references?
    - see below
    
What about passing value types BY value?
    - the values get copied
    
What about passing value types by reference?
    - ???
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    struct PointB {
        // fields of structure
        public int X;
        public int Y;

        public PointB(int x, int y) {
            X = x;
            Y = y;
        }

        // Add 1 to the (x, y) position
        public void Increment() {
            X++;
            Y++;
        }

        // substract 1 from (x, y)
        public void Decrement() {
            X--;
            Y--;
        }
        // display current pos
        public void Display() {
            Console.WriteLine("X = {0}, Y = {1}", X, Y);
        }
    }

    class PointRef {
        // fields of structure
        public int X;
        public int Y;

        public PointRef(int x, int y) {
            X = x;
            Y = y;
        }
        public void Display() {
            Console.WriteLine("X = {0}, Y = {1}", X, Y);
        }
    }

    class Person {
        public string personName;
        public int personAge;
        public Person(string name, int age) {
            personName = name;
            personAge = age;
        }
        public Person() { }
        public void Display() {
            Console.WriteLine("Name: {0}, Age: {1}", personName, personAge);
        }
    }

    class _09_valuetype_referencetypes {
        static void Main() {
            // ==============
            // example of a value type
            // ==============
            // int is really a System.In32 structure
            int i = 0;

            // recall! Point is a structure type
            Point p = new Point();
            // when this method is finished, out of scope, "i" and "p" popped off the stack here!

            // ==============
            // assignment operator on value types
            // ==============
            PointB p1 = new PointB(10, 10);
            PointB p2 = p1;

            // print both points.
            p1.Display();
            p2.Display();

            // change p1.X and print again. p2.X is not changed.
            p1.X = 100;
            Console.WriteLine("\n => Changed p1.X\n");
            p1.Display();
            p2.Display();
            // - as you can see, the stack contains two copies of MyPoint, each
            // of which can be independently manipulated

            // ==============
            // assignm,ent opeartor on reference types
            // ==============
            Console.WriteLine("===== assignment reference types =====" );
            PointRef p3 = new PointRef(10, 10);
            PointRef p4 = p3;
            p3.Display();
            p4.Display();
            p3.X = 100;
            p3.Display();
            p4.Display();
            // the two references pointing to the same object on the MANAGED HEAP

            // ==============
            // passing refernce by value
            // ==============
            Person fred = new Person("fred", 12);
            Console.WriteLine("\nBefore by value call, Person is: ");
            fred.Display();
            SendAPersonByValue(fred);
            Console.WriteLine("\nAfter by value call, Person is:");
            fred.Display();

            // - passing a refernece by value to a method
            // the callee will let you update the members but it is not
            // possible to reassign what the reference is pointing to
            // - so yes, the reference gets passed but has restrictions

            // ==============
            // passing refernce by reference
            // ==============
            // - same as value but this time the callee lets you
            // reassingm the reference to something else
            Person martin = new Person("martin", 12);
            Console.WriteLine("\nBefore by value call, Person is: ");
            martin.Display();
            SendAPersonByRef(ref martin);
            Console.WriteLine("\nAfter by value call, Person is:");
            martin.Display();
        }

        static void SendAPersonByValue(Person p) {
            // Change the age of "p"?
            p.personAge = 99;

            // will the caller see this reassignment?
            p = new Person("Nikki", 99);
        }

        static void SendAPersonByRef(ref Person p) {
            // Change the age of "p"?
            p.personAge = 99;

            // will the caller see this reassignment?
            p = new Person("Nikki", 99);
        }
    }
}

namespace valuetype {
    // ================
    // initialize value types
    // ================
    class MyValueType {
        static void Main() {
            // declare without initialization
            int myInt;

            // initialize
            myInt =  new int();

            // this statement is equivalent to the following statement
            myInt = 0;

            // declare and initialize
            int myInt2 = new int();
                // returns 0 as the value

            // OR
            int myInt3 = 0;
        }
    }
}