﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _06b_enum {
        enum EntityType {
            Course = 1,
            Program = 2
        }
        static void Main() {
            EntityType a1 = EntityType.Course;
            EvaluateEnum(a1);
        }

        static void EvaluateEnum(System.Enum e) {
            Console.WriteLine("=> Information about {0}", e.GetType().Name);
            // get all name/value pairs for incoming paramter
            Array enumData = Enum.GetValues(e.GetType());
            Console.WriteLine("This enum has {0} memebrs.", enumData.Length);
            for (int i = 0; i < enumData.Length; i++) {
                Console.WriteLine("Name: {0}, Value: {0:D}", enumData.GetValue(i));
            }
        }
    }
}
