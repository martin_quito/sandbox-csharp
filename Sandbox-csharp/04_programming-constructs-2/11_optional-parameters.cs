﻿using System;
/**
 * Why?
 *  - "allows the caller to invoke a single method while omitting arguments
 *  deemed unnecessary, provided the caller is happy with the specified defaults" (book 1)
 *  - the key motivation for adding optional arguments to C# is to simplify interacting with COM objects
 * 


 * How?
 *  - the value assigned to an optional parameter MUST BE known at compile time, and cannot be resolved
 *  at runtime (otherwise, you will get compile errors)
 *  - to avoid ambiquity, optional paramets must be placed at the end of a method signature. Otherwise, compile error.
 */
namespace Sandbox_csharp._04_programming_constructs_2 {
    class _11_optional_parameters {
        static void Main() {
            // ==========
            // optional params
            // ==========
            EnterLogData("Oh no! Grid cant find data");
            EnterLogData("Oh no! I can't find the payroll data", "CFO");
        }

        // - the last paramenter has an argument assigned (default value) of "Programmer" 
        // within the parameter definition
        static void EnterLogData(string message, string owner = "Programmer") {
            Console.Beep();
            Console.WriteLine("Error: {0}", message);
            Console.WriteLine("Owner of Error: {0}", owner);
        }
    }
}
