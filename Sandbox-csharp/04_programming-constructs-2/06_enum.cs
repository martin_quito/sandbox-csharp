﻿/**
 * What is an enum type?
 *  - remember that the .NET type system is composed of classes, structures, interfaces, enumerations, and delegates,
 *  so here enum refers to enumerations
 *  - 'enum' and 'enumerator' are different things. enum is a data type of name/value paris where as
 *  enumerator is a 'class' or 'structure' that implements a .net interfacce name 'IEnumerable'
 *  - use to create a set of symbolic names that map to known numerical values
 *  - it cannot be inside a method, i think it works like a class
 *  - enumerators are nothing morre than a user-defined type
 *  - enumerations are used extensively throughout the .NET base class libraries
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {

    class _06_enum {
        // ===============
        // a simple custom enum
        // ===============
        // - by default, the first element is set to the value of zero followed by a pattern of n+1 progression
        enum EmpType {
            Manager,
            Grunt,
            Contractor,
            VicePresident
        }

        // ===============
        // setting intial values
        // ===============
        enum EmpType2 {
            Manager = 102,
            Grunt = 2,
            Contractor = -1,
            VicePresident = 3
        }
        // - see that it doesn't need to follow a sequential oder
        // - the valeus don't need to be unique


        // ===============
        // controlling the underlying storage for an enum
        // ===============
        // - by default, the data type to store the values are in System.Int32
        enum EmpType3 : byte{
            Manager = 10,
            Grunt = 1
        }

        static void Main() {
            // ===============
            // declare num variables
            // ===============      
            // - after establishing a rnage and storage type of your enumeration, you can use it
            // in place of so called "magic numbers"
            Console.WriteLine(" *** Fun with Enums *** ");

            // assigning a value to an enum variable
            EmpType2 emp = EmpType2.VicePresident;
            Console.WriteLine(AskForBonus(emp));

            // ===============      
            // The System.Enum type
            // ===============     
            // a useful member is the static 'Enum.GetUnderlyingType()' which returns the data type
            // used to store the values of the enumeratred type
            EmpType con = EmpType.Contractor;
            Console.WriteLine(con.GetType()); // returns the data type of the custom enumerator
            Console.WriteLine("EmpType uses a {0} for storage",
                Enum.GetUnderlyingType(con.GetType()));
            Console.WriteLine("EmpType uses a {0} for storage",
                Enum.GetUnderlyingType(typeof(EmpType)));

            // ===============     
            // dynam ically discover an emu's name/value pairs
            // ===============     
            EmpType2 emp2 = EmpType2.Contractor;
            Console.WriteLine("{0} = {1}", emp2.ToString(), (byte)emp2);

        }

        static string AskForBonus(EmpType2 e) {
            string output = "";
            switch (e) {
                case EmpType2.Manager:
                    output = "Whow about stopc options instead?";
                    break;
                case EmpType2.Grunt:
                    output = "You have got to be kidding ...";
                    break;
                case EmpType2.Contractor:
                    output = "You already get enough cash ...";
                    break;
                case EmpType2.VicePresident:
                    output = "VERY GOOD, SIR";
                    break;
                default:
                    output = "aha";
                    break;
            }
            return output;
        }

    }
}
