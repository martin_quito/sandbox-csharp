﻿/*
 * What is an out?
 *  - it is a parameter modifier where it will gets its assignment value
 *  by the method being called and therefore are passed by reference. If the called
 *  method fails to assign output paramter, a compiler error is thrown.
 *  - known as a output parameter
 *  - these guys don't need to be initalized before they passed to the method. The reason
 *  for this is that the method must assing output parameters before existing.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _03_out {
        static void Main() {
            // ===============
            // out (simple example)
            // ===============
            // - no need to assing initial value to local
            // variables used as output paramters because
            // they will need to get reassigned!
            int ans;
            Add(90, 90, out ans);
            Console.WriteLine("90 + 90 = {0}", ans);

            // ===============
            // a more real usage
            // ===============
            // allows the caller to obtain multiple outputs from a single method invocation
            int a;
            string b;
            bool c;
            Object my = new Object();
            Object my2 = my;
            FillTheseValues(out a, out b, out c, out my);
            // - what you need to remember is when using 'out' parmater modifier, you must
            // always assing the paramter to a valid value before existing the method scope
            Console.WriteLine(my2 == my);
        }

        static void Add(int x, int y, out int ans) {
            ans = x + y;
        }

        static void FillTheseValues(out int a, out string b, out bool c, out Object myObj) {
            a = 9;
            b = "Enjoy your string";
            c = true;
            myObj = new Object();
        }
    }
}
