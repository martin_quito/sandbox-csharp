﻿/*
 * What are structure types?
 * - this is part of the .NET system (structures)
 * - lighweight classes and best suited for modeling mathematical, geometrical, and other atomic entities in ur application
 * - cannot be family of tpyes
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
    // ==========
    // defining struct
    // ==========
    struct Point {
        // fields of structure
        public int X;
        public int Y;

        public Point(int x, int y) {
            X = x;
            Y = y;
        }

        // Add 1 to the (x, y) position
        public void Increment() {
            X++;
            Y++;
        }

        // substract 1 from (x, y)
        public void Decrement() {
            X--;
            Y--;
        }
        // display current pos
        public void Display() {
            Console.WriteLine("X = {0}, Y = {1}", X, Y);
        }
    }

    class _08_structure_type {
        static void Main() {
            // =============
            // basic use
            // =============
            Console.WriteLine("*** First look at structures ***");
            Point myPoint;
            myPoint.X = 349;
            myPoint.Y = 76;
            myPoint.Display();

            myPoint.Increment();
            myPoint.Display();

            // =============
            // create structure varibles
            // =============
            // - simply create a struct variable and assign each
            // piece of public field data before invoking its memebrs, if you do not
            // you will get an compile error
            Point p1;
            p1.X = 10;
            p1.Y = 20;
            p1.Display();

            // - using the C# new keyword which will invoke the structure's default constructor
            // and when that is incoked, each piece of field data is automatically set to its default value
            Point p2 = new Point();
            p2.Display();

            // you can use a custom constructor
        }
    }
}
