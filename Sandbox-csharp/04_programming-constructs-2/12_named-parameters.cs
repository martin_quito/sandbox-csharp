﻿/*
 * What?
 *  - named argumnets allow you to invoke a method by specifying parameter values
 *  in any order you choose
 * Why?
 *  - to simplify interaction with COM objects
 *  - helps when using optional parameters
 * How?
 *  - know that named argumnets must always be packed onto the end of a method call. 
 *  It is okay to have positional arguments before name args. But, it is an error if
 *  name args are before positional args
 */
using System;

namespace Sandbox_csharp._04_programming_constructs_2 {
    class _12_named_parameters {
        static void Main() {
            // =============
            // simple usage
            // =============
            Console.WriteLine("*** Fun with MEthods ***");
            DisplayFancyMessage
                (
                    message: "Wow! Very Fancy Indeed!",
                    textColor: ConsoleColor.DarkRed,
                    backgroundColor: ConsoleColor.White
                );
            // =============
            // using optional parameters
            // =============    
            DisplayFancyMessage2(message: "Helllo");
            // not having the named parameters
            DisplayFancyMessage2(ConsoleColor.Blue, ConsoleColor.White, "Hello");

        }

        static void DisplayFancyMessage(ConsoleColor textColor, ConsoleColor backgroundColor, string message) {
            // store old colors to restore after message is printer.
            ConsoleColor oldTextColor = Console.ForegroundColor;
            ConsoleColor oldBackgroundColor = Console.BackgroundColor;

            // set new colors and print message.
            Console.ForegroundColor = textColor;
            Console.BackgroundColor = backgroundColor;
            
            Console.WriteLine(message);

            // restore previous colors.
            Console.ForegroundColor = oldTextColor;
            Console.BackgroundColor = oldBackgroundColor;
        }

        static void DisplayFancyMessage2(ConsoleColor textColor = ConsoleColor.Blue
            , ConsoleColor backgroundColor = ConsoleColor.White
            , string message = "Test message") {
            // store old colors to restore after message is printer.
            ConsoleColor oldTextColor = Console.ForegroundColor;
            ConsoleColor oldBackgroundColor = Console.BackgroundColor;

            // set new colors and print message.
            Console.ForegroundColor = textColor;
            Console.BackgroundColor = backgroundColor;

            Console.WriteLine(message);

            // restore previous colors.
            Console.ForegroundColor = oldTextColor;
            Console.BackgroundColor = oldBackgroundColor;
        }
    }
}
