﻿/**
 * What is the ref?
 * - known as reference parameters
 * - it is a parameter modifier
 * - its behavior changes when the argument is a value or a reference
 *      - when is a refernece, (ref Reference), the callee can have the reference point to another object) If it didn't have
 *       the ref modifier, it can only update members not not repointing the reference
 *      - when is a value, it works similar to 'out' except that is not required to assign a value
 *      in the called method. 
 * - it is necessary when you wish to allow a method to OPERATE ON data points declared in the caller's scope (sorting or swapping routine)
 * - "Reference parameters must be initialized before they are passed to the method. The reason for this is that you are 
 * passing a reference to an existing variable. If you don't assign it to an initial value, that would
 * be the equivalent of operating on an unassigned local variables"
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._04_programming_constructs_2 {
	class MyPerson {
		public string Name { get; set; }
		public int Age { get; set; }
	}

    class _04_ref_modifier {
        static void Main() {
            // =============
            // refernece parameters
            // =============   
            string str1 = "Flip";
            string str2 = "Flop";
            Console.WriteLine("Before: {0}, {1} ", str1, str2);
            SwapStrings(ref str1, ref str2);
            Console.WriteLine("After: {0}, {1} ", str1, str2);

			// =============   
			// value ref
			// =============   
			int myNum1 = 100;
			int myNum2 = 200;
			Console.WriteLine("Before: {0}, {1} ", myNum1, myNum2); // 100, 200
			SwapNumbers(ref myNum1, ref myNum2);
			Console.WriteLine("After: {0}, {1}", myNum1, myNum2); // 200, 100

			// =============   
			// object
			// =============   
			MyPerson p1 = new MyPerson() { Name = "Martin", Age = 28 };
			changePersonMembers(p1);
			Console.WriteLine("Person, Name={0}, Age={1}", p1.Name, p1.Age);
				//-> works as expected

			MyPerson p2 = new MyPerson() { Name = "Martin", Age = 28 };
			changePersonMembersRef(ref p2);
			Console.WriteLine("Person, Name={0}, Age={1}", p2.Name, p2.Age);
				//-> works as expected


			MyPerson p3 = new MyPerson() { Name = "Jonathan", Age = 30 };
			CreateNewPerson(p3);
			Console.WriteLine("Person, Name={0}, Age={1}", p3.Name, p3.Age);
				// -> the method, callee, cannot repoint it to a new object
				// it requires a ref
				// -> here, you can ONLY update methods

			MyPerson p4 = new MyPerson() { Name = "Jonathan", Age = 30 };
			CreateNewPersonRef(ref p4);
			Console.WriteLine("Person, Name={0}, Age={1}", p4.Name, p4.Age);
        }

        static void SwapStrings(ref string s1, ref string s2) {
            // the value of s1 gets copied to a local variable
            string tempStr = s1;
            // the values of s2 gets copied to s1 (same as in the caller's scope)
            s1 = s2;
            s2 = tempStr;
        }

		static void SwapNumbers(ref int s1, ref int s2) {
			int tempInt = s1;
			s1 = s2;
			s2 = tempInt;
		}

		static void changePersonMembers(MyPerson p1) {
			p1.Name = "BOB";
		}

		static void changePersonMembersRef(ref MyPerson p1) {
			p1.Name = "BOB";
		}

		static void CreateNewPerson(MyPerson p1) {
			p1 = new MyPerson();
		}

		static void CreateNewPersonRef(ref MyPerson p1) {
			p1 = new MyPerson();
		}
    }
}
