﻿/*
 * What?
 *	- delegate that accepts type parameters
 */

using System;
using System.Collections.Generic;
namespace GenericDelegates
{
	// ================
	// simple
	// ================
	// - this generic delegate can call anymethod 
	// returning void and taking a single type paramter
	public delegate void MyGenericDelegate<T>(T arg);

	class Program
	{
		static void Main()
		{
			MyGenericDelegate<string> t = new MyGenericDelegate<string>(StringTarget);
			t("Some string data");

			MyGenericDelegate<int> t2 = new MyGenericDelegate<int>(IntTarget);
			t2(9);
		}

		static void StringTarget(string arg)
		{
			Console.WriteLine("arg in uppercase is: {0}", arg.ToUpper());
		}
		static void IntTarget(int arg)
		{
			Console.WriteLine("++arg is: {0}", ++arg);
		}
	}

	// ================
	// generic delegate as a paramenter
	// ================
	// these delegate is generic 
	public delegate void DelegateA<T>(T arg);
	class Dummy {
		public void A(DelegateA<int> b) {

		}

		public static void CallBack<T>(T msg)
		{
			Console.WriteLine("Exuectiong callback! {0}", msg);
		}

		public static void Main()
		{
			Dummy d1 = new Dummy();
			// creaeting a delegate (type string)
			DelegateA<int> delegate1 = new DelegateA<int>(CallBack);
			delegate1(2);
		}
	}

	// ================
	// simulation checklists
	// ================
	public delegate void Hide<T>();
	public delegate void Show<T>();
	public class ActionHide { }
	public class ActionShow { }

	public class Checklist {
		private Hide<ActionHide> HideHandlers;
		public void AddHideHandlers(Hide<ActionHide> h)
		{
			HideHandlers += h;
		}

		private Show<ActionShow> ShowHandlers;
		public void AddHideHandlers(Show<ActionShow> h)
		{
			ShowHandlers += h;
		}

		public void Hide()
		{
			HideHandlers();
		}
	}

	public class ChecklistItem {
		public string Name { get; set; }
		public void Hide()
		{
			Console.WriteLine("{0} is Hiding", this.Name);
		}

		public void Show() 
		{
			Console.WriteLine("{0} is Showing", this.Name);
		}
	}

	class P10
	{
		static void Main()
		{
			List<ChecklistItem> cl = new List<ChecklistItem>
			{
				new ChecklistItem() {Name = "martin"},
				new ChecklistItem() {Name = "nefi"}
			};
			Checklist parent = new Checklist();
			foreach (ChecklistItem l in cl)
			{
				parent.AddHideHandlers(new Hide<ActionHide>(l.Hide));	
			}
			parent.Hide();
		}
	}

}
