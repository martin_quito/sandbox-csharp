﻿/*
What is an expression in LINQ?
    Func<T> = Lambda expression 
        (compiles into =>) Executable Code
            (Executes in =>) Same AppDomain
    Expression<TDelegate> = lambda expression
        (compiles into =>) Expression Tree
            (Used as a data structyre by =>) Remote LINQ PRovider (Different appDomain)

    A fancy anonymous method (LINQ) cam be added to a delegate (FUNC) which procudes
    executable code to be ran. But when that fanncy anonmyous method is added
    to a n Expression<TDelegate> it results in a "Expression tree" which is 
    used by remote LINQ query providers as a data structure to build a runtime
    query out of it (such as LINQ to SQL, EntityFramework or any other LINQ
    query provider that implements IQuerytable<T> interface)

*/