﻿/*
 * What?
 *	- the problem is that when a caller wants to listen for events, it must define a custom
 *	method in a class (or structure) that matches the signatureof the associated delegate. So, 
 *	these custom methods I created to server as event handlers are seldom intended to be called by any part
 *	of the program other than the invoking delegate. It is not a 'show-stopper' but it affects productivity.
 *	To address this point, it is possible to associate an event directly to a block of code statements at the
 *	time of event registration. 
 *	
 * Things to know about anonymous types?
 *	- you can access the local variales of the method that defines them (these guys are termed as outer variables)
 *	- cannot access 'ref' or 'out' parameters of the definiting method
 *	- cannot have a local variable with the same name as a local variable in the outer method
 *	- can access instance variable (or static) in the outer class scope
 *	- can declare local variables with the same name as outer class member variables 
 *	(the local variables have a distinct scope and hide the otuer class member variables)
 */
using System;
using System.Collections.Generic;
namespace AnonymousMethod
{
	public class CarEventArgs : EventArgs
	{
		public readonly string msg;
		public CarEventArgs(string message)
		{
			msg = message;
		}
	}
	class Car
	{
		public int CurrentSpeed { get; set; }
		public int MaxSpeed { get; set; }
		public string PetName { get; set; }

		private bool carIsDead;
		public Car() { MaxSpeed = 100; }
		public Car(string name, int maxSp, int currSp)
		{
			CurrentSpeed = currSp;
			MaxSpeed = maxSp;
			PetName = name;
		}

		public delegate void CarEngineHandler(object sender, CarEventArgs e);
		public event CarEngineHandler Exploted;
		public event CarEngineHandler AboutToBlow;
		public void Accelerate(int delta)
		{
			if (carIsDead)
			{
				if (Exploted != null)
				{
					Exploted(this, new CarEventArgs("Sorry, this car is dead ..."));
				}
			}
			else
			{
				CurrentSpeed += delta;

				// almost dead?
				if (10 == MaxSpeed - CurrentSpeed && AboutToBlow != null)
				{
					AboutToBlow(this, new CarEventArgs("Careful buddy! Gonna blow!"));
				}

				// still ok!
				if (CurrentSpeed >= MaxSpeed)
				{
					carIsDead = true;
				}
				else
				{
					Console.WriteLine("CurrentSpeed = {0}", CurrentSpeed);
				}
			}
		}
	}
	class P
	{
		static void Main()
		{
			Car c1 = new Car("SlugBug", 100, 10);
			int aboutToBlowCounter = 0;

			c1.AboutToBlow += delegate
			{
				aboutToBlowCounter++; // outer variable
				Console.WriteLine("Eel! Going to fast!");
			};

			c1.AboutToBlow += delegate(object sender, CarEventArgs e)
			{
				aboutToBlowCounter++;
				Console.WriteLine("Message from Car: {0}", e.msg);
			};

			c1.Exploted += delegate(object sender, CarEventArgs e)
			{
				Console.WriteLine("Fatal Message from Car: {0}", e.msg);
			};

			// either takes no arguments or it does
			c1.Exploted += delegate
			{
				Console.WriteLine("Exploted?!");
			};

			// run
			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}
			Console.WriteLine(aboutToBlowCounter);
		}
	}
}
