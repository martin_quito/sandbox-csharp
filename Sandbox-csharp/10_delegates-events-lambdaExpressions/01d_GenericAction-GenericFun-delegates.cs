﻿/**
 * What?
 *	- a recap, delegates are used to enable callbacks in your app. Typically, you will follow these steps
 *		- define a custom delegate that matches the format of the method being pointed to
 *		- create an instance of your custom delegate, passing in a method name as a constructor argument
 *		- invoke the method indirectly, via a call to Invoke() on the delegate object
 *	  The problem is that when this approach is used, a number of delegates are created that might
 *	  never be used beoyond the current task at hand.
 *	  Another problem is, evn though sometimes you need a custom, uniquely named delegate, most of the
 *	  time, the exact name of the delegate DOES NOT MATTER>. In many cases, you simply
 *	  want "some delegate" that takes a set of arguments and possibly has a return value other than void. If that is the
 *	  case, then I can use .NET farmewokr builtin Action<> and Func<> delegates
 *	  
 * Action<>
 *	- returns void
 *	- up to 16 params
 * Function<>
 *	- returns value
 *	- up to 16 parms
 *	- the last type tparamter is the type for the return type
 */
using System;
using System.Collections.Generic;

namespace DelegatesGenericActionFunc
{
	class P {
		static void Main()
		{
			// ============
			// using Action<T>
			// ============
			// instead of creating a named delegate (custom), Im using the Action<T> delegate 
			Action<string, ConsoleColor, int> actionTarget = new Action<string, ConsoleColor, int>(DisplayMessage);
			// invoke
			actionTarget("MARTIN", ConsoleColor.Yellow, 2);

			// ============
			// using Func<T>
			// ============
			Func<int, int, string> f = new Func<int, int, string>(Add);
			Console.WriteLine(f(10, 15));
		}

		static void DisplayMessage(string msg, ConsoleColor txtColor, int printCount) 
		{
			ConsoleColor previous = Console.ForegroundColor;
			Console.ForegroundColor = txtColor;
			for (int i = 0; i < printCount; i++)
			{
				Console.WriteLine(msg);
			}
			Console.ForegroundColor = previous;
		}

		static string Add(int x, int y)
		{
			return (x + y).ToString();
		}
	}
	
}
