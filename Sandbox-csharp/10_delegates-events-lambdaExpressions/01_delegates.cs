﻿/**
 * What is a delegate type?
 *  - historically, windows API made frequent use of C-style function pointers to
 *  create termed callback functions. Using callbacks, programmers were able to
 *  configure functions to call another function (callback). This approach
 *  let windows developers handle button-clicking, mouse-moving, and general
 *  bidirectional communications between two entities in memory. The problem with this approach
 *  is that c-style callback functions represent little more than a raw address in memory. Ideally, we
 *  would like those callbacks functions to support type safety. In .NET, callbacks with support of
 *  type safety are called 'delegates' 
 *  
 * - a delegate type is a type-safe object that points to another method (or list of methods) in the application
 * which can invoke at a later time
 *		- the delegate type is the object that will INVOKE functions (callback functions)
 * 
 * - .net delegates can point to either static or instance methods
 * 
 * - deletate types has 3 main parts
 *		 - the address of the method on which it make calls
 *		 - the parameters (if any) of this method
 *		 - the return type (if any) of this method
 *  
 * What is a callback?
 *	- calling back a piece of code that was received
 *	
 * What does the 'delegate' keyword do?
 *	- when C# compiler processes a delegate type, it generates code
 *		- a sealed class deriving System.MulticastDelegate which derives from System.Delegate (read book 1 for more details)
 *		
 * Things to note when creating a delegate type?
 *	- the parameter signature needs to match
 *	- the actual name of the method pointed to is irrelevant
 */
using System;

namespace Delegates
{
	// ===============
	// simplest possible delegate
	// ===============
	// - this delegate can POINT to any method,
	// taking two integers and returning an integer
	public delegate int BinaryOp(int x, int y);

	// this class contains methods BinaryOp will POINT TO
	public class SimpleMath
	{
		public static int Add(int x, int y) { return x + y; }
		public static int Subtract(int x, int y) { return x - y; }
		public static int SquareNumber(int a) { return a * a; }
	}
	class Program
	{
		static void Main()
		{
			// - passing the target method to a given delegate object
			// - its like passing a callback functions to a function that will invoke it
			BinaryOp b = new BinaryOp(SimpleMath.Add);
				// Invoke() is actually being called here
			Console.WriteLine(b(10, 10));

			// - because delegate types are type safe, if you attempt to
			// pass a method that does not match the pattern, you receive a compile error
			//BinaryOp b2 = new BinaryOp(SimpleMath.SquareNumber);

			// investingate a delete object
			DisplayDelegateInfo(b);

		}

		static void DisplayDelegateInfo(Delegate delObj)
		{
			foreach (Delegate d in delObj.GetInvocationList())
			{
				Console.WriteLine("Method name: {0}", d.Method);
				Console.WriteLine("Type Name: {0}", d.Target);
					// - is empty if the method is static, but if is an instance
					// then it would output the type
			}
		}
	}

	// ===============
	// sending object state notifications using delegates (a more realistic example)
	// ===============
	public class Car
	{
		// internal state data
		public int CurrentSpeed { get; set; }
		public int MaxSpeed { get; set; }
		public string PetName { get; set; }

		private bool carIsDead;
		public Car() { MaxSpeed = 100; }
		public Car(string name, int maxSp, int currSp)
		{
			CurrentSpeed = currSp;
			MaxSpeed = maxSp;
			PetName = name;
		}
		// delegate types

		// 1) define delegate type
		public delegate void CarEngineHandler(string msgForCaller);

		// 2) define a member variable of this delegate
		private CarEngineHandler listOfHandlers;

		// 3) Add regristration function for the caller
		public void RegisterWithCarEngine(CarEngineHandler methodToCall)
		{
			listOfHandlers += methodToCall;

			// or
			//if (listOfHandlers == null)
			//{
			//	listOfHandlers = methodToCall;
			//} else { 
			//	Delegate.Combine(listOfHandlers, methodToCall);
			//}
		}

		public void UnRegisterWithCarEngine(CarEngineHandler methodToCall)
		{
			listOfHandlers -= methodToCall;
		}

		// 4) Implement the Accelerate() method to invoke the delegate's 
		// invocation list under the correct circumstances.
		public void Accelerate(int delta)
		{
			// if this car is 'dead' , send dead message
			if (carIsDead)
			{
				if (listOfHandlers != null)
				{
					listOfHandlers("Sorry, this car is dead");
				}
			}
			else
			{
				CurrentSpeed += delta;

				// if speed is gettig close to max speed, warning
				if (
					10 == (MaxSpeed - CurrentSpeed)
					&& listOfHandlers != null
				)
				{
					listOfHandlers("Careful buddy! Gonna blow!");
				}

				// if speed is greater than max speed, it dies
				if (CurrentSpeed >= MaxSpeed)
				{
					carIsDead = true;
				}
				else
				{
					Console.WriteLine("CurrentSpeed = {0}", CurrentSpeed);
				}
			}
		}
			// - the reason why we are checking our privatea delegate (publisher)
			// is because if the caller does not register functions to the delegate
			// then  we will trigger a 'NullReferenceExecption' at runtime

	}

	public class B
	{
		static void Main()
		{
			Car c1 = new Car("SlugBug", 100, 10);
			// tell the car which method to call
			// when it wants to send us messages
			c1.RegisterWithCarEngine(new Car.CarEngineHandler(OnCarEngineEvent)); // creates a delegate and passes it

			// pass another one (multicasting)
			Car.CarEngineHandler handler2 = new Car.CarEngineHandler(OnCarEngineEvent2);
			c1.RegisterWithCarEngine(handler2);

			Console.WriteLine("*** Speeding up ***");
			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

			// unrefister from the second handler
			c1.UnRegisterWithCarEngine(handler2);

			// fix car
			Console.WriteLine("*** Speeding up2 ***");
			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

		}

		public static void OnCarEngineEvent(string msg)
		{
			Console.WriteLine("=> {0}", msg.ToLower());
		}

		public static void OnCarEngineEvent2(string msg)
		{
			Console.WriteLine("=> {0}", msg.ToUpper());
		}
	}

	// ===============
	// eabling multicasting
	// ===============
	/**
	 * - you can use the += operato on a delegate object , the compiler resolves this to a call on the static 
	 * Deletgate.Combine() method. (See above)
	 */

	// ===============
	// removing targets from a delegate's invokation list
	// ===============
	/*
	 * - The Delegate class also defiens 'Remove' static method to unsubscribe a method
	 * - you can use 'Remove' or -=
	 */
}

