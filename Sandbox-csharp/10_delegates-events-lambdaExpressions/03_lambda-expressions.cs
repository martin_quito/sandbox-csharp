﻿/*
 * What?
 *	- it is a very concise way to author anonymous methods and ultimately simplify
 *	how we work with the .NET delegate type
 *	- you can use it anywhere you would have used an anonymous method or
 *	a strongly typed delegate
 * - the compiler translates the lambda expression into a standard anonymous
 * method making use the target delegate (e.g. Predicate<T> delegate type)
 * 
 * Other?
 *	- the 'event' helps in simplifying the process of sending yourt event notifications to waiting callers
 *	- the anonymous methods helps in setting handlers (or registering) to delegates to invoke
 *	- lambda expressions is there to provide a clean, concise manner to define an anonymous method
 */
using System;
using System.Collections.Generic;

namespace lambda
{

	// ====================
	// 1
	// ====================
	class Program
	{
		static void Main()
		{
			// ====================
			// using raw delegation
			// ====================
			List<int> list = new List<int>();
			list.AddRange((new int[] { 20, 1, 4, 8, 9, 44 }));

			// call FindAll() using traditional delegate syntax
			Predicate<int> callback = new Predicate<int>(IsEventNumber);
			List<int> eventNumbers = list.FindAll(callback);
			Action<int> callback2 = new Action<int>(Print);
				// - remember that the action is a generic delegate where the methods
				// i will assign it to match the return type and arguments
			eventNumbers.ForEach(callback2);
			
			/*
			 * So, Predicate, Func, and Action are different but similar generic delegates
			 * 
			 * Action => returns void
			 * Predicate => returns bool 
			 * Func => return a value
			 */
			// ====================
			// using anonymous
			// ====================			
			List<int> list2 = new List<int>();
			list2.AddRange((new int[] { 20, 1, 4, 8, 9, 44 }));
			List<int> result2 = list2.FindAll(delegate(int i)
			{
				return (i % 2) == 0;
			});
			
			result2.ForEach(delegate
			{
				Console.WriteLine("No number?");
			});

			result2.ForEach(delegate(int i) {
					Console.WriteLine(i);
			});

			// ====================			
			// using lambda
			// ====================		
			Console.WriteLine("*** Using Lambda ****");
			List<int> list3 = new List<int>();
			list3.AddRange((new int[] { 20, 1, 4, 8, 9, 44 }));
			List<int> result3 = list3.FindAll(i => (i % 2) == 0);
			result3.ForEach(x =>
			{
				Console.WriteLine(x);
			});

			// *** LAMBDA can be used anywhere you would
			// use an anonymous method or strongly type delegated type

			// ====================		
			// dissection lambda expressions
			// ====================		
			// - the parameters of a lambda expression can be explicitly or implicitly typed
			// - the compiler can determine the type based on the context and the underlying delegate
			// - if the lambda expression has a single parameter, then you can omit the parentheses
			List<int> en1 = (new List<int> { 1, 2, 3 }).FindAll((int i) => (i % 2) == 0);
			List<int> en2 = (new List<int> { 1, 2, 3 }).FindAll((i) => (i % 2) == 0);
			List<int> en3 = (new List<int> { 1, 2, 3 }).FindAll(i => (i % 2) == 0);

			// - processing withing multiple statements
			List<int> e1 = new List<int>();
			e1.AddRange(new int[] { 1, 2, 3, 4, 5 });
			List<int> re1 = e1.FindAll((i) =>
			{
				Console.WriteLine("Value of i is currently: {0}", i);
				bool isEven = ((i % 2) == 0);
				return isEven;
			});

		}

		static bool IsEventNumber(int i)
		{
			return (i % 2) == 0;
		}

		static void Print(int i) {
			Console.WriteLine("Integer {0}: ", i);
		}

	}

	// ====================
	// zero or more arguments
	// ====================
	public class SimpleMath
	{
		public delegate void MathMessage(string msg, int result);
		public delegate string VerySimpleDelegate();
		private MathMessage mmDelegate;
		public void SetMathHandler(MathMessage target)
		{
			mmDelegate = target;
		}

		public void Add(int x, int y)
		{
			if (mmDelegate != null)
			{
				mmDelegate.Invoke("Adding has Completed!", x + y);
			}
		}



		static void Main()
		{
			SimpleMath m = new SimpleMath();
			// using anonymous method along with method group conversion
			m.SetMathHandler((msg, result) =>
			{
				Console.WriteLine("Message: {0}, Result: {1}", msg, result);
			});
				// it is similar to 
				//m.SetMathHandler(new MathMessage(dummy));

			// This will execute the lambda expression
			m.Add(10, 10);

			// and zero parameters 
			VerySimpleDelegate v = new VerySimpleDelegate(() => "Enjoy your string!");
			VerySimpleDelegate v2 = new VerySimpleDelegate(() => { 
				Console.WriteLine("Enjoy your string!");
				return "";
			});
			v2();
		}

		static void dummy(string msg, int result)
		{
			Console.WriteLine("Message: {0}, Result: {1}", msg, result);
		}
	}
}
