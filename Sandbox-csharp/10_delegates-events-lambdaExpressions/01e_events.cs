﻿/*
 * What is the problem?
 *	- using delegates in the raw, can bring problems
 *		- not defining the delegate member as private which then the caller will have direct access to it
 *		  The caller can reassign the variable to a new delegate object which deletes the list of 
 *		  functions to call. Or worse, the caller can invoke the function list'.
 *		  Given this problem, it is alway a good PRACTICE to declare private delegate member variables.
	- creation of some boilerplate code
		- defining the delegate
		- declaring necessary member variables
		- creating custom registration and unregistration methods to preserver encapsulation
 * Solution ?
 *	- event keyword
 *	
 * What is the 'event' keyword?
 *	- when the compiler processes the event keyword
 *		- you are automatically provided with registration and unregistraiton methods (i don't have to do that anymore)
		, as well as any necessary member variables for the delegate types
 *		- these delegated member variables are ALWAYS DECLARED PRIVATE 
	- simplifies how a custom class sends out notifications to external objects
 *		 
 * How to create an event?
	1. define a delegate type that will hold the list of methods to be called when the event is fired
	2. declare an event (using the C# event) in terms of the related delegate type
 * 
 * How to listen for thos events?
 *	- C# events simplifies the act of registering the caller-side event handlers
 *	- use the += and -= to register or unregister
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evento
{
	// ===================
	// using event
	// ===================
	// - the car can now send CUSTOM EVENTS witout having
	// to define custom registration or declare delegate member variables.
	public class Car
	{
		public int CurrentSpeed { get; set; }
		public int MaxSpeed { get; set; }
		public string PetName { get; set; }

		private bool carIsDead;
		public Car() { MaxSpeed = 100; }
		public Car(string name, int maxSp, int currSp)
		{
			CurrentSpeed = currSp;
			MaxSpeed = maxSp;
			PetName = name;
		}

		// this deletate works in conjection with the Car's events
		public delegate void CarEngineHandler(string msg);

		// This car can send these events
		public event CarEngineHandler Exploted;
		public event CarEngineHandler AboutToBlow;

		public void Accelerate(int delta)
		{
			if (carIsDead)
			{
				if (Exploted != null) {
					Exploted("Sorry, this car is dead ...");
				}
			}
			else
			{
				CurrentSpeed += delta;
				
				// almost dead?
				if (10 == MaxSpeed - CurrentSpeed && AboutToBlow != null)
				{
					AboutToBlow("Careful buddy! Gonna blow!");
				}
				
				// still ok!
				if (CurrentSpeed >= MaxSpeed)
				{
					carIsDead = true;
				}
				else
				{
					Console.WriteLine("CurrentSpeed = {0}", CurrentSpeed);
				}
			}
		}
	}

	public class P
	{
		static void Main()
		{
			Car c1 = new Car("Slugbug", 100, 10);

			// registering
			c1.AboutToBlow += new Car.CarEngineHandler(CarIsAlmostDoomed);
				// using the method group conversion
				//c1.AboutToBlow += CarIsAlmostDoomed;
			c1.AboutToBlow += new Car.CarEngineHandler(CarIsAboutToBlow);

			Car.CarEngineHandler d = new Car.CarEngineHandler(CarExploted);
			c1.Exploted += d;

			// extra, using Visual studio, press tab after typing either += or -= to create a handler for that event
			// event handler, delegate target, callback, all are the same!!

			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

			// remove carexploted method from invocation list
			c1.Exploted -= d;

			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

		}

		public static void CarIsAboutToBlow(string msg)
		{
			Console.WriteLine(msg);
		}
		public static void CarIsAlmostDoomed(string msg)
		{
			Console.WriteLine("=> Cirtical MEssage from Car: {0}", msg);
		}
		public static void CarExploted(string msg)
		{
			Console.WriteLine(msg);
		}

	}

	// ===================
	// customer order 
	// ===================
	public delegate void Order<T>(T arg);
	class Coffee {}
	class CoffeeShop
	{
		public event Order<Coffee> SendOrders;
		public void Serve()
		{
			Console.WriteLine("Serving coffee");
			SendOrders(new Coffee());
		}

	}
	class Customer
	{
		public int Id { get; set; }

		public void ReceiveCoffee(Coffee c)
		{
			Console.WriteLine("{0} receives his cofree", this.Id);
		}
	}
	class RunMe
	{
		static void Main()
		{
			CoffeeShop shop = new CoffeeShop();

			List<Customer> cs = new List<Customer>() 
			{
				new Customer { Id = 1 },
				new Customer { Id = 2 },
			    new Customer { Id = 3 }
			};
			foreach (Customer customer in cs)
			{
				shop.SendOrders += customer.ReceiveCoffee;
			}
			shop.Serve();
		}
	}
}
