﻿/**
 * What?
 * - this allows to pass the name of a method to a method expecing a delegate type. If method format
 * don't match, then it would issue a compile error
 */
using System;
using System.Collections.Generic;
namespace Delegates
{
	class C
	{
		static void Main()
		{
			Car c1 = new Car();
			c1.RegisterWithCarEngine(CallMeHere);
				// - same as: 
				//c1.RegisterWithCarEngine(new Car.CarEngineHandler(CallMeHere));

			Console.WriteLine("**** speeding up *****");
			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

			c1.UnRegisterWithCarEngine(CallMeHere);

			Console.WriteLine("**** speeding up *****");
			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}
		}

		static void CallMeHere(string msg)
		{
			Console.WriteLine("=> Message from Car: {0}", msg);
		}
	}
}