﻿/**
 * What?
 *	- microsoft's recommended event pattern to use the System.EventARgs
 */

using System;
using System.Collections.Generic;
namespace CustomEventArgument
{
	// ============
	// simple
	// ============
	public class CarEventArgs: EventArgs
	{
		public readonly string msg;
		public CarEventArgs(string message)
		{
			msg = message;
		}
	}

	public class Car
	{
		public int CurrentSpeed { get; set; }
		public int MaxSpeed { get; set; }
		public string PetName { get; set; }

		private bool carIsDead;
		public Car() { MaxSpeed = 100; }
		public Car(string name, int maxSp, int currSp)
		{
			CurrentSpeed = currSp;
			MaxSpeed = maxSp;
			PetName = name;
		}

		// this deletate works in conjection with the Car's events
		public delegate void CarEngineHandler(object sender, CarEventArgs e);

		// This car can send these events
		public event CarEngineHandler Exploted;
		public event CarEngineHandler AboutToBlow;

		public void Accelerate(int delta)
		{
			if (carIsDead)
			{
				if (Exploted != null)
				{
					Exploted(this, new CarEventArgs("Sorry, this car is dead ..."));
				}
			}
			else
			{
				CurrentSpeed += delta;

				// almost dead?
				if (10 == MaxSpeed - CurrentSpeed && AboutToBlow != null)
				{
					AboutToBlow(this, new CarEventArgs("Careful buddy! Gonna blow!"));
				}

				// still ok!
				if (CurrentSpeed >= MaxSpeed)
				{
					carIsDead = true;
				}
				else
				{
					Console.WriteLine("CurrentSpeed = {0}", CurrentSpeed);
				}
			}
		}
	}

	class Program
	{
		static void Main()
		{
			Car c1 = new Car("Slugbug", 100, 10);
			c1.AboutToBlow += new Car.CarEngineHandler(CarIsAboutToBlow);
			Car.CarEngineHandler d = new Car.CarEngineHandler(CarExploted);
			c1.Exploted += d;

			// extra, using Visual studio, press tab after typing either += or -= to create a handler for that event
			// event handler, delegate target, callback, all are the same!!

			for (int i = 0; i < 6; i++)
			{
				c1.Accelerate(20);
			}

		}

		public static void CarIsAboutToBlow(object sender, CarEventArgs e)
		{
			Console.WriteLine(e.msg);
		}
		public static void CarExploted(object sender, CarEventArgs e)
		{
			Console.WriteLine(e.msg);
		}
	}
}
