﻿/**
 * What is the "partial types"?
 *	- the 'partial' keyword enables to parittion a class into the same file or multiple files as long
 *	as the types are idential and all of them are withing the same namspace. 
 * 
 * Why?
 *	- large projects, spreading over separated files neables multiple programmers to work on it
 *	at the same time
 *	- with atuomatically generated source, code can be added to the class without having to recreate
 *	the source file. Visual studio uses this approach when it creates Windows Forms, web service wrapper code, 
 *	and so on. 
 */
using System;
partial class EmployeeA
{
	public int Salary { get; set;  }
}

partial class EmployeeA
{
	public int DepartmentId { get; set; }
}

class ProgramE
{
	static void Main()
	{
		EmployeeA a1 = new EmployeeA() { Salary = 20, DepartmentId = 20};
		Console.WriteLine(a1.Salary);
		Console.WriteLine(a1.DepartmentId);
	}
}

