﻿/*
 * What is a class type?
 *  - the most fundamental programmign construct is the CLASS type
 *  - it is composed of members
 *      - field data (member variables)
 *          - should not be defined as public
 *      - constructors
 *      - properties
 *      - methods
 *      - events
 *      - etc.
 *  - remember that 'object' is used to describe an instance of a given class type created using the 'new' keyword
 *  
 * What does 'new' do?
 *  - it allocates the object into memory
 *  
 * What are constructors?
 *  - they are called indirectly when creating an object
 *  - it is a special method with no return type
 *  - they are default constructors
 *      - defauilt constructor
 *          - never takes arguments
 *          - after allocating the new object into memory, the default 'constructor'
 *          ensures that all field data of the class is set to an appropriate default value
 *      - custom default constructor
 *  - they are custom constructors
 *          - the default constructor is taken away when you define a custom constructor
 *          , so put back the default constructor, which will set all data members to default values
 *          
 * The role of 'this' keyword?
 *  - 'this' provides access to the current class instance
 *  - it used to resolve scope ambiquity which can arise when an incoming parameter
 *  is named identically to a data field of the class. 
 *      Class A {
 *          A(name) {
 *              this.name = name;   // oka
 *              name = name; // will not work, it will assing it sself
 *              Name = name;    // okay assuming that "Name" is a data field
 *          }
 *      }
 *  - can be used for chaining constructors
 */

// =================
// defining a class
// =================
class Car {
    // member variables which defines the state of the car
    public string petName;
    public int currSpeed;

    // methods, behavior
    public void PrintState() {
        System.Console.WriteLine("{0} is going {1} MPH.", petName, currSpeed);
    }

    public void SpeedUp(int delta) {
        currSpeed += delta;
    }

    // default constructor
}


// =================
// custom default constructor
// =================
class CarB {
    public string petName;
    public int currSpeed;

    // a custom default constructor
    public CarB() {
        petName = "Chuck";
        currSpeed = 10;
    }
}

// =================
// overload custom constructors
// =================
class CarC {
    public string petName;
    public int currSpeed;

    public CarC() {
        petName = "Check";
        currSpeed = 10;
    }
    public CarC(string pn) {
        petName = pn;
    }
    public CarC(string pn, int cs) {
        petName = pn;
        currSpeed = cs;
    }

}

// ===============
// chaning constructors
// ===============
// an example of redundant code
class Motorcyle {
    public int driverIntensity;
    public string driverName;
    public Motorcyle() { }
    // redundant constructor logic!
    public Motorcyle(int intensity) {
        if (intensity > 10) {
            intensity = 10;
        }
        this.driverIntensity = intensity;
    }
    public Motorcyle(int intensity, string name) {
        if (intensity > 10) {
            intensity = 10;
        }
        this.driverIntensity = intensity;
        this.driverName = name;
    }
}
// another approach is using methods
class MotorcyleB {
    public int driverIntensity;
    public string driverName;
    public MotorcyleB() { }
    // redundant constructor logic!
    public MotorcyleB(int intensity) {
        SetIntensity(intensity);
    }
    public MotorcyleB(int intensity, string name) {
        SetIntensity(intensity);
        this.driverName = name;
    }
    public void SetIntensity(int intensity) {
        if (intensity > 10) {
            intensity = 10;
        }
        this.driverIntensity = intensity;
    }
}
// chaining constructor using 'this'
class MotorcycleC {
    public int driverIntensity;
    public string driverName;
    public MotorcycleC() { }
    public MotorcycleC(int intensity) : this(intensity, "") { 
        // - after the master constructo has finished,
        // code here will be executed
    }
    public MotorcycleC(string name) : this(0, name) {
        // - after the master constructo has finished,
        // code here will be executed
    }

    // master constructor that does all the real work
    public MotorcycleC(int intensity, string name) {
        if (intensity > 10) {
            intensity = 10;
        }
        this.driverIntensity = intensity;
        this.driverName = name;
    }
}

// ===============
// using optional arguments instead of chaining
// ===============
class MotorcycleD {
    public string driverName;
    public int driverIntensity;
    public MotorcycleD(int intensity = 0, string name = "") {
        if (intensity > 10) {
            intensity = 10;
        }
        driverIntensity = intensity;
        driverName = name;
    }
}


// =================
// init
// =================
class Program {
    static void Main() {

        // =================
        // a simple object
        // =================
        Car myCar = new Car();
        myCar.petName = "Henry";
        myCar.currSpeed = 10;

        // speed up the car a few times and print out the new state
        for (int i = 0; i <= 10; i++) {
            myCar.SpeedUp(5);
            myCar.PrintState();
        }

        // =================
        // not initializating
        // =================
        //Car myCar4;
        //myCar4.petName = "Fred";
            // compile error 

        // =================
        // a diff way to initialize
        // =================
        Car myCar2 = new Car();
        myCar2.petName = "Fred";

        // =================
        // another to initialize
        // =================    
        Car myCar6;
        myCar6 = new Car();
        myCar6.petName = "Fred";

        // =================    
        // using a custom default constructor
        // =================    
        CarB chuck = new CarB();

        // =================    
        // overloadde constructors
        // =================    
        CarC c = new CarC("Martin", 2);

        // =================    
        // using class that is chained
        // =================    
        MotorcycleC c1 = new MotorcycleC("bob");

        // =================    
        // using optional arguments
        // =================    
        MotorcycleD d1 = new MotorcycleD();
        MotorcycleD d2 = new MotorcycleD(name: "Tiny");
        MotorcycleD d3 = new MotorcycleD(intensity: 10);
        MotorcycleD d4 = new MotorcycleD(10, "Tiny");

    }
}
