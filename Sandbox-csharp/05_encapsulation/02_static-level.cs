﻿/*
 * What is static?
 *  - the member in question must be invoked directly from the class level, rather than from an object reference variable
 *  - they are usually found in utility classes
 *      - a utility class is a class that does not maintain any object-level state
 *      and is not created with the 'new' keyword
 *  - they are called class-level members OR static members, unlike, member variables or data fields
 *  - the 'static' keyword can be applied to
 *      - data of a class
 *          - make sense, is shared by all objects of that type
 *      - methods of a class
 *          - make sense
 *      - properties of a class
 *          - works as expected
 *      - a constructor
 *          - is a special constructor that is an ideal place to initialize the values
 *          of static data when the value is not known at compile time (e.g. you need to read
 *          in the value from an external file, a database, etc.). 
 *          - CLR calls all static constructors before first use and never calls them again
 *          for that instance of the application
 *          - a class can only have one single static constructor
 *          - does nto use access modifiers and cannot take any parameters
 *          - executes exactly one time
 *          - the runtime invokes it when it creaets an instance of the class
 *          or before accessing the first static member invoked by the caller
 *          - executes before any instance level constructor
 * 
 *      - entire class definition
 *          - you cannot use 'new'
 *          - all members must be static
 * 
 */
using System;

namespace Sandbox_csharp._05_encapsulation {
    class _02_static_level {
    }
}
