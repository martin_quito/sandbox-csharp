﻿/*
 * What is it?
 *  - it is a shorthand notation for the syntax used to create a class
 * variable using a default constructor, and setting the state data property by property
 * - you can call on default constructors or custom constructors
*/
class Point {
    public int X { get; set; }
    public int Y { get; set; }

    public Point() { }
    public Point(int xVal, int yVal) {
        X = xVal;
        Y = yVal;
    }
}

class Rectangle {
    private Point topLeft = new Point();
    private Point bottomRight = new Point();

    public Point TopLeft {
        get { return topLeft; }
        set { topLeft = value; }
    }

    public Point BottomRight {
        get { return bottomRight; }
        set { bottomRight = value; }
    }
}

class ProgramD {
    static void Main() {
        // ===============
        // different ways to use it
        // ===============
        // make a point by setting each property manually
        Point firstPoint = new Point();
        firstPoint.X = 10;
        firstPoint.Y = 10;
        
        // or make a point via custom constructor
        Point secPoint = new Point(20, 20);

        // or make a point using object init syntax (calling the default constructor explicitly)
        Point thirdPoint = new Point() { X = 30, Y = 30 }
			;

        // or do the same as before but calling default constructor implicitly!
        Point fourthPoint = new Point { X = 30, Y = 30 };
        
        // or
        Point fifthPoint = new Point(10, 10) { X = 100, Y = 100 };
            // - x and y will be 100
            // you can creative with this 

        // ===============
        // initializing inner types
        // ===============
		Rectangle myRect = new Rectangle
		{
			TopLeft = new Point { X = 10, Y = 10 },
			BottomRight = new Point { X = 200, Y = 200 }
		};
    }
}