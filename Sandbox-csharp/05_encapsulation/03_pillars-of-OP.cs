﻿/*
 * What is encapsulation?
 *  - hiding the details of an object 
 *  - use access modifiers to preserve the integrity of an object's state data
 *  
 * What is inheritance?
 * - creating objects from other objects
 * - build new class definitions based on an existing class definitions
 * - this forms a "is-a" relationship
 * - "has-a" is a form of aggreation, a different subject
 * 
 * What is polymorphism?
 * - treates related objects in a similar manner
 * - allows base classes to define a SET of MEMBERS (formally termed as polymorphic interface) that are
 * available to all descendents. These members are build either as a VIRTUAL or abstract member
 *      - virtual member is a member in a base class that defines a default implementation
 *      that may be changed (or more formally speaking, overriden) by a dervied class
 *      - abstract member is a member in a base class that does not provide a default implementation, but
 *      does provide a signature. When a class derives from a base class defining an abstract method, it
 *      must be overriden by a derived type.
*/