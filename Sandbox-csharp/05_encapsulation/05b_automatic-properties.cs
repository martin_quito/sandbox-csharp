﻿/*
 * What are automatic propeties?
 *  - when no bussines rules are needed to access or mudate a data field and you want to use
 *  .net properties
 *  - to streamline the process of providing simple encapuslation of field data, you use 'automatic property syntax'
 *  - also note that it is NOT possible to build read only or write only automatic properties, you need both!
 *  - during compile time, the compiler will define the private backing field which means when defining the class
 *  or using the isntance of the class, you will need to do it through the properties to get to the underlying value
 *  - be aware about default values, the automatic properties will get assigned either a default value or null, to mitigate this is by
 *  updating the constructors to initialize defaults to those automatic instructors
*/

//using the standard proeprty (too verbose)
class CarD {
	private string carName = "";
	public string CarName {
		get { return carName; }
		set { carName = value; }
	}
}

class CarE {
	public string PetName { get; set; }
	public int Speed {get; set; }
	public string Color {get; set; }
	// use 'prop' syntax from visual studio if needed
}

