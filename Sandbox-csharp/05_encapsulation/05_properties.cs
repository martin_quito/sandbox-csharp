﻿/**
 * What are .NET properties?
 *  - the first character is capitalized
 *  - provides a simple way to define accessors and mutators
 *  - remember is a bad practice to let your internal data fields (member variables) be accessible
 *  plainly to the outsie would.
 * 
 * How?
 *  - the get and set don't use () parentheses
 *  - the type of the property needs to match as the private member
 *  - the 'value' token is not a C# keyword by a contextual keyword, when the token
 *  value is within the set scope of the property, it always represents the value being
 *  assigned by the caller and it will ALWAYS be the same underlying data type as 
 *  the property itself
 *  - they response find to instrunsic operators 
 *  - it is good practice to use properties throughtout a class implemetnation, to 
 *  ensure business rules are always enfored
 *  - to make a read-only or write-only properties, remove one othe other!
 */

using System;
class Employee {
	// Field data
	private string empName;
	private int empID;
	private float currPay;
	private int empAge;
	private int _ssn;

	public Employee() {}

	// this is a problem
	public Employee(string name, int age, int id, float pay) {
		if (name.Length > 15) {
			Console.WriteLine("Error");
		}
		else {
			this.empName = name;
		}
		this.empID = id;
		this.empAge = age;
        
	}
    
	// using properties instead
	public Employee(string name, int age, int id, float pay, bool dummy) {
		this.Name = name;
		this.Age = age;
		this.ID = id;
		this.Pay = pay;
	}

	// Properties
	public string Name {
		get { return empName; }
		set {
			if (value.Length > 15) {
				Console.WriteLine("Error");
			}
			else {
				empName = value;
			}
		}
	}
	public int ID {
		get { return empID; }
		set { empID = value;  }
	}
	public float Pay {
		get { return currPay; }
		set { currPay = value; }
	}
	public int Age {
		get { return empAge; }
		set { empAge = value; }
	}
	public int SSN
	{
		get { return _ssn; }
	}
}

class ProgramC {
	static void Main() {
		Employee joe = new Employee();
		joe.Name = "JOE";
		Console.WriteLine(joe.Name);
		joe.Age = 10;
		Console.WriteLine(--joe.Age);
	}
}

