﻿ /*
 * Why?
 *  - when working with encapsulation, you must always take into account which aspects of a type
 *  are viisble to various parts of your application
 *  
 * How?
 *  - by default, type members are IMPLICITLY PRIVATE and types are IMPLICITLY INTERNAL
 *  
 * C# Access modifiers?
 * public                   
 *  - type or type members            
 *  - has no restrictions
 *      - can be accessed from an object
 *      - can be accessed from any derived class
 *      - can be accssed from other external assemblies
 *      
 * private                  
 *  - type members or nested types
 *  - can only be accssed by the CLASS (OR STRUCTURE) that defines the item
 *  
 * protected                
 *  - type members or nested types
 *  - can only be accessed 
 *      - by the class which defines it
 *      - any child class (even if is in a different assembly)
 *      
 * internal                 
 *  - type members or type members
 *  - can only be accessed withing the same assembly
 * 
 * protected internal       
 *  - type members or nested types
 *  - can only be accessed withing the same assembly (includes
 *  declared class, other classes, derived class) and if is in another
 *  assembly, only the derived class
 * 
*/