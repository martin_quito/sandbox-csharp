﻿/**
 * What is a constant?
 *  - the 'const' keyword defines a constant data member where it can never change
 *  after the initial assignment regardless where the assignment gets located.
	- the intialize value assigned to the constant must be specified at the
time you define the constant!
	- these guys are IMPLICITY STATIC 
 *  
 * What is a read-only field?
 *  - similar to a const except that it can receive a value during runtime
 *  - you use the keyword 'read-only'
 *  - can only be assigned inside constructor but nowhere else
 *  
 * What is a static read-only field?
 *  - it works in a class level
	- read-only are not implciity static, if you want to expose this from a 
	class level, then make it static
	

Summary
	- const, read-only, statci are funny. Const are already static. Its value
	needs to be assgined when is declared. Readonly is like const but the value
	can be defined during runtime, only once though, inside a constructor. But the
	readonly value will work in an object-level. static readonly, it will work
	in a class level.
 */
using System;

namespace ConstData {
	// ============
	// const
	// ============
	//class MyMathClass {
	//	public const double PI = 3.14;
	//}

	//class Program {
	//	static void Main(string[] args) {
	//		// error can't change a constant
	//		//MyMathClass.PI = 3.1444;

	//	}
	//}

	// ============
	// read-only
	// ============
	class MyMathClass {
		public readonly double PI;

		public MyMathClass() {
			PI = 3.14;
		}


		static void Main() {

		}
	}

}