﻿/*
 *	
 * Basic C# query operator shorthand notation
 * -------
 *	from, in	=> define the backbone for any LINQ expression which allows you to extract a subset of data from a fitting container
 *	where		=> define restriction for which items to extract from a container
 *	select		=> select sequence from the containare
 *	join, on, equals, into				=> performs joins ased on specified key. (Remember that these joins do not need to have anything to do with data
 *										in a relational database
 *  orderby, ascending, descending		=> allows the resulting subset to be ordered in ASC or DESC
 *  group by							=> yields a subset with data group by a specified value
 *  
 * * Note that not all members in System.Linq.Enumerable class have a C# query operatorion shorthand notation (e.g. Union, Intersect, etc.)
 * 
 * What?
 *	- of course, linq querys are validated in compile time
 *	- the ordered is critical
 *	- every simple linq query is built from 'from', 'in', and 'select'
 *		
 *			var result = from matchingItem in container select matchingItem
 *			
 *			- matchingItem can be named anything, it reperesnts an item from the data
 *			- the item after 'in' operator represents the data container (array, colelction, xml, etc.)
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace LINQQueryOperator
{
	class ProductInfo
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public int NumberInStock { get; set; }

		public override string ToString()
		{
			return string.Format("Name={0}, Description1={1}, Number in Stock={2}",
			  Name, Description, NumberInStock);
		}
	}

	class P
	{
		static void Main()
		{
			// This array will be the basis of our testing...
			ProductInfo[] itemsInStock = new[] {
                new ProductInfo{ Name = "Mac's Coffee", 
                                 Description = "Coffee with TEETH", 
                                 NumberInStock = 24},
                new ProductInfo{ Name = "Milk Maid Milk", 
                                 Description = "Milk cow's love", 
                                 NumberInStock = 100},
                new ProductInfo{ Name = "Pure Silk Tofu", 
                                 Description = "Bland as Possible", 
                                 NumberInStock = 120},
                new ProductInfo{ Name = "Cruchy Pops", 
                                 Description = "Cheezy, peppery goodness", 
                                 NumberInStock = 2},
                new ProductInfo{ Name = "RipOff Water", 
                                 Description = "From the tap to your wallet", 
                                 NumberInStock = 100},
                new ProductInfo{ Name = "Classic Valpo Pizza", 
                                 Description = "Everyone loves pizza!", 
                                 NumberInStock = 73}
                };

			// something simple
			var result = from item in itemsInStock
						 select item;

			// same as before but extrat a field
			var result2 = from item in itemsInStock
						  select item.Name;

			// - if you want to obtain a sub set of data by filter
			// - you can use any valid C# operators to build complex expressions
			var resul3 = from item in itemsInStock
						 where item.NumberInStock > 50
						 select item.Name;
			resul3.ToList().ForEach(w => Console.WriteLine(w));

			Console.WriteLine("Projecting time ****");
			// projecting time
			// lets say you only want the name and description
			System.Array res = GetProjectedSubset(itemsInStock);
			foreach (object o in res)
			{
				Console.WriteLine(o);
				// - calls the ToString() on each anonympus object
			}

			// A couple of things to notice
			/*
			 * - I need to use the literal System.Array and cannot make
			 * a use of the C# array declaration syntax because
			 * I dont know the underlying type of the compile-generated
			 * anonymous class
			 * 
			 * - also note that I'm not specifying a type parameter to
			 * ToArray<T> since, once again, I dont know the underlying data type
			 * until compile tiem.
			 * 
			 * - the problem is that I lose any strong typing as eaech
			 * item in the Array object is assemed to b e a type object. 
			 * 
			 * - "Nevertheless, when you need to return a LINQ result set
			 * that is the result of a projection operation, transforming
			 * the data into an Array type (or another suitable container via
			 * other memebrs of the Enumerable type) is mandatory" (book1)
			 * 
			 */

			// obtainings counts using Enumerable
			var num = from i in itemsInStock
					  select i;
			Console.WriteLine(num.Count<ProductInfo>());
			Console.WriteLine(num.Count());

			// reversing result sets
			var allProducts = (from p in itemsInStock select p).Reverse();
			allProducts.ToList().ForEach(x => Console.WriteLine(x.Name));

			// ordering (default is asc)
			var res2 = from p in itemsInStock orderby p.Name select p;
			var res3 = from p in itemsInStock 
					   orderby p.Name descending 
					   select p;
			res2.ToList().ForEach(x => Console.WriteLine(x.Name));

			// ==============
			// LINQ as a better veen Diagramming tool
			// ==============
			// - the enumerable class provides a set of extension methods
			// that alllows for two or more LINQ queries as the basis to find
			// unions, differences, concatenations, and intersections of data
			Console.WriteLine("======== Veen Diagraming tool ");
			List<string> myCars = new List<string> {"Yugo", "Aztec", "BMW" };
			List<string> myCars2 = new List<string>(){ "BMW", "Saab", "Aztec" };
			var m1 = (from c in myCars select c);
			var m2 = (from c2 in myCars2 select c2);

			// except
			var carDiff = m1.Except(m2);
			carDiff.ToList().ForEach(x =>
			{
				Console.WriteLine(x);
			});

			// intersect
			m1.Intersect(m2).ToList().ForEach(x => Console.WriteLine(x));

			// union
			Console.WriteLine("Union ===== ");
			m1.Union(m2).ToList().ForEach(x => Console.WriteLine(x));
				
			// contact
			m1.Concat(m2).ToList().ForEach(x => Console.WriteLine(x));

			// concat (without duplicates)
			Console.WriteLine("Concat ===== ");
			m1.Concat(m2).Distinct().ToList().ForEach(x => Console.WriteLine(x));
		}

		static Array GetProjectedSubset(ProductInfo[] products)
		{
			var nameDesc = from p in products
						   select p.Name;
			// does not work
			//var nameDesc2 = from p in products
			//			   select p.Name, p.Description;
			var nameDesc2 = from p in products
							select new { p.Name, p.Description };
			
			// you have no way in knowing the type until it gets compile,
			// and since you can't have 'var' as the return type, 
			// you can try this method
			return nameDesc2.ToArray();
		}

		static IEnumerable GetProjectedSubset2(ProductInfo[] products)
		{
			var nameDesc = from p in products
							select new { p.Name, p.Description };
			return nameDesc;
		}
		
	}
}
