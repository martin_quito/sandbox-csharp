﻿/*
 * What?
 *	- the C# compiles C# linq operators into calls on
 *	methods of the Enumerable class
 *	- so why LINQ query operators? It is there to build query expressions
 *	and letting the compiler handle all the small details when calling the
 *	Enumerable methods. Using the LINQ query operators is the most common
 *	and straightforward approach
 *	
 * How LINQ is represented under covers? (qe = query expressions)
 * "
 *	- query expressions are created using various C# operators
 *	- query operators are simply shorthand notations for invoking extensions methds defined by the System.Linq.Enumerable type
 *	- many methods of Enumerable require delegates (Func <> in particular) as parameters
 *	- any method requiring a delegate parameter can instead be passed a lambda expression
 *	- lambda expressions are simply anonymous methods in disguise (which greatly improve readability)
 *	- anonymous methods are shorthand notations for allocating a raw delegate and manually building a delegate target method
 * " (method)
 */
using System;
using System.Collections.Generic;
using System.Linq;
namespace InternalLINQ
{
	// I don't have to have T2
	public delegate T2 FuncPO<T1, T2>(T1 arg1);
	static class ExtensionsPo
	{
		
		public static System.Collections.Generic.IEnumerable<TSource> WherePo<TSource> (this System.Collections.Generic.IEnumerable<TSource> source,
																			            FuncPO<TSource, bool> del)
		{
			// the delegate will have the function, which when I need to invoke it
			List<TSource> newList = new List<TSource>();
			foreach(var i in source) {
				bool res = del.Invoke(i) == true;
				if (res)
				{
					newList.Add(i);
				}
			}
			return newList;
		}
	}

	class P {

		public delegate bool FindMartin(string name);
		public delegate bool FindMartin<T>(T name);

		static void Main()
		{
			// ==================
			// try all methods that I have learned (using the where member in the Enumerable class)
			// building my own where clause
			// ==================
			string[] names = { "Martin", "Nefi", "Miriam Quito", "Moises", "Catherine"};

			// building query expression using the enumerable type and raw delegates
			FuncPO<string, bool> del = new FuncPO<string, bool>(Meow);
			List<string> res = names.WherePo(del).ToList();
			res.ForEach(x =>
			{
				Console.WriteLine(x);
			});
			
			// building query expressions using the Enumerable type and Anonymous methods
			List<string> res2 = names.WherePo(delegate(string name)
			{
				return name == "Catherine";
			}).ToList<string>();
			res2.ForEach(x => {Console.WriteLine(x);});

			// building query expressions using the Enumerable type and lambda expresison
			names.WherePo(x => x == "Catherine").ToList().ForEach(x => Console.WriteLine(x));
		}

		public static bool Meow(string name) {
			return name == "Catherine";
		}
		
	}
}
