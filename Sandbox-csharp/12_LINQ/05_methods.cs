﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox_csharp._12_LINQ
{
    public class Subject {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int ClientId { get; set; }
    }

    public class Person {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Address HomeAddress { get; set; }
        public bool IsApproved { get; set; }
        public Role Role { get; set; }
        public string Gender { get; set; }
    }

    public class Address {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public enum Role {
        Admin,
        User,
        Guest
    }

	public class Methods
	{
 
		static void Main() 
        {
            // =====================
            // Data
            // =====================
            Person[] personData = {
            new Person {FirstName = "Adam", LastName = "FreeMan", Role = Role.Admin},
            new Person {FirstName = "Jackqui", LastName = "Griffyh", Role = Role.User},
            new Person {FirstName = "John", LastName = "Smith", Role = Role.User},
            new Person {FirstName = "Anne", LastName = "Jones", Role = Role.Guest},
            };

            // subject and an array of clientIds
            List<Subject> data = new List<Subject>()
            {
                new Subject(){Id = 1, GroupId = 1, ClientId = 1},
                new Subject(){Id = 2, GroupId = 1, ClientId = 2},				
                new Subject(){Id = 3, GroupId = 2, ClientId = 1},				
                new Subject(){Id = 4, GroupId = 3, ClientId = 1},				
                new Subject(){Id = 5, GroupId = 3, ClientId = 2},				
                new Subject(){Id = 6, GroupId = 3, ClientId = 3},				
            };

            // =====================
            // Group BY
            // =====================
		    /*
			    GroupId: 1 [1,2]
			    GroupId: 2 [1]
			    GroupId: 3 [1,2,3]
		    */
		    var res = (from s in data
					    group s by s.GroupId into x
					    select x).ToList();

		    Console.WriteLine("A");

            // =====================
            // First
            // =====================
            /*
            - public static TSource First<TSource> (this System.Collection.Generic.Ienumerable<TSource> source);
            - returns the first element of a sequence
            */
            Console.WriteLine("=== First ===");
            Console.WriteLine(personData.First().FirstName);

		}
    }


}
