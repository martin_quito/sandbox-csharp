﻿/*
 * What?
 *	- Language Integrated Query (LINQ) technology
 *	- started in .NET 3.5
 *	- provides a concice, symmetrical, and strongly typed manner to access a wide variety of
 *	data stores
 *	
 * How?
 *	- when LINQ came out, it came out features to support LINQ
 *		- implicitly type local variables
 *		- object/collection initialization syntax
 *		- lambda expressions
 *		- extensions methods
 *		- anonymous types
 * Why?
 * 
 * 
 */
using System;
using System.Collections.Generic;

namespace LINQ
{
	class Person {
		public string Name { get; set; }
	}
	class P
	{
		static void Main()
		{
			// ===========
			// Implicitly typeing of local variables
			// ===========
			// - it will become a strong type after 
			// the compiler determines the underlying type
			// - this will help with LINQ because many LINQ
			// queries will return a sequence of data types which
			// are not known until compile time.
			var myInt = 0;
			var myBool = true;
			var myString = "Time, marches on ...";
			Console.WriteLine(myInt.GetType());
			Console.WriteLine(myBool.GetType());
			Console.WriteLine(myString.GetType());

			// ===========
			// object and collection initialization syntax
			// ===========
			// - they are not required but if doing so, it results in
			// a more compact code
			List<Person> p = new List<Person>
			{
				new Person {Name = "Martin"},
				new Person {Name = "Bob"},
				new Person {Name = "Amber"},
			};

			// ===========
			// lambda expresions
			// ===========
			// - simplify how to work with .NET delegates (when assingn handlers)
			// in that they reduce the amount of code you have to author by hand
			List<int> list = new List<int>();
			list.AddRange(new int[] { 1, 2, 3, 4, 5, 6 });
			List<int> evenNumbers = list.FindAll(x => (x % 2) == 0);
			evenNumbers.ForEach(x =>
			{
				Console.WriteLine(x);
			});

			// ===========
			// extension methods
			// ===========
			// - allow to add methods to existing classes without
			// the need to subclass or modify the existing class
			// - when wokring with LINQ, you will seldom be required
			// to manually build your own extension method. However
			// LINQ QUERY EXPRESSIONS will actually
			// be making use of NUMEROUS EXTENSIONS methods ALREADY DEFINED
			// by Microsoft. 

			// ===========
			// Anonymous types
			// ===========
			// - create a custom type on the fly
			// - compiler generates a new class definition using value-based semantics
			// - each virtual method of System.Object will be overriden accordingly
			var purchaseItem = new
			{
				TimeBought = DateTime.Now,
				ItemBought = new { Color = "Red", Make = "Saab" },
				Price = 34.000
			};

			// - LINQ reuires to create custom data types on the fly when you watn to
			// project new forms of data on the fly. For example, assume you have a collection
			// of Person objects, and want to use LINQ to ontain information on the age and Socia
			// l Number number of each. Using a LINQ projection, you can allow the compiler
			// to generate a new anonymous type that contains your info

		}
	}
}
