﻿/**
 * Why?
 *	- data is everywhere and it takes a low of work for developers to manipulate data.
 *	Before LINQ came out, develoeprs weer using other APIS to achieve that. However, the problem
 *	was that each API is an island it self offering veri little intergration between them. True, it is possible
 *	for example to save an ADO.NET Dataset as XML, and then manopulate it via System.Xml namespace,
 *	but nontheless, data manipulation remained asymmetrical. LINQ API provides a
 *	"consistent, symmetrical manner in which programmers can obtain and manipulate "data"
 *	in the broad sense of the term" (book 1) "LINQ is the term used to describe this
 *	overall approach to data access" (book1)
 *	
 * What?
 *	- you create "query expressions"
 *		- these guys can be u sed to interact with numerous types of
 *		data 
 *			- LINQ to Objects
 *			- LINQ to XML
 *			- LINQ to Dataset
 *			- LINQ to Entities
 *			- Parallel LINQ
 *	- are strongly typed
 *	-
 *	How?
 *		- core LINQ-Centri assemblies
 *			- System.Core.dll
 *			- System.Data.DataSetExtensions.dll
 *			- System.Xml.Linq.dll
 *		- make sure you that you import System.Linq namspace which is primarily defined
 *		wiithing System.Core.dll
*/
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Linki {
	// ===============
	// getting started
	// ===============
	class P {
		static void Main() {
			// ===============
			// LINQ queries to primitive arrays
			// ===============
			string[] currentVideoGames = {"Morrowind", "Uncharted 2", "Fallout 3",
										 "Daxter", "System Shock 2"};

			// contain data with embedded blank space
			IEnumerable<string> subset = from g in currentVideoGames
										 where g.Contains(" ")
										 orderby g
										 select g;

				// - g can be anything, it acts like an alias to the table
			foreach (string i in subset)
			{
				Console.WriteLine(i);
			}

			// - doing it manually, will require some extra work and is verbose, but its possible
			// - so LINQ is never mandatory

			// ===============
			// reflecing (use previous)
			// ===============
			Console.WriteLine(subset.GetType().Name);			
				// => OrderedEnumerable`2
			Console.WriteLine(subset.GetType().Assembly.GetName().Name);
				// => System Core

			// ===============
			// linq and implicitly typed local variables
			// ===============
			// As you saw above, the underlying type is OrderedEnumerable`2
			// which implements the IEnumerable interface. Many times,
			// it will be tedious to find the underlying type and sometimes
			// it will not be directly accessible from your code base because
			// in some cases, the type will be gnereated at compile time.
			// So, as a rule of thumb, you will always want to
			// make use of implicity typing when capturing the results of 
			// a LINQ query.

			// ===============
			// extensions
			// ===============
			// When using linq query expressions, Im using extensions methods.
			// For example, System.Array does not direclty implement the Ienumerable<T> interface
			// It gains it via the static System.Linq.Enumerable class type

			// ===============
			// role of derred execution
			// ===============
			// - query expressions are NOT evaluated until you iterate over the sequence
			int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
			var subset2 = from i in numbers where i < 10 select i;

			// linq evlauated
			foreach (var i in subset2)
			{
				Console.WriteLine("{0} < 10", i);
			}
			// change
			numbers[0] = 4;
			Console.WriteLine("Change===");
			foreach (var i in subset2)
			{
				Console.WriteLine("{0} < 10", i);
			}
			// as you can see, you can apply the same LINQ query multiple times
			// to the same conatiner and rest assured you are obtaining the latest
			// and greatest results.

			// ===============
			// role of immediate execution
			// ===============
			// when I need to evaluate a LINQ epxression from outside the foreaech logic
			// , I can call any number of extensions methods defined by the Enumerable type
			// as 
			//	ToArray<T>()
			//  ToDictionary<TSource, TKey>
			//  ToList<T>()
			// these guys will cause the LINQ to execute in the exact momemnt you call them!
			int[] my = { 10, 20, 30, 40, 1, 2, 3, 8 };
			int[] a3 = (from i in my where i < 10 select i).ToArray<int>();
			List<int> a2 = (from i in my where i < 10 select i).ToList();
			int[] a4 = (from i in my where i < 10 select i).ToArray();		
				// C# compiler can unambiguously determine the type
		}
	}


	// ===============
	// returning the result of a LINQ query
	// ===============
	class LINQBasedFieldAreClunky
	{
		private static int[] nums = { 1, 2, 3, 4 , 12};

		// - I can't use var since cannot be used for fields
		// - also, the TARGET of the LINQ query cannot be instance-level data; therefore, it must be static
		public  IEnumerable<int> subset = from g in nums
										  where g < 10
										  select g;
	}

	class P2
	{
		static void Main()
		{

			// you will seldom do this
			(new LINQBasedFieldAreClunky()).subset.ToList().ForEach(x =>
			{
				Console.WriteLine(x);
			});

			// - remember often I will define LINQ
			// queries withing the scope of a method or property
			// - Also remember the limetations of impleicity type local variables (var)
			// cannot be used to 
			//		- define parameters
			//		- return values
			//		- fields of a class or structure

			// the question then is, how can I return a query result
			// to an externall caller. The answer depends, if I'm returning
			// a custom strongly type data, such  an array or sttrings or
			// a List<T> of Cars, I can use the interface of
			// IEnumerable<T> or IEnumerable
			GetStringSubset().ToList().ForEach(x =>
			{
				Console.WriteLine(x);
			});
		}

		static IEnumerable<string> GetStringSubset()
		{
			string[] colors = { "Light Red", "Green", "Yellow", "Dark Red", "Red", "Purplse" };
			var theRedColors = from c in colors
							   where c.Contains("Red")
							   select c;
			// you can also opt exusting the linq query in the method
			// and returning the result. This means that the caller
			// will unware that their result came from a LINQ query and simply
			// work wit that is returning
			return theRedColors;
		}
	}

	// ===============
	// applying LINQ Queries to Collection Objects
	// ===============
	class Car
	{
		public string PetName { get; set; }
		public string Color { get; set; }
		public int Speed { get; set; }
		public string Make { get; set; }
	}

	class P3
	{
		static void Main()
		{
			List<Car> myCars = new List<Car>()
			{
				new Car {PetName = "Henry", Color = "Silver", Speed = 100, Make = "BMW"},
				new Car {PetName = "Diasy", Color = "Tan", Speed = 90, Make = "BMW"},
				new Car {PetName = "Mary", Color = "Black", Speed = 55, Make = "VW"},
				new Car {PetName = "Clunky", Color = "Rust", Speed = 5, Make = "Yugo"},
				new Car {PetName = "Melvin", Color = "White", Speed = 43, Make = "Ford"},
			};
			// there is really no different in using LINQ query to a generic collection
			var fastCars = from c in myCars
						   where c.Speed > 55
						   select c;
			fastCars.ToList<Car>().ForEach(x =>
			{
				Console.WriteLine(x.PetName);
			});
		}
	}

	// ===============
	// applying LINQ queries to nongeneric collections
	// ===============
	// using car from previous
	/*
		- LINQ works with any type that implements IEnumerable<T> (not IEnumerable) (either
		directly or via extension methods) System.Array was extended to have
		the necessary infrastructure to be used with LINQ (System.Array didn't implement IEnumerable<T>).
		System.Array INDIRECTLY gains the required functionalily of this type as well as many
		other LINQ centric members via the static System.Linq.Enumerable class type.

		But, the legacy nongeneric collection
		types have not. Thankfully, it s possible to use LINQ with those legacy types
		by using Enumerable.OfType<> extension method
	 
		OfType<TResult>(this System.Collections.IEnumerable)
		- see does not extend generic types
	 
	 */
	class P4
	{
		static void Main()
		{
			ArrayList myCars = new ArrayList()
			{
				new Car {PetName = "Henry", Color = "Silver", Speed = 100, Make = "BMW"},
				new Car {PetName = "Diasy", Color = "Tan", Speed = 90, Make = "BMW"},
				new Car {PetName = "Mary", Color = "Black", Speed = 55, Make = "VW"},
				new Car {PetName = "Clunky", Color = "Rust", Speed = 5, Make = "Yugo"},
				new Car {PetName = "Melvin", Color = "White", Speed = 43, Make = "Ford"},
			};
			
			// when this method is called against a type that implements "IEnumerable"
			// , I simply specify the type of the container it holds to extract
			// a compatible IEnumerable<T>
			var myCarsEnum = myCars.OfType<Car>();
			var fastCars = from c in myCarsEnum
						   where c.Speed > 55
						   select c;
			foreach (var car in fastCars)
			{
				Console.WriteLine("{0} is going toofast!", car.PetName);
			}

			// the nice feature of filtering Data Using OfType<T>()
			// is that if the container contains different ypes than the
			// type parameter, it will filter them out!
			
		}
	}

}