﻿/*
 * What?
 *	- using the 'abstract' keyword, it prevents to instantiate the class. This is
 *	because becase classes (abstract or not) are very useful in that they contain all of the common data and functionality of dervied types. 
 *	- an abstract class can impleemt abstract members (abtrasct or virtual). An 'abstract' member does not need implemtnation but its dervied type
 *	needs to provide the implemetnation
 * 
 * 
*/
namespace Shapes1
{
	abstract class Shape
	{
		public string PetName { get; set; }
		public Shape(string name = "NoName")
		{
			this.PetName = name;
		}

		// vritual
		public virtual void Draw()
		{
			System.Console.WriteLine("Inside Shape.Draw()");
		}

		// abstract (forces)
		public abstract void Paint();
	}

	class Circle : Shape
	{
		public Circle() { }
		public Circle(string name) : base(name) { }
		public override void Paint()
		{
			System.Console.WriteLine("Drawing {0} the Circle", PetName);
		}
	}

	class Hexagon : Shape
	{
		public Hexagon() { }
		public Hexagon(string name) : base(name) { }
		public override void Draw()
		{
			System.Console.WriteLine("Drawing {0} the HExagon", PetName);
		}
		public override void Paint()
		{
			System.Console.WriteLine("Drawing {0} the HExagon", PetName);
		}
	}

	class Program
	{
		static void Main()
		{
			// ==============
			// virtual
			// ==============
			Hexagon hex = new Hexagon("beth");
			hex.Draw(); // callls the chld
			Circle cir = new Circle("CIndy");
			cir.Draw(); // calls the base

			// ==============
			// abstract
			// ==============
			Shape[] myShapes = {
				new Hexagon(),
				new Circle(),
				new Hexagon("Mick"),
				new Circle("Beth"),
				new Hexagon("Linda")
			};
			foreach (Shape s in myShapes)
			{
				s.Paint();
			}
			// - this last section illustrates polymorphisam at its finest. Although it is not possible to 
			// directly create an instance of an abstract base class(myShape), you are able to freely
			// store references to any subclass with an abstract ase variable.

			// - given that all items in the myShapes array do indded dervive from Shape, you know they all 
			// SUPPORT the same "polymorphic interface" 

			// - remember that virual and abstract are considered formally as polymorphic interface
		}
	}
}