﻿/**
 * What?
 *	- use the 'base' keyword
 *	- use 'protected' when you need to. Normally, you will mostly used it on methods than on data fields
 *	- a standard extend to the parent class will execute the parent's base constructor
 * 
 */
using System;

namespace Employees
{
	class Employee
	{
		// Field data
		private string empName;
		private int empID;
		private float currPay;
		private int empAge;
		public readonly string _ssn;

		public Employee() { }

		// using properties instead
		public Employee(string name, int age, int id, float pay, string ssn)
		{
			this.Name = name;
			this.Age = age;
			this.ID = id;
			this.Pay = pay;

			// this is ok!
			this._ssn = ssn;
		}

		// Properties
		public string Name
		{
			get { return empName; }
			set
			{
				if (value.Length > 15)
				{
					Console.WriteLine("Error");
				}
				else
				{
					empName = value;
				}
			}
		}
		public int ID
		{
			get { return empID; }
			set { empID = value; }
		}
		public float Pay
		{
			get { return currPay; }
			set { currPay = value; }
		}
		public int Age
		{
			get { return empAge; }
			set { empAge = value; }
		}
		
	}

	class Manager : Employee
	{
		public int StockOptions { get; set; }

		// inefficient approach
		//public Manager(string fullName, int age, int empID, float currPay, string ssn, int numOfOpts)
		//{
		//	// this property is defined by the Manager class
		//	this.StockOptions = numOfOpts;
		//	this.ID = empID;
		//	this.Age = age;
		//	this.Name = fullName;
		//	this.Pay = currPay;

		//	// OOPS! This would be a compile error,
		//	// if the SSN property were read-only!
		//	//this._ssn = ssn;
		//}

		public Manager() {}

		// effective approach
		public Manager(string fullName, int age, int empID, float currPay, string ssn, int numOfOpts)
			: base(fullName, age, empID, currPay, ssn)
		{
			this.StockOptions = numOfOpts;
		}
	}

	class SalesPerson : Employee
	{
		public int SalesNumber { get; set; }
	}

	class ProgramA1
	{
		static void Main()
		{
			// ===============
			// subclass
			// ===============
			SalesPerson fred = new SalesPerson();
			fred.Name = "fred";
			fred.Age = 31;
			fred.SalesNumber = 50;

			Manager spon = new Manager();
			spon.Name = "Spongebob";
			spon.Age = 15;

			//Console.WriteLine("Are base class instances the same: {0}", fred.);
			// I believe that the base object and the dervied child is one
			// whole instance in the heap.
		}
	}

}