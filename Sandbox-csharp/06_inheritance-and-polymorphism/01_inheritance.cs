﻿/**
 * What is inheritance?
 *	- is a 'is-a' relatinship
 *	- builds a strong depenndency
 *	- enables code reuse
 *	- the idea is to create new classes from existing classes
 *  - The existing class that will server as the bases is termed a 'base' or 'parent' class. 
 *  - its role is to define all the common data and members for the classes that extend it
 *  - the extending classes are termed 'derived' or 'child' classes
 *  - remember that inheritance preserves encapsulation (access modifiers)!!!
 *  - C# demands that a given class have exactly on direct base class; however, you can have it 
 *  extend multiple interfaces
 *  - C# structures are always implicitly sealed. Structures can only be used to model stand alone, atomic
 *  , user-defined data types. 
 *  
 * 
 * What is the 'sealed' keyword?
 * - prevents inheritance from occurring
 * 
 * 
 */
using System;
class CarF
{
	public readonly int MaxSpeed;
	private int currSpeed;

	// a good use of readonly where it works like a constant and its value
	// can be received during runtime
	public CarF(int max)
	{
		MaxSpeed = max;
	}

	public CarF()
	{
		MaxSpeed = 55;
	}

	public int Speed
	{
		get { return currSpeed; }
		set
		{
			currSpeed = value;
			if (currSpeed > MaxSpeed)
			{
				currSpeed = MaxSpeed;
			}
		}
	}
}

class MiniVanF : CarF
{
	// this will not work because currSpeed is private
	public void TestMethod()
	{
		// OK! Can access public memebers
		// of a parent within a derived type
		Speed = 10;

		// Error! Cannot access private memebers
		//currSpeed = 10;
	}
}

class Program6 {
	static void Main()
	{
		// ================
		// parent class!
		// ================
		CarF myCar = new CarF(80);
		myCar.Speed = 50;

		// ================
		// child class
		// ================
		MiniVanF myVan = new MiniVanF();
		myVan.Speed = 10;
	
	}
}