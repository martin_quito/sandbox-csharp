﻿/**
 * What?
 *  - all types dervied from System.Object (directly or indirectly)
 *  - some members in the System.Object
 *       
 *      public virtual bool Equals(object obj); 
 *          - "by default, this method returns true only if the items being
 *          compared refer to the exact same item in memory. Thus, Equals()
 *          is used to compare object references, not the state of the object." (book 1)
 *          - "Typically this method is overriden to return true
 *          only if the objects being compared have the same internal state values
 *          (taht is, value-based semantics)" (book 1)
 *          - "Be aware that if you override Equals(), you should also override
 *          GetHashCode(), as these methods are used internally by
 *          Hashtable types to retrieve subobjects from the container"
 *          - "Also recall from Chapter 4, that the ValueType class overrides
 *          this method for all structures, so they work with value-based comparisons"
 *          
 *      protected virtual void Finalize();
 *          - is called to free any allocated resources before the object is destroyed
 *          
 *      public virtual int GetHashCode();
 *          - returns an INT that identifies a specific obhject instance
 *          
 *      public virtual string ToString();
 *          - termed as the "Fully qualified name"
 *      
 *      public Type GetType()
 *          - this is a Runtime Type identification (RTTI) method available
 *          to all objects
 *          
 *      protected object MemberwiseClone(0
 *          - return a member by member copy of the current object (which is often
 *          used when cloning an object)
 *      
 *      public static bool Equals (object objA, object objB)
 *      public static bool ReferenceEquals (object objA, object objB);
 *      
 * 
 */
using System;
using System.Collections.Generic;

namespace SystemObjects{
    // ================
    // list of object memebrs
    // ================
    class Person {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Person(string fName, string lName, int personAge) {
            FirstName = fName;
            LastName = lName;
            Age = personAge;
        }
    }
    class P {
        static void Main() {
            Person p1 = new Person("Martin", "Quito", 12);
            Console.WriteLine(p1.ToString());
            Console.WriteLine(p1.GetHashCode());
            Console.WriteLine(p1.GetType());

            Person p2 = p1;
            object o = p2;

            // are the references pointing to the same object in Memory
            if (o.Equals(p1) && p2.Equals(o)) {
                Console.WriteLine("same instance");
            }
        }
    }

    // ================
    // overriding System.Object.ToString()
    // ================
    /*
     * - ToString() returns a string textual representation of the type's current state
     * - many classes (and structures) you create will benefit from overrirding ToString()
     * - how to construct is based on personal matter, but a common pattern is of
     * name/value pair with semicolons and wrap the entire string withing square brackets 
     */
    class PersonB {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public PersonB(string fName, string lName, int personAge) {
            FirstName = fName;
            LastName = lName;
            Age = personAge;
        }
        public PersonB() { }
        public override string ToString() {
            return string.Format("[First Name: {0}; Last Name: {1}; Age: {2}]", FirstName, LastName, Age);
        }
    }

    class P2 {
        static void Main() {
            PersonB p1 = new PersonB("Martin", "Quito", 12);
            Console.WriteLine(p1.ToString());
        }
    }

    // ================
    // overriding System.Object.Equals()
    // ================
    // - override the behavior of Object.Equals() to work with value based semantics
    class PersonC {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public PersonC(string fName, string lName, int personAge) {
            FirstName = fName;
            LastName = lName;
            Age = personAge;
        }
        public PersonC() { }
        public override string ToString() {
            return string.Format("[First Name: {0}; Last Name: {1}; Age: {2}]", FirstName, LastName, Age);
        }
        public override bool Equals(object obj) {
            if (obj is PersonC && obj != null) {
                PersonC temp;
                temp = (PersonC)obj;
                if (temp.FirstName == this.FirstName
                    && temp.LastName == this.LastName
                    && temp.Age == this.Age) {
                        return true;
                }
                else {
                    return false;
                }
            }
            return false;
        }
    }

    class P3 {
        static void Main() {
            PersonC p1 = new PersonC("Martin", "Quito", 12);
            PersonC p2 = new PersonC("Martin", "Quito", 12);
            PersonC p3 = new PersonC("Martin", "Quito", 1);
            Console.WriteLine(p1.Equals(p2)); // true
            Console.WriteLine(p1.Equals(p3)); // false

            // or you can use toString() to compare
        }
    }

    // ================
    // overriding System.Object.GetHashCode()
    // ================
    // - hash code in this context is a numerical value that represents an object
    // as a particular state. By default, it uses the object's current location
    // in memory to yield the hash value

    // - if you can idenfity a piece of field data on your class that should
    // be unique for all instances (such as SSN) simply call
    // GetHashCode() on that point of field data
    
    // - if you cannot find a single point of unique string data, but you have
    // overriden TOString(), call GetHashCode() on your own string representation

    // - when a class overrides the Equals() method, you should also
    // override the default implemetnation of GetHashCode()
}
