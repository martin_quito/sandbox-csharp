﻿/*
 * Rules for casting objects?
 *	- when two class types have a 'is-a' relationship, it is always safe to store
 * the dervied object withing a base class REFERENCE. This is know as "implicit cast"
 *	- in cases, you explicitly downcast using the C# casting operator 
 *		- (ClassIWantToCastTo) referenceIHave
 *		- EXPLICIT CASTING IS evaluated at runtime and NOT compile time which means
 *		an exception can happen where you will need to wrap it inside a try/catch block 
 *		
 * What is 'as' used for?
 *	- determines at runtime whether a given type is compatible with another
 *	- it returns 'null' if it fails or the casted object it is a success
 *	
 * What is 'is' used for?
 *	- evalations type compatibility at runtime. 
 *	- it returns 'false' if it fails or true 
 *	- note that this vs 'as' is that in 'is', you need to check the type using it
 *	and then if is true, cast it. Whereas, in 'as', it does both in one step. 
 *		- this could improve performance
 *	
*/
namespace polyCastingRules {
	class Animal
	{
		public string Walk { get; set; }
	}
	class Dog : Animal
	{
		public string Bark {get; set;}
	}

	class Labrador : Dog
	{
		public string Save {get; set;}
	}

	class Circle { }

	class Program
	{
		static void Main()
		{
			// ===========
			// implicit cast
			// ===========
			// a dog is an object
			object peludo = new Dog();

			// a dog is an animal
			Animal meow = new Dog();

			// a Labrador is a dog
			Dog l = new Labrador();

			// ===========
			// more implicit casting
			// ===========
			Resurrect(new Animal());
			Resurrect(new Dog());
			Resurrect(new Labrador());


			// ===========
			// explicit casting
			// ===========
			// how about this ?
			object peludo2 = new Dog();
			//Resurrect(peludo2); // not work
				// 'Argument 1: cannot convert from 'object' to 'polyCastingRules.Animal'
				// - any object hat is an 'Animal' will work but 'object' is not an 'Animal'...
				// - the compiler cannot know that until runtime, so to satisfy the compiler
				// is by performing an 'explicit' cast
			Resurrect((Animal)peludo2);

			//  - but remember that EXPLICIT CASTING happens in runtime not in compiling time
			//  - this will compile okay but will blow up during runtimes
			//Circle cr = (Circle)peludo2;

			// ===========
			// 'as'
			// ===========
			// use 'as' instead of wrapping withing a try/catch block
			Circle cr2 = peludo2 as Circle;
			if (cr2 == null)
			{
				System.Console.WriteLine("Sorry but this is not a circle");
			}

			// ===========
			// 'is'
			// ===========
			object peludo3 = new Dog();
			Speak((Animal)peludo3);
			// - speak performs a runtime check to determine what the incoming
			// base class reference is actually pointing to in MEMORY!!!!
			// - note I don't need to wrap my casting operation withing a try/catch
			// construct because the cast is safe since I gave a conditional check 'is'
		}

		static void Resurrect(Animal an)
		{
			System.Console.WriteLine("Animal was resurrected");
		}

		static void Speak(Animal an)
		{
			if (an is Dog)
			{
				System.Console.WriteLine(((Dog)an).Bark);
				System.Console.WriteLine("Dog speak");
			}

			if (an is Labrador)
			{
				System.Console.WriteLine(((Labrador)an).Save);
				System.Console.WriteLine("Labrador speak");
			}
			
			// or
			Dog d = an as Dog;
			if (d != null)
			{
				System.Console.WriteLine("Dog speak");
			}

			Labrador ld = an as Labrador;
			if (ld != null)
			{
				System.Console.WriteLine("Labrador speak");
			}

		}
	}
}