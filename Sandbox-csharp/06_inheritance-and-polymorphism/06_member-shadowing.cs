﻿/**
 * What?
 *	- if a derevied class defines a memberthat is identical to a member defined in a base class,
 *	the dervied class has shadowed the parent's version. 
 *	
 * What is 'new' keyword?
 */
namespace MemberShadowing
{
	class Circle
	{
		public void Draw()
		{
			System.Console.WriteLine("Base");
		}
	}

	class ThreeDCircle : Circle
	{
		public  void Draw()
		{
			System.Console.WriteLine("Drawing a 3D Circle");
		}
	}

	class Program
	{
		static void Main()
		{
			// ============
			// shadowing
			// ============
			ThreeDCircle t = new ThreeDCircle();
			t.Draw();
			// "Warning	1	'MemberShadowing.ThreeDCircle.Draw()' hides 
			//inherited member 'MemberShadowing.Circle.Draw()'. Use the 
			//new keyword if hiding was intended..."

			// - the solution would be either add 'new' to the dervied member
			// or update the parent to a virtual, but imagined that the
			// parent is from a third party which you don't have access to
			// so when using 'new' it explicitly states that the derivedd type's 
			// implementation is intentially designed to effectively ignore the parent's
			// version 
		}
	}
}