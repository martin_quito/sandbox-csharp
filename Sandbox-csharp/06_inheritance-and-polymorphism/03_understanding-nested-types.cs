﻿/*
 * What are nested types?
 *	- all 5 system types can be defined within a class or struct
 *	- the ensted or inner type is considered as a member of the nesteding or 'outer' class
 *	
 * Traits of nested types?
 *	- because a nested type is a mber of tha containng class, it can access private members of the containg class
 *	- often, a nested type is only useful as a helper for the outer class, and is not intendede for use by the outised world
*/

public class OuterClass
{
	// a private nested type can only be used by members
	private class PrivateInnerClass { }

	protected class ProtectedInnerClass { }

	internal class InternalInnerClass { }

	protected internal class ProtectedInteralInnerClass { }

	// a public nested type can be used by anybody
	public class PublicInnerClass { }
}

class ProgamA2
{
	static void Main()
	{
		OuterClass.PublicInnerClass inner = new OuterClass.PublicInnerClass();
		OuterClass.InternalInnerClass inner2 = new OuterClass.InternalInnerClass();
		OuterClass.ProtectedInteralInnerClass inner3 = new OuterClass.ProtectedInteralInnerClass();
	}
}