﻿/*
 * What?
 *	- polymorphisms provides a way for a child class
 *	to define its own version of a method defined by its base class by the process termed
 *	METHOD OVERRIDING. 
 *	
 * How?
 *	- if a base class wants to define a method that may be  (but does nto have to be) overridden by a child class
 *	, it must mark the method with the 'virtual' keyword. Once its done, the methods are not termed as "VIRTUAL METHODS"!
 *		- also, once the method is marked by 'overriden', that method can be overriden unless you place a 'sealed' keyword
*/

using System;
public class EmployeeC
{
	public double Pay { get; set; }

	public EmployeeC()
	{
		this.Pay = 1;
	}
	
	// this method can now be 'overridden' by a derived class
	public virtual void GiveBonus(float amount)
	{
		Pay += amount;
	}
}

public class Manager : EmployeeC 
{
	public int StockOptions { get; set; }
	public Manager ()
	{
		// here, pay is 1!!!
		//Console.WriteLine(Pay);
		Pay = 100;
	}

	public override void GiveBonus(float amount)
	{
		base.GiveBonus(amount);
		Random r = new Random();
		StockOptions += r.Next(500);
	}

}

public class SalesPerson : EmployeeC
{
	public int SalesNumber { get; set; }
	public SalesPerson() 
	{
		Pay = 50;
	}

	public override void GiveBonus(float amount)
	{
		int salesBonus = 0;
		if (SalesNumber >= 0 && SalesNumber <= 100)
		{
			salesBonus = 10;
		}
		else
		{
			if (SalesNumber >= 101 && SalesNumber <= 200)
			{
				salesBonus = 15;
			}
			else
			{
				salesBonus = 20;
			}
		}

		base.GiveBonus(amount * salesBonus);
	}
}

public class PTSalesPerson: SalesPerson
{
	// - doesn't work because the method is sealed! if
	// the keyword 'sealed' is used
	// - the method the will be be overwritten needs to be marked
	// with overrride, abstract, or virtual
	public override void GiveBonus(float amount) {
		Console.WriteLine("Give bonus to PTSales");
	}
}

class ProgramA4
{
	static void Main()
	{
		// ==============
		// using the base class
		// ==============
		Manager chucky = new Manager();
		chucky.GiveBonus(300);
		Console.WriteLine(chucky.Pay);

		SalesPerson fran = new SalesPerson();
		fran.GiveBonus(100);
		Console.WriteLine(fran.Pay);

		PTSalesPerson martin = new PTSalesPerson();
	}
}