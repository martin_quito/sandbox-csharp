﻿/**
 * What?
 *	- 'is a' and 'has a' are different. 'Has a' relationship is known as
 *	the 'containment/delegation' model or 'aggregation'. Delegation means
 *	'simply the act of adding public members to the containing class that make
 *	use of the contained object's functionality' (book1)
 * 
 */

class Animal {}
class Ball {
	public void roll() {}
}
class Cat : Animal
{
	private Ball _b = new Ball();
	
	// exprose object
	public Ball Ball
	{
		get { return _b; }
		set { _b = value; }
	}
	// expose certain behaviors
	public void Roll()
	{
		Ball.roll();
	}
}

