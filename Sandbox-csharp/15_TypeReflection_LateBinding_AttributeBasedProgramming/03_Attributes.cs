﻿/*
What?
    - one job of the .NET compiler is to generate metadata for all defined
    and referened types. This metadata is part of the assembly, and addition to
    it, .NET platform provides a way for programmers to embed additional 
    metadata into an assembly using ATTRIBUTES. In other words, attributes
    are annotations that can be applied to a given type (class, interface, etc.)
    , member (property, method, etc.) assembly, or module.

    - .net attributes extend abstract System.Attribute base class. 

    - understand that when you apply attributes in your code, the embedded metadata
    from the attributes is essentially USELESS until another piece of software
    explicitly reflects OVER the information. If this is not the case, the 
    embedded metadata is ignored and harmless. 

    - key points of .NET attributes
        - attributes are classes that derived from System.Attribute
        - attributs result in embedded metadata
        - attributes are basically useless UNTIL ANOTHER AGENT REFLECTS upon them
        - attributes are applied in C# using square brackets.
Why?


How?
    - see 1
    - see 2

Conclusion
    - it adds metadata to a type or member of a type for another agent to 
    reflect over. 
*/
using System;
using System.Collections.Generic;
using System.Linq;

namespace Attributes {
    // =============
    // 1 applying attributes
    // =============
    // This class can be saved to disk
    [Serializable]
    public class Motorcycle {
        // however, this fill will not be persisted
        [NonSerialized]
        float field;
    }

    class Program {
        static void Main() {

        }
    }

    // =============
    // 2 multiple atributes
    // =============
    [Serializable, Obsolete("Use another vehicle!")]
    public class HorseAndBuggy {

    }

    [Serializable] // SerializableAttribute
    [Obsolete("Use another vehicle!")]
    public class HorseAndBuggy2 {

    }
    /*
        - as a naming convention, all attribute names are suffixed "Attribute" token. To
        simplify the process of applying attributes, the C# does not require you to type
        the suffix. Be ware, that this is a courtesy provided by C#, not all .NET
        enabled languages support
        - Obsolte is actually the name of class when you view the formal definition
    */

    class ProgramB {
        static void Main() {
            HorseAndBuggy mule = new HorseAndBuggy();
                // -> 'Atttributes.HourseAndBuggy' is obsolete: 'Use another vehicle'

            /*
            - here the C# compiler is REFLECTING on the [Obsolete] attribute
            */
        }
    }
}
