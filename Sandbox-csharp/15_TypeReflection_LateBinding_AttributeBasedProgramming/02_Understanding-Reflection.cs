﻿/*
What?
	- its the process of runtime type discovery
	- to programmatically obtain the same metadata information displayed by ildasm.exe using a friendly object model
	- through reflection, you can obtain a list of all the types within an assembly including methods, fields, props, and events
	defined by a given type and much more.
	- sampling of members of the System.Reflection namespaces
	
	Assembly        - abstract, use to load and investigate and manipulate an assembly
    AssemblyName    - learn about assembly identity
    EventINfo       - abstract, holds ifnromation about for a given event
    FieldInfo       - abstract, holds info about a given field
    MemeberInfo     - abstract, that defines common behavior for the EventInfo
                       FieldINfo, MethodInfo, and PropertyInfo
    MethodIndo      - abstract, info of a given method
    Module          - ...
    ParameterInfo   - ...
    PropertyINfo    - ...


Why?
	- 
	
How?
	- understand the "System.Type"
		- You cannot use 'new' keytword to create a Type class because its 
        abstract. There are multiple ways in getting the type.  For example,
        System.Object, the method GetType(). This approach works ONLY IF 
        you have the COMPILE KNOWLEDGE of the types you wish to reflect over.
        With this, you will see that ildasm.exe does not obtain type info
        by directly calling the System.Object.GetType() because ildams.exe
        was not compiled against your custom assembly.
        See Example 2. Another way to get it is by using 'typeof', see example
        3. With this, you don't have to create an instance of the type to
        extract info about the type, unlike GetType(), where you have to. But
        this approach still suffers that you must still have compile-time
        knowledge of the type you are interested in examining, as "typeof"
        expects a strongly typed name of the type.
        
       - A more flexible manner to get the type is by using the "System.Type.
        GetType()" static method. This does not expect a strong type named of
        the type, it expects a string. See Example 4.
    - see "MYTypeViewer" in how to use reflection and usefulness of SYstem.Type

    - understanding Dynamically loading Assemblies
        - rememebr how the CLR consults the assembly manifest when probing 
        for an externally referenced assembly.  However, there will be 
        many times when I need to load an assembly on the fly programmatically
        even if there is no record of the targeted assembly in the manifest.
        This formally spealking, act of loading external assemblies on demand
        is called "dynamic load"

        You can dynamically load private and shared assemblies, as well
        load an assembly located at an arbitrary location. 

        See "ExternalAssemblyReflector"

    - understanding LATE BINDING
        - it is a technique where you can create an instance of a type
        and invoke its members AT RUNTIME WITHOUT having hard-coded-compile-time
        knowledge of its existence

        - see 4 for example

Conclusion
    - Refelection, late binding, and dynamic loading do all play together in 
    example 4. USe reflection to load an assembly during runtime and use late
    binnding (reflection) to get create an object and invoke its memebers
    during runtime and without having hard-coded compile-time knowledge. 
	
*/

using System;
using System.Reflection;

namespace TypeReflection {
    class Reflection 
    {
        static void Main() 
        {
            // ===============
            // 1 Type (Common methods)
            // ===============                   
            int[] list = { 1, 2, 3 };
            Console.WriteLine(list.GetType().IsArray);
            Console.WriteLine(list.GetType().IsArray);

            // ===============                   
            // 2 one way to get the type
            // ===============        
            int a = 2;
            Type at = a.GetType();


            // ===============        
            // 3 another way to get the type
            // ===============        
            int b = 2;
            Console.WriteLine(typeof(int));

            // ===============        
            // 4 System.Type.GetType()
            // ===============        
            // assemues the type is withing the currently executing assembly
            Type t = Type.GetType("int", false, true);
            Console.WriteLine(t);

            // obtain metadata for a type withing an external private assembly
            // the string parameter is formatted using type's fully qualified
            // name
            Type t2 = Type.GetType("CarLibrary.SportsCar, SampleClassLibrary-CarLibrary");
            Console.WriteLine(t2.ToString());
                // -> CarLibrary.SportsCar

            // to denote a nested type you can use (+)
            Type t3 = Type.GetType("CarLibrary.JamesBondCar+SpyOptions");

            // ===============                   
            // 4 Late binding
            // ===============    
            /*
                THE WHOLE POINT HERE IS TO CREATE AN OBJECT THAT IS NOT KNOWN
                AT COMPILE TIME (THAT IS LATE BINDING)
            */
            Assembly y = null;
            try {
                y = Assembly.Load("SampleClassLibrary-CarLibrary");
                
            }
            catch (Exception ex){
                Console.WriteLine(ex.Message);
            }
            if (y != null) {
                CreateUsingLateBinding(y);
            }

        }

        static void CreateUsingLateBinding(Assembly asm) {
            try {
                // get metadata for the MiniVan type
                Type minVan = asm.GetType("CarLibrary.MiniVan");

                // create the Minivan on the fly
                object obj = Activator.CreateInstance(minVan);
                    /*
                    - defined in the mscorlib.dll
                    - creates an instance of a type (late binding)
                    */
                Console.WriteLine("Created a {0} using late binding!", obj);

                // get ingo for turboboost
                MethodInfo mi = minVan.GetMethod("TurboBoost");

                // Invoke methd ('null' for no paramers)
                mi.Invoke(obj, null);


            } catch (Exception ex) {
                Console.WriteLine(ex.Message);        
            }
        }
    }
}