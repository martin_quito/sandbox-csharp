﻿/*
How?
    - see 1
    - see 2

Why

What?
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomAttributes {
    // ========
    // 1 creating
    // ========
    // create a new class deriving from System.Attribute
    public sealed class VehicleDescriptionAttribute : System.Attribute {
        public string Description { get; set; }
        public VehicleDescriptionAttribute(string vehicleDescription) {
            Description = vehicleDescription;
        }
        public VehicleDescriptionAttribute() { }
    }
    /*
        - it just contains a string "Description"
        - for security reasons, its good to have the all custom attributes
        as sealed
    */

    [VehicleDescription(Description = "My Rocketing Harley")]
    public class Motorcycle { }

    [VehicleDescription("The old gray mare, she ain't what she used to be ...")]
    public class HorseAndBuggy { }

    /*
        - in the Motorcycle, we are using the "named property" to assing the string
        to the prop
        - in the HourseAndBuggy, are not usign the "named property syntax" but
        is using the custom constructor
        - we can use ildasm.exe, as an agent, to reflect over the type, to find
        out the embedded metadata
    */

    // ========
    // 2 restricting attribute usage
    // ========
    [VehicleDescription("A very long, show, but feature rich auto")]
    public class Winnebago {
        [VehicleDescription("My rocking CD player")]
        public void PlayMusic(bool On) {

        }
    }
    // how to restrict the application of an atribute to a selection of code elements
    // or the scope of the custom attribute.

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
    public sealed class VehicleDescriptionAttribute2 : System.Attribute {
        public string Description { get; set; }
        public VehicleDescriptionAttribute2(string vehicleDescription) {
            Description = vehicleDescription;
        }
        public VehicleDescriptionAttribute2() { }
    }

    // - can only be applied to a struct or class, otherwise it is a compile error
    // - cannot be inhertied 


}
