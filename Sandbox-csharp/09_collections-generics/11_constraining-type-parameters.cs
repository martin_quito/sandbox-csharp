﻿/**
 * What is 'constraining type parameters'?
 *	- any generic item has at least one type parameter that you need to
 *	specify at the time you interact with the generic type or member which this alone
 *	helps you to build some type-safe code; however, the .NET platform
 *	allows you to use the 'where' keyword to get extremely specific about what a given type parameter must look like
 * 
 * How?
 *	where T : struct
 *		- the type parameter <T> must have System.ValueType in its chain of inhertiance (i.e. <T> must be structure)
 *	where T : class
 *		- the type parameter <T> must not have System.ValyeType in in its chain of inheritance
 *	where T : new()
 *		- the type parameter <T> Must have a default constructor. 
 *	where T : NameOfBaseClass
 *		- the type parameter <T> must be derived from the class specified by NameOfBaseClass
 *	where T : NameOfInterface
 *		- the type parameter <T> must IMPLEMENT the interface specified by NameOfINterface. Uou can 
 *		separate multiple interfaces as a commona delimited list
 *		
 * Remember that you can apply multiple constraints
 */
using System;
using System.Collections.Generic;
namespace ConstraintTypeParam {
	public class Draw { }

	// ================
	// derives from object, while contained items must have a defualt CTOR
	// ================
	public class MyGenericClass<T> where T : new()
	{
	}

	// ================
	// multiple constraints
	// ================
	public class MyGenericClass2<T> where T : class, IEnumerable<object>, new()
	{

	}

	// the 'new ()' should always be last; otherwise, a compile error occurs

	// ================
	// multiple type parametners
	// ================
	public class MyGenericClass3 <K, T> where K : Draw, new() where T: struct, IEnumerable<int> {

		// type can only be a struct
		static void Swap<T>(ref T a, ref T b) where T : struct
		{

		}

	}

}
