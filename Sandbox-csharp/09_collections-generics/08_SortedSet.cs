﻿/*
 * What?
 *	- ensures that the items in the set are sorted when you insert or remove items. However, yo 
 *	do need to inform the SortSet<T> class exactly HOW you want it to
 *	sort the objects BY passing in as a constructor argument an object that 
 *	implements the generic IComparer<T> interface
 */
using System;
using System.Collections.Generic;

namespace GenericSortedSet
{
	// ===========
	// simple
	// ===========
	class SortPeopleByAge : IComparer<Person>
	{
		public int Compare(Person firstPerson, Person secondPerson)
		{
			if (firstPerson.Age > secondPerson.Age)
			{
				return 1;
			}
			if (firstPerson.Age < secondPerson.Age)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
	}
	class Person
	{
		public string Name { get; set; }
		public int Age { get; set; }
		public Person() { }
		public Person(string name)
		{
			this.Name = name;
		}
		public override string ToString()
		{
			return String.Format("{0} {1}", this.Name, this.Age);
		}
	}
	class P
	{
		static void Main()
		{
			SortedSet<Person> s = new SortedSet<Person>(new SortPeopleByAge())
			{
				new Person {Name = "Homer", Age =  47},
				new Person {Name = "Marge", Age = 45},
				new Person {Name = "List", Age = 9},
				new Person {Name = "Bart", Age = 8},
			};

			foreach (Person p in s)
			{
				Console.WriteLine(p);
			}

		}
	}
}
