﻿/**
 * What?
 *	- similar to List but what makes it unique is that this
 *	class supports an event named 'CollectionChanged' whenever the collection
 *	is modified. It throws a 'CollectionChanged' event
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace oc
{
	class Person
	{
		public string Name { get; set; }
		public int Age { get; set; }
		public Person() { }
		public Person(string name)
		{
			this.Name = name;
		}
		public override string ToString()
		{
			return String.Format("{0} {1}", this.Name, this.Age);
		}
	}

	class Program
	{
		static void Main()
		{
			// make a collection to observer and add a few Person objects
			ObservableCollection<Person> people = new ObservableCollection<Person>()
			{
				new Person {Name = "Martin", Age = 10},
				new Person {Name = "Moises", Age = 120}
			};

			// write up the collectionChange event
			people.CollectionChanged += people_CollectionChanged;
		}

		static void people_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			throw new NotImplementedException();
		}
	}
}