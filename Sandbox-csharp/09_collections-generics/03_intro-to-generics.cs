﻿/*
 * Generic type parametners?
 *	- List<T> this means List of "T" or List of type "T". These tokens (e.g. T) are formally refer as "type parameters"
 *	but they are also called "placeholders"
 *	
 * What?
 *	- remember that after you set the type paramenter, you cannot change it
 * 
 */
using System;
using System.Collections.Generic;

namespace Generics
{
	class Person {}
	class Shape : IPointy<Shape>
	{
		public void Count(Shape obj)
		{
			Console.WriteLine("Count");
		}
	}

	public interface IPointy<T>
	{
		void Count(T obj);
	}

	class P
	{
		static void Main()
		{
			// ==============
			// using list
			// ==============
			// This List<> can help only Person objects
			List<Person> mp = new List<Person>();
			mp.Add(new Person());
				// - can only contain Person objects therefore, the CLR does not
				// need to perform a cast when plucking the items from the container
			// which makes t his approach more type save
			//mp.Add(2);
				// this does not work

			List<int> moreInts = new List<int>();
			moreInts.Add(10);
			moreInts.Add(22);
			int sum = moreInts[0] + moreInts[1];
				// - this list can only contain ints, all of which
				// are allocated on the stack; in other words, there is
			// no hidden boxing OR unboxing as you found in the nongeneric ArrayList

			// ==============
			// specifying type parameters for generic members
			// ==============
			int[] myInts = { 10, 4, 2, 33, 9, 33 };
			Array.Sort<int>(myInts);
			foreach (int i in myInts)
			{
				Console.WriteLine(i);
			}

			// ==============
			// specifying tpe parametners for generic intefaces
			// ==============
			IPointy<Shape> b = new Shape();
		}
		
	}
}