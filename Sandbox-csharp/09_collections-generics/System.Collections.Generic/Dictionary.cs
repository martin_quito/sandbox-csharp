﻿/*
Why?
    - because it represents a collection of keys and values

What?
    - its implemened as a hash table. what are hash tables?
    - speed of the retrievel depends on the quality of the HASHING ALGORITHM of the
    type specified for "TKEY" 
    - every key must be unique according to the DICTIONARY's equality comparer
        - by default it uses the "EqualityComparer<T>.Default"
    - each item is treated as a keyvaluepair STRUCTURE representing a value
    and its key. THe order in which the items are returned is undefined


How?
    - Assembly
        System.Collections.dll  
        mscorlib.dll
        netstandard.dll
    - Namespace
        System.Collections.Generic

    - syntax
        Dictionary<TKey, TValue>

    - implements
        ICollection <KeyValuePair<TKey, TValue>>
        IDictionary <TKey, TValue>
        IEnumerable <TKey, TValue>
        IReadOnlyCollection KeyValuePair<TKey, TValue>>
        IReadOnlyDictionary <TKey, TValue>
        IDictionary
        IDeserializationCallback
        ISerializable

*/
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dictionary {
    // ======================        
    // playing around!
    // ======================    
    class MyDictionary {
        static void Main() {
            // ============
            // create a dictionary of strings, with string keys
            // and play around with it
            // ============
            Dictionary<string, string> openWith = 
                                            new Dictionary<string, string>();
            // add some elements
            openWith.Add("txt", "notepad.text");
            openWith.Add("bmp", "paint.exe");
            openWith.Add("dib", "paint.ext");
            openWith.Add("rtf", "wordpad.exe");

            // add method thows an exception if they new key is already
            // already in the dictionary
            try {
                openWith.Add("txt", "winword.exe");
            }
            catch (ArgumentException) {
                Console.WriteLine("An element with Key = \"txt\" already exists");
            }

            // the "Item" property is another name for the "indexer", so you
            // can omit its name when accessing elements
            Console.WriteLine("For key = \"rtf\", value = {0}", openWith["rtf"]);
            
            // the indexer can be used to change the value associated
            // a key.
            openWith["rtf"] = "winword.exe";
            Console.WriteLine("For key = \"rtf\", value = {0}.", openWith["rtf"]);
            
            // the indexer throws an exception if the requested key is not in the
            // dictionary
            try {
                Console.WriteLine("For key = \"tif\", value = {0}.", openWith["tif"]);
            }
            catch {
                Console.WriteLine("Key = \"tif\" is not found");
            }

            // when a program has to try keys that turn out not to be in the dictionary
            // 'TryGetValue" can be a more effiecent way to retrieve values
            string value = "";
            if (openWith.TryGetValue("tif", out value)) {
                Console.WriteLine("For key = \"tif\", value = {0}.", value);
            }
            else {
                Console.WriteLine("Key = \"tif\" is not found.");
            }

            // ContainsKey can be used to test keys before inserting them.
            if (!openWith.ContainsKey("ht")) {
                openWith.Add("ht", "hypertrm.exe");
                Console.WriteLine("Value added for key = \"ht\": {0}", openWith["ht"]);
            }

            // when you use foreaech to enumerate dictionary elements,
            // the elements are retrieved as KeyValuePair objects
            Console.WriteLine();
            foreach (KeyValuePair<string, string> kvp in openWith) {
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }

            // To get the values alone, use Values property.
            Dictionary<string, string>.ValueCollection valueColl = openWith.Values;
            Console.WriteLine();
            foreach (string s in valueColl) {
                Console.WriteLine("Value = {0}", s);
            }

            // To get the keys alone, use the keys property
            Dictionary<string, string>.KeyCollection keyColl = 
                                    openWith.Keys;
            
            // the elements of the KeyCollection are strongly typed
            // with the type that was specified for dictionary keys.
            Console.WriteLine();
            foreach (string s in keyColl) {
                Console.WriteLine("Key = {0}", s);
            }

            // use the remove method to remove a key/value pair.
            Console.WriteLine("\nRemove(\"doc\")");
            openWith.Remove("doc");
            if (!openWith.ContainsKey("doc")) {
                Console.WriteLine("Key \"doc\" is not found");
            }
        }
    }
}
