﻿/*
Why?
    - defines a key/value pair that can be set or retrieved

What?


How?

*/

using System;
using System.Collections.Generic;
namespace KeyValuePair {
    // =============
    // playing around 
    // =============
    class Pokemon {
        public string Name { get; set; }
    }

    class MyKeyValuePair {
        static void Main() {
            Dictionary<string, Pokemon> pokemons = new Dictionary<string, Pokemon>();
            pokemons["pikachu"] = new Pokemon { Name = "Pikachu" };
            pokemons["Chalmander"] = new Pokemon { Name = "Chalmander" };

            foreach(KeyValuePair<string, Pokemon> pokemon in pokemons) {
                Console.WriteLine();
            }
        }
    }
}
