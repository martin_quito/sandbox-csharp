﻿/*
How?

What?
    Namespace: System.Collections.GEneric
    Assemblies: System.Runtime.dll, mscorlib.dll, netstandard.dll
    public interface ICollection<T>: System.Collections.Generic.IEnumerable<T>
        - the type of element in the collection

Why?
    - defines methods to manipulate generic collections

*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace MyCollections {
    // defines two boxes as equal if they have the same dimensions
    public class BoxSameDimensions : EqualityComparer<Box> {
        public override bool Equals(Box b1, Box b2) {
            if (b1.Height == b2.Height && b1.Length == b2.Length) {
                return true;
            }
            else {
                return false;
            }
        }

        public override int GetHashCode(Box bx) {
            int hCode = bx.Height ^ bx.Length ^ bx.Width;
            return hCode.GetHashCode();
        }
    }


    // box
    public class Box : IEquatable<Box> {
        public int Height { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public Box(int h, int l, int w) {
            this.Height = h;
            this.Length = l;
            this.Width = w;
        }

        // defines equality using the BoxSameDimensions equality comparer
        public bool Equals(Box other) {
			return true;
        }
    }

    class Program {
    }
}
