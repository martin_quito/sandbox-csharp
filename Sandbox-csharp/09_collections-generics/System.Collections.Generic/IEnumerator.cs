﻿/*
What?
    - Namespace
        System.Collections.Generic
    - Assemblies
        System.Runtime.ddl
        mscorlib.dll
        netstandard.dll

Why?
    - support a simple iteration over a generic collection
    

How?
    C#
        public interface Inumerator<out T>: 

    implements
        IDisposable
        System.Collections.IEnumerator

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyIEnumerator{
    // ==========
    // example 1
    // ==========
    // Defines the enumerator for the Boxes collection.
	//public class Box {}
	//public class BoxEnumerator : IEnumerator<Box> {
	//	private BoxCollection _collection;
	//	private int curIndex;
	//	private Box curBox;

	//	public BoxEnumerator(BoxCollection collection) {
	//		_collection = collection;
	//		curIndex = -1;
	//		curBox = default(Box);
	//	}

	//	public bool MoveNext() {
	//		if (++curIndex >= _collection.Count) {

	//		}
	//	}
	//}

	//class IEnumerator {
	//}
}
