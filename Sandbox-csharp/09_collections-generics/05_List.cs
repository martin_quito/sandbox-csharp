﻿/*
 * What?
 *	- the most used type in the System.Collections.Generics because
 *	it allows you to resize the contents dynamically
 *	
 * 
 */
using System;
using System.Collections.Generic;

namespace GenericList
{
	class Person { }
	class P
	{
		static void Main()
		{
			// ==========
			// some useful members
			// ==========
			List<Person> people = new List<Person>()
			{
				new Person(),
				new Person(),
				new Person(),
			};
			// print out the count
			Console.WriteLine(people.Count);
			// enumerate
			foreach (Person p in people)
			{
				Console.WriteLine(p);
			}
			// insert a new person
			people.Insert(2, new Person());

			// copy data into an array
			Person[] a = people.ToArray();

		}
	}
}
