﻿/*
 * Problem with non-generic?
 *	- the majority of the classes of System.Collections can typically
 *	 help anyting whatsoever because their membes are prototyped to opeerate
 *	 on System.Objects
 */

using System;
using System.Collections;
namespace IssuesTypeSafety
{
	public class Person
	{
		public int Age { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public Person() { }
		public Person(string firstName, string lastName, int age)
		{
			Age = age;
			FirstName = firstName;
			LastName = lastName;
		}
		public override string ToString()
		{
			return "Muahaha I override your string";
		}
	}

	public class PersonCollection : IEnumerable
	{
		private ArrayList arPeople = new ArrayList();
		public Person GetPerson(int pos) { return (Person)arPeople[pos]; }
		public void AddPerson(Person p) { arPeople.Add(p); }
		public void ClearPeople() { arPeople.Clear(); }
		public int Count { get { return arPeople.Count; } }
		IEnumerator IEnumerable.GetEnumerator()
		{
			return arPeople.GetEnumerator();
		}
	}

	class P
	{
		static void Main()
		{
			// arraylist can hold anything
			ArrayList a = new ArrayList();
			a.Add(true);
			a.Add(new OperatingSystem(PlatformID.Win32Windows, new Version(10, 0)));
			
			// this is okay but most of the item you will desire a TYPE SAFE
			// container that can operate only on a parituclar TYPE OF DATA POINT

			// prior to genercis, the only way you could address this issue  of TYPE SAFETY
			// was to create a custom strongly typed collection class manually

			// using the custom strongly type using a non-generic collection
			PersonCollection m = new PersonCollection();
			m.AddPerson(new Person("Home", "Simpson", 40));
			m.AddPerson(new Person("Home", "Simpson", 40));
			//m.AddPerson(new Object());
				// - compile time error

			// - the problem with this is that I need to create a custom collection
			// for each unique data type I want to contain.
		}
	}
}