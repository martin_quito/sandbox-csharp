﻿/*
 * 
 */
using System;
using System.Collections.Generic;
namespace CustomGenerics
{
	// =========================
	// creating custom generic methods
	// =========================	
	class P1
	{
		static void Main()
		{
			// Swap 2 ints
			int a = 10, b = 90;
			Console.WriteLine("Before swap: {0}, {1}", a, b);
			Swap<int>(ref a, ref b);
			Console.WriteLine("After swap: {0}, {1}", a, b);

			// swap 2 strings
			string a2 = "meow", b2 = "woof";
			Console.WriteLine("Before swap: {0}, {1}", a2, b2);
			Swap<string>(ref a2, ref b2);
			Console.WriteLine("After swap: {0}, {1}", a2, b2);

		}

		// - building a swapping method is okay, but if you want to do
		// it for other types, it will incrase in size,
		// and if you try building a method that operateos in object
		// you will face the two main issues that non-generic collections face
		// - the solution is a static generic method
		static void Swap(ref int a, ref int b)
		{
			int temp = a;
			a = b;
			b = temp;
		}

		static void Swap<T>(ref T a, ref T b)
		{
			Console.WriteLine("You sent the Swap() method a {0}", typeof(T));
			T temp;
			temp = a;
			a = b;
			b = temp;
		}
	}
	// =========================	
	// inference type parametners
	// =========================	
	/*
	 * - you can omit in passing the type parameter because the compiler
	 * can infer the type from the member parameters
	 */
	// using teh swap above
	class P2 {
		static void Main()
		{
			bool b1 = true, b2 = false;
			Console.WriteLine("Before swap: {0}, {1}", b1, b2);
			// infering
			//Swap<bool>(ref b1, ref b2);
			Swap(ref b1, ref b2);
			Console.WriteLine("After swap: {0}, {1}", b1, b2);
		}

		static void Swap<T>(ref T a, ref T b)
		{
			Console.WriteLine("You sent the Swap() method a {0}", typeof(T));
			T temp;
			temp = a;
			a = b;
			b = temp;
		}

		// - even though its possible, its preferable to provide the type parameter
		// so we know its a generic
		// - also, inference type paramenters WORKS ONLY IF THE GENERIC METHOD HAS
		// at least one parameter
	}

	// =========================	
	// custom generic structures and classe
	// =========================	
	public struct Point<T>
	{
		// generic state date
		private T xPos;
		private T yPos;

		// generic ctors
		public Point(T xVal, T yVal)
		{
			xPos = xVal;
			yPos = yVal;
		}

		// generic props
		public T X
		{
			get { return xPos; }
			set { xPos = value; }
		}
		public T Y
		{
			get { return yPos; }
			set { yPos = value; }
		}
		public override string ToString()
		{
			return string.Format("[{0}, {1}]", xPos, yPos);
		}
		// reset fields to the default value of the
		// type parameter
		public void ResetPoint()
		{
			xPos = default(T);
			yPos = default(T);
		}
		// - the 'default' has duo funcationality
		// - it sets a type parameter to its default value. This is helpful because
		// a generic type does not know the actual placehelpders upfront  which means
		// it cannot know what default it should set. So this default sets
		//		numerical values to 0
		//		ref types to null
	}
}
