﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Issues {
	class P
	{
		static void SimpleBoxUnboxOperation()
		{
			// Make a ValueType (int) variable
			int myInt = 25;

			// - Box the int into an object reference
			object boxedInt = myInt;
			/*
			 * Boxing?
			 *	- "formally defined as the process of explicitly assigning
			 *	a value type to a System.Object variable. When you box
			 *	a value, the CLR allocates a new object on the heap and copies
			 *	the value type's value (25, in this case) into that instance.
			 *	What is returned to you is a reference to the newly allocated help-based object"
			 *	
			 * Unboxing?
			 *	- "process of converting the value help in the object reference
			 *	back into a corresponding value type on the
			 *	stack. Syntactically speaking, an unboxing operations looks like
			 *	a normal casting operation. However, the semantics are quite different.
			 *	The CLR begins by verifying that the receiving data type is equivalent
			 *	to the boxed type; and if so, it copies the value back into a local
			 *	stack-based variable"
			 */
			int unboxedInt = (int)boxedInt;

			// example when boxing/unboxing is uself such as in an ARrayList
			ArrayList myInts = new ArrayList();
			myInts.Add(10);
				// add receives an object (this boxes the value type into an object)
			
			// this unboxes it to a stack-based data
			int i = (int)myInts[0];

			// - so the problem is the boxing and unboxing operation when the arraylist
			// becomes large. It brings issus such as code size, execution speed, and
			// type safety. 
			// - also note when explciitly casting, yo u need to wrap it in a try/catch
			// block
		}
	}
}