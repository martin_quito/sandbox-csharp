﻿/*
 * What?
 *	- LIFO (last in, first out)
 *	- 
 * 
 */
using System;
using System.Collections.Generic;

namespace GenericStack
{
	class Person { }
	class P
	{
		static void Main()
		{
			// ==========
			// some useful members
			// ==========
			Stack<Person> people = new Stack<Person>();
			
			// push
			people.Push(new Person());
			people.Push(new Person());

			// look at the top
			people.Peek();

			// pop 
			people.Pop();

		}
	}
}
