﻿/*
 * What?
 *	- 
 */
using System;
using System.Collections.Generic;

namespace GenericQueue
{
	class Person {
		public string Name { get; set; }
		public Person() { }
		public Person(string name)
		{
			this.Name = name;
		}
	}
	class P
	{
		static void Main()
		{
			// ==========
			// simple
			// ==========
			Queue<Person> people = new Queue<Person>();
			people.Enqueue(new Person("martin"));
			people.Enqueue(new Person("miriam"));
			people.Enqueue(new Person("nefi"));

			// peek at the first person in queue
			Console.WriteLine(people.Peek().Name);

			// try to de-q
			try
			{
				GetCofee(people.Dequeue());
				GetCofee(people.Dequeue());
				GetCofee(people.Dequeue());
				GetCofee(people.Dequeue());
			}
			catch (InvalidOperationException e)
			{
				Console.WriteLine(e.Message);
			}
		}

		static void GetCofee(Person p) {
			Console.WriteLine("{0} got cofee!", p.Name);
		}
	}
}
