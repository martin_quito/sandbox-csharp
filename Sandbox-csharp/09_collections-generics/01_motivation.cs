﻿/*
 * What are collections classes?
 *	- classes to overcome the limitations of arrays where they can 
 *	resize on the fly as you insert or remove items
 *	- it has two broad categories nongeneric and generic collections
 * 
 * History?
 *	- generics came in .net 2 which added a new namespace System.Collection.Generics where as
 *	before System.Collections was being used
 * 
 * Benefits of Generics over nongenveric?
 *	- provide greater type safety they can contain only the type of type you specify
 *	- performance benefits because they do not result in boxing or  unboxing penalities when storing
 *		value types
 *	- greatly reduce the need to build custom collection types becase you can specify
 *	the "type of type" when creating the generic container.
 *	
 * Benefits or an array 
 *	- always remember that they are fixed size, once its set to a size, you cannot add more to it
 *	- good to manage small amaounts of fixed size
 *	
 * What arrays lack?
 *	- more flexible data structure to dynamically grow and shrink
 *	- a container that can hold objects that meet a specific crieteria (only objects
 *	deriving form a specific base class or only objects implementing a particular interface)
 *	
 * Nongeneric collections?
 *		System.Collections
 *			ArrayList
 *			BitArray
 *			Hashtable
 *			Queue
 *			SortedList
 *			Stack
 *		System.Collections.Specialized
 *			HybridDictionary
 *			ListDictionary
 *			StringCollection
 *			BitVector32
 *		
 * What is the problem with nongeneric collections?
 *	- poor performing code, especially when maniplating numerical data (value types).
 *		- CLR must perform a number of memory transfer operations when you store structures
 *		in any nongeneric collection class prototyped to operate on System.Objects which can
 *		hurt execution speed
 *	- most of the nongeneric collections are not TYPE SAFE because (again) they were develop to 
 *	operate on System.Objects, and they could therefore contain anything at all. If a .NET developer
 *	needed to create a highly type-safe collection (e.g. a container that can hold objects
 *	implementing only a certain interface), they only real choice was to create a brand
 *	new collection class by hand.
 * 
 * 
 */
using System;
using System.Collections.Generic;
namespace GenericsMotivation
{
	// ==========
	// a simple array
	// ==========
	// good to manage sm all amounts of fixed-size data
	class Program
	{
		static void Main()
		{
			string[] a = { "first", "second", "third" };
			Console.WriteLine(a.Length);
			foreach (string s in a)
			{
				Console.WriteLine(s);
			}
		}
	}
}