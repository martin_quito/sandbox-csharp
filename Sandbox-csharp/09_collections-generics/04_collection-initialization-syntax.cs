﻿/*
 * What?
 *	- the collection initialization syntax can only be applied to classes that
 *	support an Add() method, which is fformalized by the ICollection<t>/ICollection
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace CollectionInitializationSyntax
{
	class Point 
	{
		public int X { get; set; }
		public int Y { get; set; }
	}
	class P
	{
		static void Main()
		{
			// simple collection initialization syntax
			int[] m1 = { 0, 1, 2, 3 };
			List<int> m2 = new List<int> { 0, 1, 2, 3 };
			ArrayList m3 = new ArrayList { 0, 1, 2, 3 };

			// - blending the OBJECT INITIALIZATION SYNTAX with
			// COLLECTION INITALIZATION SYNTAX 
			List<Point> m4 = new List<Point>
			{
				// this is the object init syntax
				new Point() {X = 2, Y = 2},
				new Point {X = 1, Y = 1}
			};
			foreach (Point i in m4)
			{
				Console.WriteLine(i.X);
			}
		}
	}
}